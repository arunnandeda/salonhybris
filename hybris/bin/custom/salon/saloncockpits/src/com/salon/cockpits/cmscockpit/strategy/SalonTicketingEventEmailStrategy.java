package com.salon.cockpits.cmscockpit.strategy;

import de.hybris.platform.comments.model.CommentAttachmentModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.ticket.email.context.AbstractTicketContext;
import de.hybris.platform.ticket.events.model.CsTicketEmailModel;
import de.hybris.platform.ticket.events.model.CsTicketEventModel;
import de.hybris.platform.ticket.model.CsTicketEventEmailConfigurationModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.strategies.impl.DefaultTicketEventEmailStrategy;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;

public class SalonTicketingEventEmailStrategy extends DefaultTicketEventEmailStrategy {

	private static final Logger LOG = Logger.getLogger(SalonTicketingEventEmailStrategy.class);
	@Autowired
	private MediaService mediaService;
	@Autowired
	private RendererService rendererService;
	
	public SalonTicketingEventEmailStrategy() {
		super();
	}

	@Override
	public void sendEmailsForEvent(CsTicketModel ticket, CsTicketEventModel event) {

		super.sendEmailsForEvent(ticket, event);
	}

	@Override
	protected CsTicketEmailModel constructAndSendEmail(AbstractTicketContext ticketContext,
			CsTicketEventEmailConfigurationModel config) {

		try {
			if (ticketContext.getTo() != null) {
				
			}
			LOG.warn("Could not send email for event [" + ticketContext.getEvent() + "]. With config [" + config
					+ "] No recipient could be found.");
			
				HtmlEmail e = (HtmlEmail) this.getPreConfiguredEmail();
				LOG.info("creating email to send to the customer service team for call back request");
				try {
					e.setCharset("UTF-8");
				} catch (IllegalArgumentException arg13) {
					LOG.error("Setting charset to \'UTF-8\' failed.", arg13);
				}

				VelocityContext ctx = new VelocityContext();
				ctx.put("ctx", ticketContext);
				StringWriter subj = new StringWriter();
				Velocity.evaluate(ctx, subj, "logtag", new StringReader(config.getSubject()));
				e.setSubject(subj.toString());
				e.addTo(Config.getParameter("mail.smtp.user"));
				StringWriter htmlVersion = new StringWriter();
				this.rendererService.render(config.getHtmlTemplate(), ticketContext, htmlVersion);
				e.setHtmlMsg(htmlVersion.toString());
				StringWriter textVersion = new StringWriter();
				if (config.getPlainTextTemplate() != null) {
					this.rendererService.render(config.getPlainTextTemplate(), ticketContext, textVersion);
					e.setTextMsg(textVersion.toString());
				}

			Collection attachments = ticketContext.getAttachments();
			if (attachments != null && !attachments.isEmpty()) {
				Iterator messageID = attachments.iterator();

				while (messageID.hasNext()) {
					CommentAttachmentModel storedEmail = (CommentAttachmentModel) messageID.next();
					if (storedEmail.getItem() instanceof MediaModel) {
						try {
							MediaModel ex = (MediaModel) storedEmail.getItem();
							ByteArrayDataSource dataSource = new ByteArrayDataSource(
									this.mediaService.getStreamFromMedia(ex), ex.getMime());
							e.attach(dataSource, ex.getRealFileName(), ex.getDescription());
						} catch (IOException arg12) {
							LOG.error("Failed to load attachment data into data source [" + storedEmail + "]", arg12);
						}
					}
				}
			}

			CsTicketEmailModel storedEmail1 = (CsTicketEmailModel) this.getModelService()
					.create(CsTicketEmailModel.class);
			storedEmail1.setTo(ticketContext.getTo());
			storedEmail1.setFrom(e.getFromAddress().toString());
			storedEmail1.setSubject(e.getSubject());
			storedEmail1
					.setBody(textVersion.toString() + System.getProperty("line.separator") + htmlVersion.toString());
			String messageID1 = e.send();
			storedEmail1.setMessageId(messageID1);
			return storedEmail1;

		} catch (EmailException arg14) {
			LOG.error("Error sending email to [" + config.getRecipientType() + "]. Context was [" + ticketContext + "]",
					arg14);
		}

		return null;
	}
}
