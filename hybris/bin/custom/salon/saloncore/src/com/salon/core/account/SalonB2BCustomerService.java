package com.salon.core.account;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;


public interface SalonB2BCustomerService
{

	EmployeeModel getSalesRepresentative(B2BCustomerModel b2bCustomerModel);

}