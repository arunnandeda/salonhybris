package com.salon.core.account.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;

import com.salon.core.account.SalonB2BCustomerService;


public class SalonB2BCustomerServiceImpl extends DefaultB2BCustomerService implements SalonB2BCustomerService
{

	private B2BUnitService<CompanyModel, UserModel> b2bUnitService;

	public B2BUnitService<CompanyModel, UserModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	public void setB2bUnitService(final B2BUnitService<CompanyModel, UserModel> b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}



	@Override
	public EmployeeModel getSalesRepresentative(final B2BCustomerModel b2bCustomerModel)
	{
		validateParameterNotNull(b2bCustomerModel, "B2B Customer Model cannot be null");
		final B2BUnitModel b2bUnitModel = (B2BUnitModel) b2bUnitService.getParent(b2bCustomerModel);
		if (b2bUnitModel != null)
		{
			final EmployeeModel salesRepresentative = b2bUnitModel.getSalesRepresentative();
			return salesRepresentative;
		}
		else
		{
			return null;
		}

	}
}