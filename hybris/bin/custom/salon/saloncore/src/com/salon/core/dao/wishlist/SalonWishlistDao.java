package com.salon.core.dao.wishlist;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.impl.daos.Wishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface SalonWishlistDao extends Wishlist2Dao {

	public Wishlist2Model findWishlist2ByName(String wishListName, UserModel user);

	public Wishlist2Model findWishlist2ById(String id, UserModel user);

}
