package com.salon.core.dao.wishlist.impl;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.salon.core.dao.wishlist.SalonWishlistDao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.impl.daos.impl.DefaultWishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class SalonWishlistDaoImpl extends DefaultWishlist2Dao implements SalonWishlistDao {

	private static final Logger LOG = LoggerFactory.getLogger(SalonWishlistDaoImpl.class);

	@Autowired
	FlexibleSearchService flexibleSearchService;

	/**
	 * Getting wish List by name
	 * 
	 * @param wishlistName
	 *            wishlist Name
	 * @param user
	 *            currentCustomer
	 * 
	 * @return Wishlist2Model(null or Wishlist2Model) When customer create new
	 *         Wish List that time We are checking that wishList already existed
	 *         or not for current customer . If wishList not exist then it will
	 *         return null otherwise return wishlist model
	 */
	@Override
	public Wishlist2Model findWishlist2ByName(String wishListName, UserModel user) {
		LOG.debug("wish List Name.." + wishListName + " user is .." + user.getName());

		StringBuilder query = new StringBuilder("SELECT {pk} FROM {Wishlist2} WHERE LOWER({");
		query.append("name}) = LOWER(?wishListName) AND {");
		query.append("user} = ?user");
		FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
		fQuery.addQueryParameter("wishListName", wishListName);
		fQuery.addQueryParameter("user", user);

		final SearchResult<Wishlist2Model> result = flexibleSearchService.search(fQuery);
		return CollectionUtils.isNotEmpty(result.getResult()) ? result.getResult().get(0) : null;
	}

	/**
	 * Getting wish List by id
	 * 
	 * @param id
	 *            wishlist id(pk)
	 * @param user
	 *            currentCustomer
	 * @return Wishlist2Model(null or Wishlist2Model) When customer delete the
	 *         wish list that time it will pass pk of wish list and Here we are
	 *         Getting that particular wishlist pk model.
	 */
	@Override
	public Wishlist2Model findWishlist2ById(String id, UserModel user) {
		LOG.debug("wish List ID.." + id + "user is .." + user.getName());

		StringBuilder query = new StringBuilder("SELECT {pk} FROM {Wishlist2} WHERE {");
		query.append("pk} = ?id AND {");
		query.append("user} = ?user");
		FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
		fQuery.addQueryParameter("id", id);
		fQuery.addQueryParameter("user", user);

		final SearchResult<Wishlist2Model> result = flexibleSearchService.search(fQuery);
		return CollectionUtils.isNotEmpty(result.getResult()) ? result.getResult().get(0) : null;
	}

}
