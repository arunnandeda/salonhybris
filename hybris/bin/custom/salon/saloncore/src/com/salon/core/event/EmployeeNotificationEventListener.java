package com.salon.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.salon.core.process.model.StorefrontEmployeeNotificationProcessModel;
import com.salon.core.registration.event.SendEmployeeNotificationEvent;


public class EmployeeNotificationEventListener extends AbstractAcceleratorSiteEventListener<SendEmployeeNotificationEvent>
{




	private ModelService modelService;

	private BusinessProcessService businessProcessService;


	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	@Override
	protected SiteChannel getSiteChannelForEvent(final SendEmployeeNotificationEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}

	@Override
	protected void onSiteEvent(final SendEmployeeNotificationEvent event)
	{
		final StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel = (StorefrontEmployeeNotificationProcessModel) getBusinessProcessService()
				.createProcess(
						"storefrontEmployeeNotificationProcess-" + event.getEmployee().getUid() + "-" + System.currentTimeMillis(),
						"storefrontEmployeeNotificationProcess");
		storefrontEmployeeNotificationProcessModel.setEmployee(event.getEmployee());
		storefrontEmployeeNotificationProcessModel.setCustomer(event.getCustomer());
		storefrontEmployeeNotificationProcessModel.setSite(event.getSite());
		storefrontEmployeeNotificationProcessModel.setStore(event.getBaseStore());
		storefrontEmployeeNotificationProcessModel.setLanguage(event.getLanguage());
		storefrontEmployeeNotificationProcessModel.setCurrency(event.getCurrency());
		getModelService().save(storefrontEmployeeNotificationProcessModel);
		getBusinessProcessService().startProcess(storefrontEmployeeNotificationProcessModel);



	}

}