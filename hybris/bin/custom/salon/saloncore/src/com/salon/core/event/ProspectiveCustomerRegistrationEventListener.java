package com.salon.core.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.newcustomer.event.ProspectiveCustomerRegistrationEvent;
import com.salon.core.process.model.ProspectiveCustomerRegistrationEmailProcessModel;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

public class ProspectiveCustomerRegistrationEventListener
		extends AbstractAcceleratorSiteEventListener<ProspectiveCustomerRegistrationEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(ProspectiveCustomerRegistrationEventListener.class);
	@Autowired
	private ModelService modelService;

	@Autowired
	private BusinessProcessService businessProcessService;

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService() {
		return businessProcessService;
	}

	public void setBusinessProcessService(BusinessProcessService businessProcessService) {
		this.businessProcessService = businessProcessService;
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(ProspectiveCustomerRegistrationEvent event) {
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}

	@Override
	protected void onSiteEvent(ProspectiveCustomerRegistrationEvent event) {
		final ProspectiveCustomerRegistrationEmailProcessModel prospectiveCustomerRegistrationEmailProcessModel = (ProspectiveCustomerRegistrationEmailProcessModel) getBusinessProcessService()
				.createProcess("prospectiveCustomerRegistrationEmailProcess-" + event.getProspectiveCustomer().getEmail() + "-"
						+ System.currentTimeMillis(), "prospectiveCustomerRegistrationEmailProcess");
		LOG.info("Trigerring the prospective customer registration event listener");
		prospectiveCustomerRegistrationEmailProcessModel.setNewCustomer(event.getProspectiveCustomer());
		prospectiveCustomerRegistrationEmailProcessModel.setSite(event.getSite());
		prospectiveCustomerRegistrationEmailProcessModel.setStore(event.getBaseStore());
		prospectiveCustomerRegistrationEmailProcessModel.setLanguage(event.getLanguage());
		prospectiveCustomerRegistrationEmailProcessModel.setCurrency(event.getCurrency());
		getModelService().save(prospectiveCustomerRegistrationEmailProcessModel);
		getBusinessProcessService().startProcess(prospectiveCustomerRegistrationEmailProcessModel);
	}

}
