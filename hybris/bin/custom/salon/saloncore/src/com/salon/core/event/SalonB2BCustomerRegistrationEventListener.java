package com.salon.core.event;

import org.springframework.beans.factory.annotation.Required;

import com.salon.core.process.model.B2BCustomerRegistrationEmailProcessModel;
import com.salon.core.registration.event.SalonB2BCustomerRegistrationEvent;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

public class SalonB2BCustomerRegistrationEventListener extends AbstractAcceleratorSiteEventListener<SalonB2BCustomerRegistrationEvent> {

	private ModelService modelService;
	private BusinessProcessService businessProcessService;


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	
	protected ModelService getModelService()
	{
		return modelService;
	}

	
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final SalonB2BCustomerRegistrationEvent event)
	{
		final B2BCustomerRegistrationEmailProcessModel salonB2BCustomerRegistrationProcessModel = (B2BCustomerRegistrationEmailProcessModel) getBusinessProcessService()
				.createProcess("b2bCustomerRegistrationEmailProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"b2bCustomerRegistrationEmailProcess");
		salonB2BCustomerRegistrationProcessModel.setSite(event.getSite());
		salonB2BCustomerRegistrationProcessModel.setCustomer(event.getCustomer());
		salonB2BCustomerRegistrationProcessModel.setLanguage(event.getLanguage());
		salonB2BCustomerRegistrationProcessModel.setCurrency(event.getCurrency());
		salonB2BCustomerRegistrationProcessModel.setStore(event.getBaseStore());
		getModelService().save(salonB2BCustomerRegistrationProcessModel);
		getBusinessProcessService().startProcess(salonB2BCustomerRegistrationProcessModel);
		
		
		
		
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final SalonB2BCustomerRegistrationEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}
}
