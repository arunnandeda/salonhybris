package com.salon.core.event;

import org.springframework.beans.factory.annotation.Required;

import com.salon.core.process.model.CustomerRegNotificationForSalesRepEmailProcessModel;
import com.salon.core.registration.event.SendCustomerRegNotificationToSalesRepEvent;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

public class SendCustomerRegNotificationToSalesRepEventListener extends AbstractAcceleratorSiteEventListener<SendCustomerRegNotificationToSalesRepEvent> {
	private ModelService modelService;
	private BusinessProcessService businessProcessService;


	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final SendCustomerRegNotificationToSalesRepEvent event)
	{
		final CustomerRegNotificationForSalesRepEmailProcessModel customerRegNotificationForSalesRepEmailProcessModel = (CustomerRegNotificationForSalesRepEmailProcessModel) getBusinessProcessService()
				.createProcess("customerRegNotificationForSalesRepEmailProcess-" + event.getEmployee().getUid() + "-" + System.currentTimeMillis(),
						"customerRegNotificationForSalesRepEmailProcess");
		customerRegNotificationForSalesRepEmailProcessModel.setSite(event.getSite());
		customerRegNotificationForSalesRepEmailProcessModel.setCustomer(event.getCustomer());
		customerRegNotificationForSalesRepEmailProcessModel.setLanguage(event.getLanguage());
		customerRegNotificationForSalesRepEmailProcessModel.setCurrency(event.getCurrency());
		customerRegNotificationForSalesRepEmailProcessModel.setStore(event.getBaseStore());
		customerRegNotificationForSalesRepEmailProcessModel.setEmployee(event.getEmployee());
		getModelService().save(customerRegNotificationForSalesRepEmailProcessModel);
		getBusinessProcessService().startProcess(customerRegNotificationForSalesRepEmailProcessModel);
		
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final SendCustomerRegNotificationToSalesRepEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();
	}
}

