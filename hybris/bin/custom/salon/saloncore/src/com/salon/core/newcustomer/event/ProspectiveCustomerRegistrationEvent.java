package com.salon.core.newcustomer.event;

import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.process.AbstractProspectiveCustomerNotificationEvent;


public class ProspectiveCustomerRegistrationEvent extends AbstractProspectiveCustomerNotificationEvent {

	private static final long serialVersionUID = 1L;
	private ProspectiveCustomerRegModel  prospectiveCustomer;
	
	public ProspectiveCustomerRegModel getProspectiveCustomer() {
		return prospectiveCustomer;
	}
	public void setProspectiveCustomer(ProspectiveCustomerRegModel prospectiveCustomer) {
		this.prospectiveCustomer = prospectiveCustomer;
	}
	public ProspectiveCustomerRegistrationEvent(ProspectiveCustomerRegModel prospectiveCustomer) {
		super();
		this.prospectiveCustomer = prospectiveCustomer;
	}
	
	
	
	
}
