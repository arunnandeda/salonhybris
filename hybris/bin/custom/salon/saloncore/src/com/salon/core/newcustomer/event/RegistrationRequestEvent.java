package com.salon.core.newcustomer.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class RegistrationRequestEvent extends AbstractCommerceUserEvent<BaseSiteModel>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
