package com.salon.core.process;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.EmployeeModel;


public class AbstractEmployeeNotificationEvent extends AbstractCommerceUserEvent
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private EmployeeModel employee;

	public EmployeeModel getEmployee()
	{
		return employee;
	}

	public void setEmployee(final EmployeeModel employee)
	{
		this.employee = employee;
	}



}