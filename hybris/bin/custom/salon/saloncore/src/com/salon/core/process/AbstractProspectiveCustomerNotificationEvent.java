package com.salon.core.process;

import com.salon.core.model.ProspectiveCustomerRegModel;

import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class AbstractProspectiveCustomerNotificationEvent extends AbstractCommerceUserEvent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ProspectiveCustomerRegModel prospectiveCustomer;

	public ProspectiveCustomerRegModel getProspectiveCustomer() {
		return prospectiveCustomer;
	}

	public void setProspectiveCustomer(ProspectiveCustomerRegModel prospectiveCustomer) {
		this.prospectiveCustomer = prospectiveCustomer;
	}
	

}
