package com.salon.core.registration.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class SalonB2BCustomerRegistrationEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
