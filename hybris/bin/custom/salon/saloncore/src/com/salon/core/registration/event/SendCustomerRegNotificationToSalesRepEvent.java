package com.salon.core.registration.event;

import com.salon.core.process.AbstractEmployeeNotificationEvent;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.model.user.EmployeeModel;

public class SendCustomerRegNotificationToSalesRepEvent extends AbstractEmployeeNotificationEvent {

	/**
	 * 
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private B2BCustomerModel customer;
	
	private EmployeeModel employee;
	
	

	public EmployeeModel getEmployee() {
		return employee;
	}





	public void setEmployee(EmployeeModel employee) {
		this.employee = employee;
	}





	public SendCustomerRegNotificationToSalesRepEvent(B2BCustomerModel customer,EmployeeModel employee) {
		super();
		this.customer = customer;
	}





	public B2BCustomerModel getCustomer() {
		return customer;
	}





	public void setCustomer(B2BCustomerModel customer) {
		this.customer = customer;
	}



}
