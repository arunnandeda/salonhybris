package com.salon.core.registration.event;

import de.hybris.platform.core.model.user.EmployeeModel;

import com.salon.core.process.AbstractEmployeeNotificationEvent;


public class SendEmployeeNotificationEvent extends AbstractEmployeeNotificationEvent
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private EmployeeModel employee;

	@Override
	public EmployeeModel getEmployee()
	{
		return employee;
	}

	@Override
	public void setEmployee(final EmployeeModel employee)
	{
		this.employee = employee;
	}

	public SendEmployeeNotificationEvent(final EmployeeModel employee)
	{
		super();
		this.employee = employee;
	}




}