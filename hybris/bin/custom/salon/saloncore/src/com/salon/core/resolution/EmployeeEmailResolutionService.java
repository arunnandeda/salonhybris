package com.salon.core.resolution;

import de.hybris.platform.core.model.user.EmployeeModel;


public interface EmployeeEmailResolutionService
{

	String getEmailForEmployee(EmployeeModel employee);
}