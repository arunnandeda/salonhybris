package com.salon.core.resolution;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerEmailResolutionService;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


public class SalonEmployeeEmailResolutionService implements EmployeeEmailResolutionService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomerEmailResolutionService.class);
	private ConfigurationService configurationService;

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	@Override
	public String getEmailForEmployee(final EmployeeModel employee)
	{
		return employee.getUid();

	}

	protected String validateAndProcessEmailForEmployee(final EmployeeModel employee)
	{
		validateParameterNotNullStandardMessage("employee", employee);
		final String email = employee.getUid();
		try
		{
			MailUtils.validateEmailAddress(email, "employee email");
			return email;
		}
		catch (final EmailException e)
		{
			LOG.info("Given uid is not appropriate email. Employee PK: " + String.valueOf(employee.getPk()) + " Exception: "
					+ e.getClass().getName());
		}
		return null;
	}

}