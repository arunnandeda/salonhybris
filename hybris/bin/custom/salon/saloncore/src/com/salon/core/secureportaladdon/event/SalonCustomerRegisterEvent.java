/**
 *
 */
package com.salon.core.secureportaladdon.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;


/**
 * @author 1DigitalS
 *
 */
public class SalonCustomerRegisterEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{
	private String token;



	/**
	 *
	 */
	public SalonCustomerRegisterEvent()
	{
		super();
		// YTODO Auto-generated constructor stub
	}

	/**
	 * @param token
	 */
	public SalonCustomerRegisterEvent(final String token)
	{
		super();
		this.token = token;
	}


	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @param token
	 *           the token to set
	 */
	public void setToken(final String token)
	{
		this.token = token;
	}



}
