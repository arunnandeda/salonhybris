/**
 *
 */
package com.salon.core.secureportaladdon.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;

import com.salon.core.secureportaladdon.model.process.SalonCustomerRegistrationEmailProcessModel;


/**
 * @author 1DigitalS
 *
 */
public class SalonCustomerRegistrationEventListener extends AbstractAcceleratorSiteEventListener<SalonCustomerRegisterEvent>
{

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener#getSiteChannelForEvent(de.hybris.
	 * platform.servicelayer.event.events.AbstractEvent)
	 */
	@Override
	protected SiteChannel getSiteChannelForEvent(final SalonCustomerRegisterEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return site.getChannel();

	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commerceservices.event.AbstractSiteEventListener#onSiteEvent(de.hybris.platform.servicelayer.
	 * event.events.AbstractEvent)
	 */
	@Override
	protected void onSiteEvent(final SalonCustomerRegisterEvent event)
	{
		final SalonCustomerRegistrationEmailProcessModel salonCustomerRegistrationEmailProcessModel = (SalonCustomerRegistrationEmailProcessModel) getBusinessProcessService()
				.createProcess(
						"salonCustomerRegistrationEmailProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"salonCustomerRegistrationEmailProcess");
		salonCustomerRegistrationEmailProcessModel.setSite(event.getSite());
		salonCustomerRegistrationEmailProcessModel.setCustomer(event.getCustomer());
		salonCustomerRegistrationEmailProcessModel.setToken(event.getToken());
		salonCustomerRegistrationEmailProcessModel.setStore(event.getBaseStore());
		salonCustomerRegistrationEmailProcessModel.setLanguage(event.getLanguage());
		salonCustomerRegistrationEmailProcessModel.setCurrency(event.getCurrency());
		getModelService().save(salonCustomerRegistrationEmailProcessModel);
		getBusinessProcessService().startProcess(salonCustomerRegistrationEmailProcessModel);

	}

}
