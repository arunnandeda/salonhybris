/**
 *
 */
package com.salon.core.secureportaladdon.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * @author 1DigitalS
 *
 */
public class MissingParameterFieldException extends BusinessException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public MissingParameterFieldException(final String message)
	{
		super(message);
		// YTODO Auto-generated constructor stub
	}

}
