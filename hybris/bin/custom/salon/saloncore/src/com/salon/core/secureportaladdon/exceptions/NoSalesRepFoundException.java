package com.salon.core.secureportaladdon.exceptions;

import de.hybris.platform.servicelayer.exceptions.BusinessException;

public class NoSalesRepFoundException extends BusinessException {

	

	public NoSalesRepFoundException(final String message)
	{
		super(message);
	}

	
}
