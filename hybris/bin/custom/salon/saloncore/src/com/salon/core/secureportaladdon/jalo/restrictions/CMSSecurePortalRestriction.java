package com.salon.core.secureportaladdon.jalo.restrictions;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.localization.Localization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CMSSecurePortalRestriction extends GeneratedCMSSecurePortalRestriction
{
	@SuppressWarnings("unused")
	private final static Logger LOG = LoggerFactory.getLogger( CMSSecurePortalRestriction.class.getName() );
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem( ctx, type, allAttributes );
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}
	
	/**
	 * @deprecated Since 5.4. use
	 *             {@link de.hybris.platform.secureportaladdon.model.restrictions.CMSSecurePortalRestrictionModel#getDescription()}
	 *             instead.
	 */
	@Deprecated
	@Override
	public String getDescription(SessionContext sessionContext)
	{
		return Localization.getLocalizedString("type.CMSSecurePortalRestriction.description.text");
	}
	
}
