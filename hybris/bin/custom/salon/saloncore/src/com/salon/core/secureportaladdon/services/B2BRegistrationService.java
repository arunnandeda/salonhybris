/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.core.secureportaladdon.services;

import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.core.model.user.EmployeeModel;

import java.util.List;

import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSalesRepFoundException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;


/**
 * Service methods that are used by the B2B registration process
 */
public interface B2BRegistrationService
{

	/**
	 * Gets the list of employees that are part of a given user group
	 *
	 * @param userGroup
	 *           The name of the user group
	 * @return Employees within the user group
	 */
	public List<EmployeeModel> getEmployeesInUserGroup(String userGroup);

	/**
	 * Gets the contact email address of the specified list of employees
	 *
	 * @param employees
	 *           List of employees to get email address from
	 * @return List of email addresses. It is possible that the list is empty since employees are not required to have an
	 *         email nor a contact address
	 */
	public List<EmailAddressModel> getEmailAddressesOfEmployees(List<EmployeeModel> employees);

	/**
	 * @param customerNumber
	 * @return
	 * @throws NoSuchB2BUnitFoundException
	 * @throws MissingParameterFieldException
	 */
	public B2BUnitModel getB2BUnitByCustomerNumber(String customerNumber)
			throws NoSuchB2BUnitFoundException, MissingParameterFieldException;

	/**
	 * @param b2bCustomerModel
	 */
	public void registerCustomer(final B2BCustomerModel b2bCustomerModel);

	//check expiry of token link sent via email for a customer to register
	public boolean checkTokenExpiry(String token) throws TokenInvalidatedException;

	//save the customer who has registered online wiht salon
	public void saveB2BCustomer(String token, String pwd, String titleCode, String name) throws TokenInvalidatedException, CustomerAlreadyExistsException, NoSalesRepFoundException;

	public void saveProspectiveCustomer(ProspectiveCustomerRegModel prospectiveCustomer)
			throws DuplicateUidException, CustomerAlreadyExistsException;
 
	
}
