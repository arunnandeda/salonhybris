/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.core.secureportaladdon.services.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.PasswordEncoderConstants;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.salon.core.account.SalonB2BCustomerService;
import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.newcustomer.event.ProspectiveCustomerRegistrationEvent;
import com.salon.core.process.AbstractEmployeeNotificationEvent;
import com.salon.core.process.AbstractProspectiveCustomerNotificationEvent;
import com.salon.core.registration.event.SalonB2BCustomerRegistrationEvent;
import com.salon.core.registration.event.SendCustomerRegNotificationToSalesRepEvent;
import com.salon.core.secureportaladdon.dao.B2BRegistrationDao;
import com.salon.core.secureportaladdon.event.SalonCustomerRegisterEvent;
import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSalesRepFoundException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.core.secureportaladdon.services.B2BRegistrationService;


/**
 * Default implementation of {@link B2BRegistrationService}
 */
public class DefaultB2BRegistrationService implements B2BRegistrationService
{

	private B2BRegistrationDao registrationDao;

	private EmailService emailService;

	private long tokenValiditySeconds;

	private SecureTokenService secureTokenService;

	private ModelService modelService;

	private EventService eventService;

	private BaseSiteService baseSiteService;

	private BaseStoreService baseStoreService;

	private CommonI18NService commonI18NService;

	private UserService userService;

	private String passwordEncoding = PasswordEncoderConstants.DEFAULT_ENCODING;

	@Autowired
	SalonB2BCustomerService salonB2BCustomerService;

	private static final Logger LOG = LoggerFactory.getLogger(DefaultB2BRegistrationService.class);

	/**
	 * @param registrationDao
	 *           the registrationDao to set
	 */
	@Required
	public void setRegistrationDao(final B2BRegistrationDao registrationDao)
	{
		this.registrationDao = registrationDao;
	}

	/**
	 * @param emailService
	 *           the emailService to set
	 */
	@Required
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	protected SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}

	@Required
	public void setSecureTokenService(final SecureTokenService secureTokenService)
	{
		this.secureTokenService = secureTokenService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected ModelService getModelService()
	{
		return modelService;

	}


	public UserService getUserService()
	{
		return userService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}


	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService service)
	{
		this.baseSiteService = service;
	}

	@Required
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService service)
	{
		this.baseStoreService = service;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}


	public String getPasswordEncoding()
	{
		return passwordEncoding;
	}

	public void setPasswordEncoding(final String passwordEncoding)
	{
		this.passwordEncoding = passwordEncoding;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.secureportaladdon.services.B2BRegistrationService#getEmployeesInUserGroup(java.lang.String)
	 */
	@Override
	public List<EmployeeModel> getEmployeesInUserGroup(final String userGroup)
	{
		return registrationDao.getEmployeesInUserGroup(userGroup);
	}


	protected long getTokenValiditySeconds()
	{
		return tokenValiditySeconds;
	}

	@Required
	public void setTokenValiditySeconds(final long tokenValiditySeconds)
	{
		if (tokenValiditySeconds < 0)
		{
			throw new IllegalArgumentException("tokenValiditySeconds has to be >= 0");
		}
		this.tokenValiditySeconds = tokenValiditySeconds;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.secureportaladdon.services.B2BRegistrationService#getEmailAddressesOfEmployees(java.util.List)
	 */
	@Override
	public List<EmailAddressModel> getEmailAddressesOfEmployees(final List<EmployeeModel> employees)
	{

		final List<EmailAddressModel> emails = new ArrayList<>();

		for (final EmployeeModel employee : employees)
		{
			for (final AddressModel address : Lists.newArrayList(employee.getAddresses()))
			{
				if (BooleanUtils.isTrue(address.getContactAddress()))
				{
					if (StringUtils.isNotBlank(address.getEmail()))
					{
						final EmailAddressModel emailAddress = emailService.getOrCreateEmailAddressForEmail(address.getEmail(),
								employee.getName());
						emails.add(emailAddress);
					}
					break;
				}
			}
		}

		return emails;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.secureportaladdon.services.B2BRegistrationService#getRegisteredEmails(java.lang.String)
	 */
	//custom-code starts
	@Override
	public B2BUnitModel getB2BUnitByCustomerNumber(final String customerNumber)
			throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{

		if (customerNumber != null && customerNumber != "")
		{
			final B2BUnitModel b2bUnitModel = registrationDao.getB2BUnitByUid(customerNumber);
			if (b2bUnitModel != null)
			{
				return b2bUnitModel;
			}
			else
			{
				throw new NoSuchB2BUnitFoundException("No B2BUnit is found with the given Customer Number.");
			}
		}
		else
		{
			throw new MissingParameterFieldException("The parameter field should not be empty or null.");
		}
	}

	//custom-code ends
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.secureportaladdon.services.B2BRegistrationService#registerCustomer(de.hybris.platform.b2b.model
	 * .B2BCustomerModel)
	 */
	//custom-code start
	@Override
	public void registerCustomer(final B2BCustomerModel b2bCustomerModel)
	{
		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken data = new SecureToken(b2bCustomerModel.getUid(), timeStamp);
		final String token = getSecureTokenService().encryptData(data);
		b2bCustomerModel.setToken(token);
		b2bCustomerModel.setType(null);
		getModelService().save(b2bCustomerModel);
		getEventService().publishEvent(initializeEvent(new SalonCustomerRegisterEvent(token), b2bCustomerModel));
	}


	//initialize event for sending token link as email to registered customer
	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event,
			final B2BCustomerModel b2bCustomerModel)
	{

		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(b2bCustomerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}

	
	//initialize event for sending email to sales representative
	protected AbstractEmployeeNotificationEvent initializeEvent(final AbstractEmployeeNotificationEvent event,
			final B2BCustomerModel b2bCustomerModel, final EmployeeModel emp)
	{
		final AbstractEmployeeNotificationEvent abtractempevent = (AbstractEmployeeNotificationEvent) this.initializeEvent(event,
				b2bCustomerModel);
		abtractempevent.setEmployee(emp);
		return abtractempevent;
	}
	
	
	//initialize prospective customer event
	protected AbstractProspectiveCustomerNotificationEvent initializeEvent(final AbstractProspectiveCustomerNotificationEvent event,
	final ProspectiveCustomerRegModel prospectiveCustomer){
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setProspectiveCustomer(prospectiveCustomer);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}
	
	
	//initialize customer notification event
	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event,
			final CustomerModel newCustomer)
	{
		
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(newCustomer);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}


	//check the expiry of the token link
	@Override
	public boolean checkTokenExpiry(final String token) throws TokenInvalidatedException
	{
		Assert.hasText(token, "The field [token] cannot be empty");
		/* Assert.hasText(newPassword, "The field [newPassword] cannot be empty"); */

		final SecureToken data = getSecureTokenService().decryptData(token);
		if (getTokenValiditySeconds() > 0L)
		{
			final long delta = new Date().getTime() - data.getTimeStamp();
			if (delta / 1000 > getTokenValiditySeconds())
			{
				throw new IllegalArgumentException("token expired");
			}
		}

		return true;
	}




	// save the user who has registered online with salon
	@Override
	public void saveB2BCustomer(final String token, final String pwd, final String titleCode, final String name)
			throws TokenInvalidatedException, CustomerAlreadyExistsException, NoSalesRepFoundException
	{
		final SecureToken data = getSecureTokenService().decryptData(token);
		final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);
		if (customer == null)
		{
			throw new IllegalArgumentException("user for token not found");

		}
		if (customer instanceof B2BCustomerModel)
		{
			
			final B2BCustomerModel b2bcustomer = (B2BCustomerModel) customer;
			
			if (!token.equals(b2bcustomer.getToken()))
			{
				throw new TokenInvalidatedException();
			}
			if(b2bcustomer.getType()!=null && b2bcustomer.getType() == CustomerType.REGISTERED){
				throw new CustomerAlreadyExistsException("Customer already registered");
			}
			b2bcustomer.setToken(null);
			b2bcustomer.setType(CustomerType.REGISTERED);
			b2bcustomer.setPassword(pwd);
			b2bcustomer.setName(name);

			customer.setLoginDisabled(false);
			getUserService().setPassword(data.getData(), pwd, getPasswordEncoding());
			getModelService().save(b2bcustomer);
			//get the sales representative belonging to the registered customer
			// get the sale rep for this customer
			final EmployeeModel salesRep = salonB2BCustomerService.getSalesRepresentative(b2bcustomer);
			/*
			 * Send Email To B2BCustomer and his Sales Representative respectively
			 */
			if (salesRep == null)
			{
				throw new NoSalesRepFoundException("employee for token not found");

			}

			getEventService().publishEvent(initializeEvent(new SalonB2BCustomerRegistrationEvent(), b2bcustomer));
			// initialize event is of customer type
			getEventService().publishEvent(
					initializeEvent(new SendCustomerRegNotificationToSalesRepEvent(b2bcustomer, salesRep), b2bcustomer, salesRep));

		}

	}
	
	
	// add new prospective customer
	@Override
	public void saveProspectiveCustomer(ProspectiveCustomerRegModel prospectiveCustomer)throws DuplicateUidException, CustomerAlreadyExistsException{
		
		// check if there is a customer with the same emailId;
			String uid = prospectiveCustomer.getEmail();
			final boolean userExists = userService.isUserExisting(uid);
			if (userExists)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(String.format("user with uid '%s' already exists!", prospectiveCustomer.getEmail()));
				}
				throw new CustomerAlreadyExistsException(String.format("User with uid '%s' already exists!", prospectiveCustomer.getEmail()));
			}
			else
			{
			modelService.save(prospectiveCustomer.getAddress());
			modelService.save(prospectiveCustomer);
			LOG.debug("Triggering the ProspectiveCustomerRegistrationEvent to generate the email ");
			getEventService()
			.publishEvent(initializeEvent(new ProspectiveCustomerRegistrationEvent(prospectiveCustomer),prospectiveCustomer));
			LOG.debug("Triggered the email generation event successfully");
		}
		
		
	}

	//custom-code ends
}