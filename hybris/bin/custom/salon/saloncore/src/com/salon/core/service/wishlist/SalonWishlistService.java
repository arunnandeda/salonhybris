package com.salon.core.service.wishlist;

import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public interface SalonWishlistService extends Wishlist2Service {

	public void removeWishlist(String  name,UserModel user);
	
	public Wishlist2Model getWishListById(String wishListName,UserModel user) throws NoSuchWishListFoundException;
	
	public Wishlist2Model getWishListByName(String wishListName,UserModel user);
	
	public Wishlist2Model editWishlistName(String newWishListName,String wishListId,UserModel currentUser) throws WishListAlreadyExistException;

	public void removeAllProductFromWishList(Wishlist2Model wishlist);
}