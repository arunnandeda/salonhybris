package com.salon.core.service.wishlist.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.salon.core.dao.wishlist.SalonWishlistDao;
import com.salon.core.service.wishlist.SalonWishlistService;
import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class SalonWishlistServiceImpl extends DefaultWishlist2Service implements SalonWishlistService {

	private static final Logger LOG = LoggerFactory.getLogger(SalonWishlistServiceImpl.class);

	@Autowired
	private SalonWishlistDao salonWishlistDao;

	/**
	 * It will Remove customer particular wishList.
	 * 
	 * @param wishListName
	 *            wishListName
	 * @param user
	 *            currentUser
	 * @return void
	 * 
	 */

	@Override
	public void removeWishlist(String wishListName, UserModel user) {

		validateParameterNotNull(wishListName, "wishListName cannot be null");
		validateParameterNotNull(user, "wish name cannot be null");
		LOG.debug("service getting wish list by name");
		Wishlist2Model wishlist2Model = salonWishlistDao.findWishlist2ByName(wishListName, user);

		if (wishlist2Model != null) {
			LOG.debug("Removing wish list " + wishlist2Model.getName());
			try {
				this.getModelService().remove(wishlist2Model);
			} catch (final ModelRemovalException e) {
				LOG.error("[Remove wishList]" + e);
			}
		}
	}

	/**
	 * Getting Wish list By id(pk)
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * @param currentUser
	 *            currentUser
	 * @return Wishlist2Model(null or Wishlist2Model) If wishList not found for
	 *         id then it will return null otherwise return wishlist model
	 */
	@Override
	public Wishlist2Model getWishListById(String wishListId, UserModel currentUser)
			throws NoSuchWishListFoundException {

		LOG.debug("service getting wish list by ID(pk)");
		validateParameterNotNull(currentUser, "B2B Customer Model cannot be null");
		validateParameterNotNull(wishListId, "wish List cannot be null");
		Wishlist2Model wishlist2Model = salonWishlistDao.findWishlist2ById(wishListId, currentUser);

		if (wishlist2Model != null) {
			return wishlist2Model;
		} else {
			throw new NoSuchWishListFoundException("No wishList  is found with the given  id.");
		}

	}

	/**
	 * Editing name of Wish list
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * @param currentUser
	 *            currentUser
	 * @return Wishlist2Model(null or Wishlist2Model) Here we are renaming the
	 *         existing wishlist name. if editwishList name already exist then
	 *         it will throw exception other wise it will return Wishlist2Model
	 *         or null
	 */

	@Override
	public Wishlist2Model editWishlistName(String newWishListName, String wishListId, UserModel currentUser)
			throws WishListAlreadyExistException {

		LOG.debug("wish list for edit name " + newWishListName);
		validateParameterNotNull(currentUser, "B2B Customer Model cannot be null");
		validateParameterNotNull(newWishListName, "wish List cannot be null");
		validateParameterNotNull(wishListId, "wish List cannot be null");

		Wishlist2Model wish2Model = salonWishlistDao.findWishlist2ByName(newWishListName, currentUser);

		if (null != wish2Model) {
			throw new WishListAlreadyExistException("wish list already Exist");
		} else {
			Wishlist2Model wishlist2Model = salonWishlistDao.findWishlist2ById(wishListId, currentUser);
			if (null != wishlist2Model) {

				LOG.info("Renaming the wishlist.." + wishlist2Model.getName() + " to " + newWishListName);
				wishlist2Model.setName(newWishListName);
				this.getModelService().save(wishlist2Model);

				return wishlist2Model;
			}
		}
		return null;
	}

	/**
	 * removing all wishList entry from wish
	 * 
	 * @param wishlist
	 *            wishlist2model
	 * 
	 * @return void
	 */
	@Override
	public void removeAllProductFromWishList(Wishlist2Model wishlist) {
		LOG.debug("removing  all wishlist Entry");
		validateParameterNotNull(wishlist, "wishlist model cannot be null");
		if (null != wishlist) {
			List<Wishlist2EntryModel> entries = new ArrayList<>();
			List<Wishlist2EntryModel> wishEntries = new ArrayList<>(wishlist.getEntries());
			entries.addAll(wishEntries);
			wishEntries.removeAll(entries);
			wishlist.setEntries(wishEntries);
			this.getModelService().save(wishlist);
		}
	}

	/**
	 * Getting wishList by name When customer create new wish list that time
	 * checking if given name is alredy exists or not
	 * 
	 * @param wishListName
	 *            wishListName
	 * @param currentUser
	 *            currentUser
	 * @return Wishlist2Model(null or Wishlist2Model)
	 */
	@Override
	public Wishlist2Model getWishListByName(String wishListName, UserModel currentUser) {
		LOG.debug("service getting wish list by name");
		validateParameterNotNull(currentUser, "B2B Customer Model cannot be null");
		validateParameterNotNull(wishListName, "wish name cannot be null");

		Wishlist2Model wishlist2Model = salonWishlistDao.findWishlist2ByName(wishListName, currentUser);

		if (wishlist2Model != null) {
			return wishlist2Model;
		}
		return null;
	}

}
