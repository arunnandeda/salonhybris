package com.salon.core.wishlist.exception;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * @author A.D.
 *
 */
public class NoSuchWishListFoundException extends BusinessException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public NoSuchWishListFoundException(final String message)
	{
		super(message);
	}

}
