package com.salon.core.wishlist.exception;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * @author A.D.
 *
 */
public class WishListAlreadyExistException extends BusinessException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public WishListAlreadyExistException(final String message)
	{
		super(message);
	}

}
