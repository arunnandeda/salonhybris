/**
 *
 */
package com.salon.core.secureportaladdon.registration.services.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import com.salon.core.secureportaladdon.services.impl.DefaultB2BRegistrationService;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;


/**
 * @author 1DigitalS
 *
 */
@IntegrationTest
public class SalonB2BRegistrationServiceIntegrationTest extends ServicelayerTest
{
	@Resource
	private DefaultB2BRegistrationService b2bRegistrationService;

	@Resource
	private ModelService modelService;

	@Resource
	private SessionService sessionService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	private B2BUnitModel b2bUnitModel;

	private static final String SAP_Customer_Number_1 = "12345";
	private static final String SAP_Customer_Name_1 = "Salon Bangalore";
	private static final String Customer_Number = "12345";
	private static final String Customer_Number_2 = "123456";

	private static final String TEST_BASESITE_UID = "testSite";

	private static final String USER = "admin";

	public ModelService getModelService()
	{
		return modelService;
	}

	@Before
	public void setUp()
	{
		sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(TEST_BASESITE_UID), false);
				final UserModel user = userService.getUserForUID(USER);
				userService.setCurrentUser(user);
				b2bUnitModel = modelService.create(B2BUnitModel.class);
				b2bUnitModel.setUid(SAP_Customer_Number_1);
				b2bUnitModel.setName(SAP_Customer_Number_1);
				modelService.save(b2bUnitModel);

			}
		});

	}

	@Test
	public void getRegisterEmails() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		final B2BUnitModel b2bUnitModelTest = b2bRegistrationService.getB2BUnitByCustomerNumber(Customer_Number);
		assertEquals(b2bUnitModel, b2bUnitModelTest);
	}

	@Test(expected = MissingParameterFieldException.class)
	public void getRegisterEmailsExpectExceptions() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		final B2BUnitModel b2bUnitModelTest = b2bRegistrationService.getB2BUnitByCustomerNumber(null);

	}

	@Test(expected = NoSuchB2BUnitFoundException.class)
	public void getRegisterEmailsExpectedExceptions() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		final B2BUnitModel b2bUnitModelTest = b2bRegistrationService.getB2BUnitByCustomerNumber(Customer_Number_2);

	}

	@After
	public void afterTest()
	{
		if (b2bUnitModel != null)
		{
			modelService.remove(b2bUnitModel);
		}
	}
}
