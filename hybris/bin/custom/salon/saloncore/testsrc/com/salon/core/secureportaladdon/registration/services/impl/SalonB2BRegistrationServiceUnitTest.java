/**
 *
 */
package com.salon.core.secureportaladdon.registration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;


import org.junit.Before;
import org.junit.Test;

import com.salon.core.secureportaladdon.dao.B2BRegistrationDao;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.core.secureportaladdon.services.impl.DefaultB2BRegistrationService;


/**
 * @author 1DigitalS
 *
 */
@UnitTest
public class SalonB2BRegistrationServiceUnitTest
{
	private DefaultB2BRegistrationService defaultB2BRegistrationService;
	private B2BRegistrationDao b2bRegistrationDao;

	B2BUnitModel b2bUnitModel;

	private static final String SAP_Customer_Number_1 = "12345";
	private static final String SAP_Customer_Name_1 = "Salon Bangalore";
	private static final String Customer_Number = "12345";


	@Before
	public void setUp()
	{
		defaultB2BRegistrationService = new DefaultB2BRegistrationService();
		defaultB2BRegistrationService.setRegistrationDao(b2bRegistrationDao = mock(B2BRegistrationDao.class));
		b2bUnitModel = createB2BUnitModel(SAP_Customer_Number_1, SAP_Customer_Name_1);
	}

	@Test
	public void testGetB2BUnitWithCustomerNumer1() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		when(b2bRegistrationDao.getB2BUnitByUid(Customer_Number)).thenReturn(b2bUnitModel);
		final B2BUnitModel b2bUnitModel = defaultB2BRegistrationService.getB2BUnitByCustomerNumber(Customer_Number);

		assertEquals("12345", b2bUnitModel.getUid());
		assertEquals("Salon Bangalore", b2bUnitModel.getName());
	}

	@Test(expected = MissingParameterFieldException.class)
	public void getB2BUnitExceptedExceptionTest() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		when(b2bRegistrationDao.getB2BUnitByUid(Customer_Number)).thenReturn(b2bUnitModel);
		final B2BUnitModel b2bUnitModel = defaultB2BRegistrationService.getB2BUnitByCustomerNumber("");
	}

	@Test(expected = NoSuchB2BUnitFoundException.class)
	public void getB2BUnitExpectedExceptionTest2() throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		when(b2bRegistrationDao.getB2BUnitByUid(Customer_Number)).thenReturn(null);
		final B2BUnitModel b2bUnitModel = defaultB2BRegistrationService.getB2BUnitByCustomerNumber(Customer_Number);
	}

	private B2BUnitModel createB2BUnitModel(final String customerNumber, final String name)
	{
		final B2BUnitModel b2bUnitModel = new B2BUnitModel();
		b2bUnitModel.setUid(customerNumber);
		b2bUnitModel.setName(name);

		return b2bUnitModel;
	}
}
