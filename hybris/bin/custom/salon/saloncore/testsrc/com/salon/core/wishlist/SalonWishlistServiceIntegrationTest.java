
package com.salon.core.wishlist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Before;
import org.junit.Test;

import com.salon.core.service.wishlist.SalonWishlistService;
import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;

@IntegrationTest
public class SalonWishlistServiceIntegrationTest extends ServicelayerTransactionalTest {
	private static final Logger LOG = LoggerFactory.getLogger(SalonWishlistServiceIntegrationTest.class);
	private static final String FIRST_WISH_LIST_NAME = "myWishListFirst";
	private static final String SECOND_WISH_LIST_NAME = "myWishListSecond";
	private static final String DEFAULT_DESCRIPTION = "My default wishlist";

	@Resource
	private SalonWishlistService salonWishlistService;
	@Resource
	private UserService userService;
	@Resource
	private ProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private BaseSiteService baseSiteService;

	private ProductModel product1;
	private ProductModel product2;
	private B2BCustomerModel currentUser;

	@Before
	public void setUp() throws Exception {
		createCoreData();
		createHardwareCatalog();
		importCsv("/test/B2Busergroups.impex", "UTF-8");

		product1 = productService.getProductForCode("HW2300-2356");
		product2 = productService.getProductForCode("HW2300-4121");
		currentUser = userService.getUserForUID("salontest@test.com", B2BCustomerModel.class);
		userService.setCurrentUser(currentUser);
		LOG.info("Current User is...." + currentUser.getName());
	}

	/* Getting wishList By Id (Success) */
	@Test
	public void testgetWishListById() throws NoSuchWishListFoundException {
		/* First Creating wishList model */
		final Wishlist2Model wishlist = salonWishlistService.createWishlist(currentUser, FIRST_WISH_LIST_NAME,
				DEFAULT_DESCRIPTION);

		// Getting wishList by Id
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(wishlist.getPk().toString(),
				currentUser);

		// If wish List not null means success
		assertNotNull("Get wishList by  id success", wishlist2Model);

	}

	/* Getting wishList By Id (Fail) passing id as null */
	@Test(expected = IllegalArgumentException.class)
	public void testgetWishListByIdFail() throws NoSuchWishListFoundException {
		/* First Creating wishList model */
		final Wishlist2Model wishlist = salonWishlistService.createWishlist(currentUser, FIRST_WISH_LIST_NAME,
				DEFAULT_DESCRIPTION);

		// Getting wishList by Id
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(null, currentUser);

		LOG.info("throwing Exception.." + wishlist2Model);
		// If wish List is null means success
		assertNull("Get wishList by  id fail throwing exception", wishlist2Model);

	}

	/* Getting wishList By Id (Fail) */
	@Test(expected = NoSuchWishListFoundException.class)
	public void testgetWishListByIdFailThrowException() throws NoSuchWishListFoundException {
		/* First Creating wishList model */
		final Wishlist2Model wishlist = salonWishlistService.createWishlist(currentUser, FIRST_WISH_LIST_NAME,
				DEFAULT_DESCRIPTION);

		// Getting wishList by Id
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListById("12444", currentUser);

		LOG.info("NoSuchWishListFoundException.." + wishlist2Model);
		// If wish List not null means success
		assertNull("Get wishList by  id fail throwing NoSuchWishListFoundException", wishlist2Model);

	}

	/***************************
	 * Remove WishList success,failure and exception start
	 ******************************/
	/* Removing wishList (success) */
	@Test
	public void testRemoveWishlistSuccess() {

		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/*
		 * salonWishlistService is removing wishList by passing current customer
		 * and wishLis name(FIRST_WISH_LIST_NAME)
		 */
		salonWishlistService.removeWishlist(FIRST_WISH_LIST_NAME, currentUser);

		/*
		 * salonWishlistService will give all wishList for current logged in
		 * customer
		 */
		List<Wishlist2Model> wishList2ModelList = salonWishlistService.getWishlists();

		/* Checking wishList remove or not.If remove then it will be true */
		assertEquals("1 wishList is there for current customer", 1, wishList2ModelList.size());

	}

	/* Removing wishList (Fail) */
	@Test
	public void testRemoveWishlistFail() {

		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/*
		 * salonWishlistService is removing wishList by passing current customer
		 * and wishLis name(FIRST_WISH_LIST_NAME)
		 */
		salonWishlistService.removeWishlist("Test", currentUser);

		/*
		 * salonWishlistService will give all wishList for current logged in
		 * customer
		 */
		List<Wishlist2Model> wishList2ModelList = salonWishlistService.getWishlists();

		/* Checking wishList remove or not.If remove then it will be true */
		assertEquals("wishList not remove", 2, wishList2ModelList.size());

	}

	/* Removing wishList (fail) */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveWishlistFailThrowException() {

		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/*
		 * salonWishlistService is removing wishList by passing current customer
		 * and wishLis name(FIRST_WISH_LIST_NAME)
		 */
		salonWishlistService.removeWishlist(null, currentUser);

		/*
		 * salonWishlistService will give all wishList for current logged in
		 * customer
		 */
		List<Wishlist2Model> wishList2ModelList = salonWishlistService.getWishlists();

		/*
		 * Checking wishList remove or not here size will be 2 only if not
		 * removed
		 */
		assertEquals("2 wishList is there for current customer", 2, wishList2ModelList.size());

	}

	/***************************
	 * Remove WishList success,failure and exception end
	 ******************************/

	/***********************************
	 * Get wishlist by name (success,failure end exception) start
	 *******************************/
	/* Get wishList by name (Success) */
	@Test
	public void testGetWishListByName() {
		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		// Getting wishList by Name
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(FIRST_WISH_LIST_NAME, currentUser);

		// If wish List not null means success
		assertNotNull("Get wishList by  name success", wishlist2Model);

	}

	/* Get wishList by name (Fail).wishList not found */
	@Test
	public void testGetWishListByNameFail() {

		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		// Getting wishList by Name
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName("Test", currentUser);

		// If wish List null means success
		assertNull("Get wishList by  name Fail", wishlist2Model);

	}

	/* Get wishList by name (Fail).wishList not found */
	@Test(expected = IllegalArgumentException.class)
	public void testGetWishListByNameFailParameterNull() {

		/* First Creating two wishList for current customer */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		// Getting wishList by Name
		final Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(null, currentUser);

		// If wish List null means success
		assertNotNull("Get wishList by  name Fail", wishlist2Model);

	}

	/***********************************
	 * Get wishlist by name (success,failure end exception) start
	 *******************************/

	/***********************************
	 * Renaming wishlist name,success,failure exception start
	 *******************************/
	/* Renaming wishList success */
	@Test
	public void testEditWishlistName() throws WishListAlreadyExistException {
		/* First Creating two wishList for current customer */
		Wishlist2Model wishlist2ModelFirst = createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		Wishlist2Model wishlist2ModelSecond = createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		Wishlist2Model NewWishlist2Model = salonWishlistService.editWishlistName("NewWishList",
				wishlist2ModelFirst.getPk().toString(), currentUser);

		assertEquals("Edit wishList name success Fully", "NewWishList", NewWishlist2Model.getName());

	}

	/* Renaming wishList fail */
	@Test(expected = WishListAlreadyExistException.class)
	public void testEditWishlistNameFailThrowException() throws WishListAlreadyExistException {
		/*
		 * Creating two wishList by passing current user wishList Name and
		 * description
		 */
		Wishlist2Model wishlist2ModelFirst = createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/* Editing the wishList name */
		salonWishlistService.editWishlistName(SECOND_WISH_LIST_NAME, wishlist2ModelFirst.getPk().toString(),
				currentUser);

	}

	/* Renaming wishList fail lets renaming wishList not exists */
	@Test
	public void testEditWishlistNameIdNotFoundFail() throws WishListAlreadyExistException {
		/* First Creating two wishList for current customer */
		Wishlist2Model wishlist2ModelFirst = createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/* Editing the wishList name */
		Wishlist2Model NewWishlist2Model = salonWishlistService.editWishlistName("NewWishList", "23456721",
				currentUser);

		/*
		 * It will give null if not edited
		 */
		assertNull(NewWishlist2Model);

	}

	/* Renaming wishList fail because all parameter are null */
	@Test(expected = IllegalArgumentException.class)
	public void testEditWishlistNameIdFailParameterNull() throws WishListAlreadyExistException {
		/*
		 * Creating two wishList by passing current user wishList Name and
		 * description
		 */
		createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);
		createWishlist(currentUser, SECOND_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/* Editing the wishList name throw Exception */
		salonWishlistService.editWishlistName(null, null, currentUser);

	}

	/***********************************
	 * Renaming wishlist name (success,failure exception) end
	 *******************************/

	/***********************************
	 * Remove all product from wishList (success,failure) start
	 *******************************/
	/* Removing all product from wishList Success */
	@Test
	public void testRemoveAllProductFromWishList() {

		/*
		 * Creating wishList by passing current user wishList Name and
		 * description
		 */
		final Wishlist2Model wishlist = createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/* Adding two product In wisList */

		salonWishlistService.addWishlistEntry(wishlist, product1, Integer.valueOf(1), Wishlist2EntryPriority.MEDIUM,
				"I like it");
		salonWishlistService.addWishlistEntry(wishlist, product2, Integer.valueOf(2), Wishlist2EntryPriority.HIGH,
				"Must have");

		// Removing all product from wishList
		salonWishlistService.removeAllProductFromWishList(wishlist);
		final Wishlist2Model wishlistModel = salonWishlistService.getWishListByName(FIRST_WISH_LIST_NAME, currentUser);

		// Success if no product is there
		assertEquals("No product is there", 0, wishlistModel.getEntries().size());

	}

	/* Removing all product from wishList fail.Throwing exception */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveAllProductFromWishListThrowException() {

		/*
		 * Creating wishList by passing current user wishList Name and
		 * description
		 */
		final Wishlist2Model wishlist = createWishlist(currentUser, FIRST_WISH_LIST_NAME, DEFAULT_DESCRIPTION);

		/* Adding two product In wisList */

		salonWishlistService.addWishlistEntry(wishlist, product1, Integer.valueOf(1), Wishlist2EntryPriority.MEDIUM,
				"I like it");
		salonWishlistService.addWishlistEntry(wishlist, product2, Integer.valueOf(2), Wishlist2EntryPriority.HIGH,
				"Must have");

		/* Removing all product it will throw Exception */
		salonWishlistService.removeAllProductFromWishList(null);

	}

	/***********************************
	 * Remove all product from wishList (success,failure) end
	 *******************************/

	/* Create wishList */
	private Wishlist2Model createWishlist(UserModel user, String wishListName, String description) {
		final Wishlist2Model wishlist = salonWishlistService.createWishlist(currentUser, wishListName, description);

		return wishlist;
	}

}
