package com.salon.core.wishlist;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.salon.core.dao.wishlist.SalonWishlistDao;
import com.salon.core.service.wishlist.impl.SalonWishlistServiceImpl;
import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

@UnitTest
public class SalonWishlistServiceJunitTest {

	private static final String WISH_LIST_NAME = "MYWISHLIST";
	private static final String NEW_WISH_LIST_NAME = "MYNEWWISHLIST";
	private static final String WISH_LIST_ID = "8796322497213";
	private static final String PRODUCT_CODE = "1111";

	@InjectMocks
	private SalonWishlistServiceImpl salonWishlistService = new SalonWishlistServiceImpl();

	@Mock
	private SalonWishlistDao salonWishlistDao;

	@Mock
	private ModelService modelService;

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}

	private Wishlist2Model wishlist2Model;

	private B2BCustomerModel customerModel;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		customerModel = currentCustomer("test@gmail.com");
		salonWishlistService.setModelService(modelService);

		wishlist2Model = getWishlistModel();
	}

	/***************************
	 * Remove WishList success,failure and exception start
	 ******************************/
	/* Removing wishList (success) */
	@Test
	public void testRemoveWishlistSuccess() {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(wishlist2Model);

		/* Mocking the modelSevice for remove */
		Mockito.doNothing().when(this.getModelService()).remove(any());

		// Removing wishList by passing wishList Name and current customer model
		salonWishlistService.removeWishlist(WISH_LIST_NAME, customerModel);

		// Verify wishList Remove or not.
		Mockito.verify(this.getModelService(), Mockito.times(1)).remove(wishlist2Model);
	}

	/* Removing wishList Fail. wishList not found */
	@Test
	public void testRemoveWishlistFail() {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model as null means given wihsList
		 * not found
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(null);

		/* Mocking the modelSevice for remove */
		Mockito.doNothing().when(modelService).remove(any());

		// Removing wishList by passing wishList Name and current customer model
		salonWishlistService.removeWishlist(WISH_LIST_NAME, customerModel);

		// Verify wishList Remove or not.Mock 0 time means not removed.
		Mockito.verify(this.getModelService(), Mockito.times(0)).remove(wishlist2Model);
	}

	/*
	 * Removing wishList (Fail) if customer or wishList name are empty throw
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveWishlistFailForPassingNull() {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model as null means given wihsList
		 * not found
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(wishlist2Model);

		// Removing wishList by passing wishList Name and current customer model
		salonWishlistService.removeWishlist(null, null);

	}

	/************************
	 * Remove WishList success,failure and exception End
	 *******************************************/

	/***********************************
	 * Fetch wishlist by Id(pk),success,failure exception start
	 *******************************/

	/* Getting wishList by id(Success) */
	@Test
	public void testGetWishListById() throws NoSuchWishListFoundException {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(wishlist2Model);

		/* Getting wish List by Id by passing wishList id and customerModel */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(WISH_LIST_ID, customerModel);

		/* If not null means success */
		assertNotNull("WishList found for given id", wishlist2Model);

	}

	/* Getting wishList by id(Failed) means wishList not found */
	@Test(expected = NoSuchWishListFoundException.class)
	public void testGetWishListByIdFailException() throws NoSuchWishListFoundException {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(null);

		/* Getting wish List by Id by passing wishList id and customerModel */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(WISH_LIST_ID, customerModel);

		// Success if wishlist2Model is null
		assertNull("Wish not for given found ", wishlist2Model);

	}

	/* Getting wishList by id(Failed) */
	@Test(expected = IllegalArgumentException.class)
	public void testGetWishListByIdFail() throws NoSuchWishListFoundException {

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(wishlist2Model);

		/*
		 * Getting wish List by Id by passing wishList id and customerModel as
		 * null
		 */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(null, null);

		/* Success if wishlist2Model is null */
		assertNull("Wish not found throwing exception ", wishlist2Model);

	}

	/***********************************
	 * Fetch wishlist by Id(pk),success,failure exception end
	 *******************************/

	/***********************************
	 * Renaming wishlist name,success,failure exception start
	 *******************************/
	/* Renaming wishList success */
	@Test
	public void testEditWishlistName() throws WishListAlreadyExistException {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model as null means given wishsList
		 * not found.you can give newName
		 */
		when(salonWishlistDao.findWishlist2ByName(NEW_WISH_LIST_NAME, customerModel)).thenReturn(null);

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(wishlist2Model);

		/*
		 * Renaming wishList name by passing the NewwishList
		 * name(WISH_LIST_NAME) and wishList Id (WISH_LIST_ID) that are wishList
		 * to rename and current customer
		 */
		salonWishlistService.editWishlistName(NEW_WISH_LIST_NAME, WISH_LIST_ID, customerModel);

		/* verify saving or not. If times 1 means saved success */
		Mockito.verify(this.getModelService(), Mockito.times(1)).save(wishlist2Model);

	}

	/* Renaming wishList fail */
	@Test(expected = WishListAlreadyExistException.class)
	public void testEditWishlistNameFail() throws WishListAlreadyExistException {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(wishlist2Model);

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(wishlist2Model);

		/* Mocking the modelSevice for save as fake call */
		Mockito.doNothing().when(modelService).save(any());

		/*
		 * Renaming the wihsList it will fail because you are giving rename as
		 * same as existing name
		 */
		salonWishlistService.editWishlistName(WISH_LIST_NAME, WISH_LIST_ID, customerModel);

	}

	/* Renaming wishList fail lets reanming wishList not exists */
	@Test
	public void testEditWishlistNameIdNotFoundFail() throws WishListAlreadyExistException {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(null);

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(null);

		/* Mocking the modelSevice for save as fake call */
		Mockito.doNothing().when(modelService).save(any());

		/*
		 * Renaming the wihsList it will fail because you are giving rename as
		 * same as existing name
		 */
		salonWishlistService.editWishlistName(WISH_LIST_NAME, WISH_LIST_ID, customerModel);

		Mockito.verify(this.getModelService(), Mockito.times(0)).save(wishlist2Model);
	}

	/* Renaming wishList fail because all parameter are null */
	@Test(expected = IllegalArgumentException.class)
	public void testEditWishlistNameIdFailParameterNull() throws WishListAlreadyExistException {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(null);

		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * id So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ById(WISH_LIST_ID, customerModel)).thenReturn(wishlist2Model);

		/* Mocking the modelSevice for save as fake call */
		Mockito.doNothing().when(modelService).save(any());

		/*
		 * Renaming the wihsList it will fail because you are giving rename as
		 * same as existing name
		 */
		salonWishlistService.editWishlistName(null, null, null);

	}

	/***********************************
	 * Renaming wishlist name (success,failure exception) end
	 *******************************/

	/***********************************
	 * Get wishlist by name (success,failure end exception) start
	 *******************************/
	/* Get wishList by name (Success) */
	@Test
	public void testGetWishListByName() {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(wishlist2Model);

		/* Getting wishList by passing wishList name and currentCustomer */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(WISH_LIST_NAME, customerModel);

		/* Success if both are Equals */
		assertEquals(wishlist2Model.getName(), WISH_LIST_NAME);

	}

	/* Get wishList by name (Fail).wishList not found */
	@Test
	public void testGetWishListByNameFail() {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model if not found
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(null);

		/* Getting wishList by passing wishList name and currentCustomer */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(WISH_LIST_NAME, customerModel);

		/* Success if wishList not found */
		assertNull("Wish not found  for given name", wishlist2Model);

	}

	/* Get wishList by name (Fail).wishList not found */
	@Test(expected = IllegalArgumentException.class)
	public void testGetWishListByNameFailParameterNull() {
		/*
		 * Mocking the Dao Layer and passing current customerModel and wishList
		 * Name So it will return wishList2Model if not found
		 */
		when(salonWishlistDao.findWishlist2ByName(WISH_LIST_NAME, customerModel)).thenReturn(wishlist2Model);

		/*
		 * Getting wishList by passing wishList name and currentCustomer as null
		 * for validation
		 */
		Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(null, null);

	}

	/***********************************
	 * Get wishlist by name (success,failure end exception) start
	 *******************************/

	/***********************************
	 * Remove all product from wishList (success,failure) start
	 *******************************/
	/* Removing all product from wishList Success */
	@Test
	public void testRemoveAllProductFromWishList() {

		/* Mocking the modelSerice */
		Mockito.doNothing().when(modelService).save(any());

		/* Removing all product */
		salonWishlistService.removeAllProductFromWishList(wishlist2Model);

		Mockito.verify(this.getModelService(), Mockito.times(1)).save(any());

	}

	/* Removing all product from wishList fail.Throwing exception */
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveAllProductFromWishListThrowException() {

		/* Removing all product it will throw Exception */
		salonWishlistService.removeAllProductFromWishList(null);

	}

	/***********************************
	 * Remove all product from wishList (success,failure) end
	 *******************************/

	/* Set Up wishList Model */
	private Wishlist2Model getWishlistModel() {
		Wishlist2Model wishlistModel = new Wishlist2Model();

		final List<Wishlist2EntryModel> entries = new ArrayList<>();

		final Wishlist2EntryModel wishlist2EntryModel1 = new Wishlist2EntryModel();

		final ProductModel productModel1 = createProductModel();

		wishlist2EntryModel1.setProduct(productModel1);

		entries.add(wishlist2EntryModel1);

		wishlistModel.setEntries(entries);
		wishlistModel.setName(WISH_LIST_NAME);

		return wishlistModel;

	}

	/* Set Up CurrentCutomer Model */
	private B2BCustomerModel currentCustomer(String uid) {

		final B2BCustomerModel b2bCustomer = new B2BCustomerModel();

		b2bCustomer.setName("TestName");
		b2bCustomer.setUid(uid);
		final B2BUnitModel b2bUnitModel = createB2BUnitModel();
		b2bCustomer.setDefaultB2BUnit(b2bUnitModel);
		return b2bCustomer;

	}

	/* Set Up B2bUnit Model */
	private B2BUnitModel createB2BUnitModel() {
		final B2BUnitModel b2bUnitModel = new B2BUnitModel();
		b2bUnitModel.setUid("12321");
		b2bUnitModel.setName("Salonunit");
		return b2bUnitModel;
	}

	/* Set Up Product Model */
	private ProductModel createProductModel() {

		final ProductModel productModel = new ProductModel();
		productModel.setCode(PRODUCT_CODE);
		productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		return productModel;
	}

}
