package com.salon.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.salon.core.process.model.StorefrontEmployeeNotificationProcessModel;
import com.salon.core.resolution.SalonEmployeeEmailResolutionService;
import com.salon.facades.account.data.EmployeeData;


public class EmployeeNotificationEmailContext extends AbstractEmailContext<StorefrontEmployeeNotificationProcessModel>
{

	private Converter<UserModel, EmployeeData> employeeConverter;
	private EmployeeData employeeData;

	@Autowired
	SalonEmployeeEmailResolutionService salonEmployeeEmailResolutionService;

	public EmployeeData getEmployeeData()
	{
		return employeeData;
	}

	public void setEmployeeData(final EmployeeData employeeData)
	{
		this.employeeData = employeeData;
	}

	public Converter<UserModel, EmployeeData> getEmployeeConverter()
	{
		return employeeConverter;
	}

	@Required
	public void setEmployeeConverter(final Converter<UserModel, EmployeeData> employeeConverter)
	{
		this.employeeConverter = employeeConverter;
	}

	@Override
	protected BaseSiteModel getSite(final StorefrontEmployeeNotificationProcessModel businessProcessModel)
	{

		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final StorefrontEmployeeNotificationProcessModel businessProcessModel)
	{

		return businessProcessModel.getCustomer();
	}

	@Override
	protected LanguageModel getEmailLanguage(final StorefrontEmployeeNotificationProcessModel businessProcessModel)
	{

		return businessProcessModel.getLanguage();
	}

	@Override
	public void init(final StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel,
			final EmailPageModel emailPageModel)
	{
		super.init(storefrontEmployeeNotificationProcessModel, emailPageModel);
		setEmployeeData(getEmployeeConverter().convert(getEmployee(storefrontEmployeeNotificationProcessModel)));
		// put mail
		put(FROM_EMAIL, emailPageModel.getFromEmail());
		put(FROM_DISPLAY_NAME, emailPageModel.getFromName());
		put(DISPLAY_NAME, "Kao Customer Service");
		put(EMAIL,
				salonEmployeeEmailResolutionService.getEmailForEmployee(getEmployee(storefrontEmployeeNotificationProcessModel)));

	}

	private EmployeeModel getEmployee(final StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel)
	{
		return storefrontEmployeeNotificationProcessModel.getEmployee();
	}

}