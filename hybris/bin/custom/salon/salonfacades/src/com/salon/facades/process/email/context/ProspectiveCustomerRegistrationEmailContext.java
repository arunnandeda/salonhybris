package com.salon.facades.process.email.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.process.model.ProspectiveCustomerRegistrationEmailProcessModel;


import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.Config;

public class ProspectiveCustomerRegistrationEmailContext extends AbstractEmailContext<StoreFrontCustomerProcessModel>{
	
	private static final Logger LOG = LoggerFactory.getLogger(ProspectiveCustomerRegistrationEmailContext.class);

	private ProspectiveCustomerRegModel newCustomer;
	
	public ProspectiveCustomerRegModel getNewCustomer() {
		return newCustomer;
	}
	public void setNewCustomer(ProspectiveCustomerRegModel newCustomer) {
		this.newCustomer = newCustomer;
	}
	
	
	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		
		LOG.info("Email for CSR being generated");
		if(storeFrontCustomerProcessModel instanceof ProspectiveCustomerRegistrationEmailProcessModel){
			LOG.info("generating email from prospective customer registration email context");
			final ProspectiveCustomerRegistrationEmailProcessModel prospectiveCustomerRegistrationEmailProcessModel = (ProspectiveCustomerRegistrationEmailProcessModel) storeFrontCustomerProcessModel;
			setNewCustomer(prospectiveCustomerRegistrationEmailProcessModel.getNewCustomer());
			
		}
		
		//put mail
        put(EMAIL, Config.getParameter("mail.to.prospectivecustomer.registration"));
		put(DISPLAY_NAME,Config.getParameter("mail.header"));
	}
	
	@Override
	protected BaseSiteModel getSite(StoreFrontCustomerProcessModel businessProcessModel) {
		// TODO Auto-generated method stub
		return businessProcessModel.getSite();
	}
	@Override
	protected CustomerModel getCustomer(StoreFrontCustomerProcessModel businessProcessModel) {
		// TODO Auto-generated method stub
		return businessProcessModel.getCustomer();
	}
	@Override
	protected LanguageModel getEmailLanguage(StoreFrontCustomerProcessModel businessProcessModel) {
		// TODO Auto-generated method stub
		return businessProcessModel.getLanguage();
	}

}
