package com.salon.facades.process.email.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.salon.core.process.model.CustomerRegNotificationForSalesRepEmailProcessModel;
import com.salon.core.process.model.StorefrontEmployeeNotificationProcessModel;
import com.salon.core.resolution.SalonEmployeeEmailResolutionService;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;



public class SalonCustomerRegNotificationToSalesRepEmailContext extends EmployeeNotificationEmailContext {

	private static final Logger LOG = LoggerFactory.getLogger(SalonCustomerRegNotificationToSalesRepEmailContext.class);

	
	private CustomerModel b2bCustomer;
	
	public CustomerModel getB2bCustomer() {
		return b2bCustomer;
	}

	public void setB2bCustomer(CustomerModel customerModel) {
		this.b2bCustomer = customerModel;
	}

	@Override
	protected BaseSiteModel getSite(StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel) {
		return storefrontEmployeeNotificationProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel) {
		return  storefrontEmployeeNotificationProcessModel.getCustomer();
	}

	@Override
	protected LanguageModel getEmailLanguage(StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel) {
		return storefrontEmployeeNotificationProcessModel.getLanguage();
	}
	
	@Override
	public void init(final StorefrontEmployeeNotificationProcessModel storefrontEmployeeNotificationProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storefrontEmployeeNotificationProcessModel, emailPageModel);
		
		LOG.info("Email for Salon Sales Rep being generated");
		if(storefrontEmployeeNotificationProcessModel instanceof CustomerRegNotificationForSalesRepEmailProcessModel){
			final CustomerRegNotificationForSalesRepEmailProcessModel customerRegNotificationForSalesRepEmailProcessModel = (CustomerRegNotificationForSalesRepEmailProcessModel) storefrontEmployeeNotificationProcessModel;
			setB2bCustomer(customerRegNotificationForSalesRepEmailProcessModel.getB2bcustomer());
		}
		
		
		
	}
	
	
}
