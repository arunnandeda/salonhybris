/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.facades.secureportaladdon.facades;

import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSalesRepFoundException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.facades.addon.data.B2BUnitForm;
import com.salon.facades.data.ProspectiveCustomerData;
import com.salon.facades.secureportaladdon.data.B2BRegistrationData;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;


/**
 * Facade responsible for everything related to B2B registration
 */
public interface B2BRegistrationFacade
{

	/**
	 * Initiates the registration process for B2B. This method will first validate the submitted data, check if a user or
	 * a company to the given name already exists, persist the registration request (as a model) and initiate the
	 * workflow so that the registration request either gets approved OR rejected.
	 *
	 * @param data
	 *           The registration data
	 */
	public void register(B2BRegistrationData data) throws CustomerAlreadyExistsException;

	public B2BUnitForm getRegisteredEmails(final String customerNumber)
			throws NoSuchB2BUnitFoundException, MissingParameterFieldException;

	public void registerCustomer(final String customerNumber, final String emailId)
			throws CustomerAlreadyExistsException, NoSuchB2BUnitFoundException, MissingParameterFieldException;

	boolean validateToken(String token) throws TokenInvalidatedException;
	
	public void saveB2BCustomer(String token, String pwd, String titleCode, String name) throws TokenInvalidatedException, CustomerAlreadyExistsException, NoSalesRepFoundException;

	public void saveProspectiveCustomer(ProspectiveCustomerData prospectiveCustomer) throws CustomerAlreadyExistsException, DuplicateUidException;

}
