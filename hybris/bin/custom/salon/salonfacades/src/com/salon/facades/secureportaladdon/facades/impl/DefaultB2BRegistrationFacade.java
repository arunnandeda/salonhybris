/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.facades.secureportaladdon.facades.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.customerticketingfacades.data.TicketData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


import com.salon.core.model.B2BRegistrationModel;
import com.salon.core.model.ProspectiveCustomerRegModel;
import com.salon.core.secureportaladdon.constants.SecureportaladdonConstants;
import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSalesRepFoundException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.core.secureportaladdon.services.B2BRegistrationService;
import com.salon.facades.addon.data.B2BUnitForm;
import com.salon.facades.data.ProspectiveCustomerData;
import com.salon.facades.secureportaladdon.data.B2BRegistrationData;
import com.salon.facades.secureportaladdon.facades.B2BRegistrationFacade;
import com.salon.facades.secureportaladdon.facades.B2BRegistrationWorkflowFacade;

/**
 * Default implementation of {@link B2BRegistrationFacade}
 */
public class DefaultB2BRegistrationFacade implements B2BRegistrationFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultB2BRegistrationFacade.class);

	private CMSSiteService cmsSiteService;

	private CommonI18NService commonI18NService;

	private ModelService modelService;

	private BaseStoreService baseStoreService;

	private UserService userService;

	private B2BRegistrationWorkflowFacade b2bRegistrationWorkflowFacade;

	private WorkflowTemplateService workflowTemplateService;

	private Populator<B2BUnitModel, B2BUnitForm> b2bUnitCustomPopulator;

	private B2BRegistrationService b2bRegistrationService;
	
	@Autowired
	private Populator<AddressData, AddressModel> addressReversePopulator;

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @param cmsSiteService
	 *           the cmsSiteService to set
	 */
	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @param b2bRegistrationWorkflowFacade
	 *           the b2bRegistrationWorkflowFacade to set
	 */
	@Required
	public void setB2bRegistrationWorkflowFacade(final B2BRegistrationWorkflowFacade b2bRegistrationWorkflowFacade)
	{
		this.b2bRegistrationWorkflowFacade = b2bRegistrationWorkflowFacade;
	}

	/**
	 * @param workflowTemplateService
	 *           the workflowTemplateService to set
	 */
	@Required
	public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService)
	{
		this.workflowTemplateService = workflowTemplateService;
	}

	@Required
	public void setB2BRegistrationService(final B2BRegistrationService b2bRegistrationService)
	{
		this.b2bRegistrationService = b2bRegistrationService;
	}

	public Populator<B2BUnitModel, B2BUnitForm> getB2bUnitCustomPopulator()
	{
		return b2bUnitCustomPopulator;
	}


	@Required
	public void setB2bUnitCustomPopulator(final Populator<B2BUnitModel, B2BUnitForm> b2bUnitCustomPopulator)
	{
		this.b2bUnitCustomPopulator = b2bUnitCustomPopulator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.secureportaladdon.facades.B2BRegistrationFacade#register(de.hybris.platform.secureportaladdon
	 * .data .B2BRegistrationData)
	 */
	@Override
	public void register(final B2BRegistrationData data) throws CustomerAlreadyExistsException
	{

		final Transaction tx = Transaction.current();
		tx.begin();

		boolean success = false;

		try
		{

			// Check if a user using the same email exist, if so we need to abort the current operation!
			final boolean userExists = userService.isUserExisting(data.getEmail());
			/*if (userExists)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(String.format("user with uid '%s' already exists!", data.getEmail()));
				}
				throw new CustomerAlreadyExistsException(String.format("User with uid '%s' already exists!", data.getEmail()));
			}*/

			// Save the registration model so that it is accessible to the workflow actions. The registration model will be deleted as part of the cleanup
			// of the workflow.
			final B2BRegistrationModel registration = toRegistrationModel(data);
			modelService.save(registration);

			// Save the customer. At this point, the customer is saved to generate emails and initiate the workflow. This customer will be deleted as part of the
			// cleanup of the workflow IF he is rejected. If approved, the customer will be deleted by the "approve workflow action" and will be re-created
			// as a B2BCustomer and assigned to the proper B2BUnit. At this stage, we can't create a B2BCustomer since we don't even have a B2BUnit (organization!).
			final CustomerModel customer = toCustomerModel(data);
			modelService.save(customer);

			final WorkflowTemplateModel workflowTemplate = workflowTemplateService
					.getWorkflowTemplateForCode(SecureportaladdonConstants.Workflows.REGISTRATION_WORKFLOW);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Created WorkflowTemplateModell using name '%s'",
						SecureportaladdonConstants.Workflows.REGISTRATION_WORKFLOW));
			}

			// Start the workflow
			b2bRegistrationWorkflowFacade.launchWorkflow(workflowTemplate, registration);

			tx.commit();
			success = true;

		}
		finally
		{
			if (!success)
			{
				tx.rollback();
			}
		}

	}

	/**
	 * Converts a {@link B2BRegistrationData} into a {@link CustomerModel}. Only keeps the most important fields to
	 * generate emails, the rest is ignored as this customer is to be deleted as part of the workflow execution
	 *
	 * @param data
	 *           The registration data
	 * @return An unsaved instance of {@link CustomerModel}
	 */
	protected CustomerModel toCustomerModel(final B2BRegistrationData data)
	{

		final CustomerModel model = modelService.create(CustomerModel.class);

		model.setName(WordUtils.capitalizeFully(data.getName()));
		model.setUid(data.getEmail());
		model.setLoginDisabled(true);

		final TitleModel title = userService.getTitleForCode(data.getTitleCode());
		model.setTitle(title);

		return model;

	}

	/**
	 * Converts a {@link B2BRegistrationData} into a {@B2BRegistrationModel}
	 *
	 * @param data
	 *           The registration data
	 * @return An unsaved instance of type {@B2BRegistrationModel}
	 */
	protected B2BRegistrationModel toRegistrationModel(final B2BRegistrationData data)
	{

		final B2BRegistrationModel model = modelService.create(B2BRegistrationModel.class);

		// Use reflection to copy most properties and ignore these since we want to manage them manually
		BeanUtils.copyProperties(data, model, new String[]
		{ "titleCode", "companyAddressCountryIso", "companyAddressRegion", "baseStore", "cmsSite", "currency", "language" });

		// Title is mandatory
		final TitleModel title = userService.getTitleForCode(data.getTitleCode());
		model.setTitle(title);

		// Country is mandatory
		final CountryModel country = commonI18NService.getCountry(data.getCompanyAddressCountryIso());
		model.setCompanyAddressCountry(country);

		// Region is optional
		if (StringUtils.isNotBlank(data.getCompanyAddressRegion()))
		{
			final RegionModel region = commonI18NService.getRegion(country, data.getCompanyAddressRegion());
			model.setCompanyAddressRegion(region);
		}

		// Get these from current context
		model.setBaseStore(baseStoreService.getCurrentBaseStore());
		model.setCmsSite(cmsSiteService.getCurrentSite());
		model.setCurrency(commonI18NService.getCurrentCurrency());
		model.setLanguage(commonI18NService.getCurrentLanguage());

		return model;

	}


	/*
	 * * (non-Javadoc)
	 *
	 * @see de.hybris.platform.secureportaladdon.facades.B2BRegistrationFacade#getRegisteredEmails(java.lang.String)
	 **/
	//custom-code starts
	@Override
	public B2BUnitForm getRegisteredEmails(final String customerNumber)
			throws NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		if (customerNumber != null && customerNumber != "")
		{
			final B2BUnitModel b2bUnitModel = b2bRegistrationService.getB2BUnitByCustomerNumber(customerNumber);
			final B2BUnitForm b2bUnitForm = new B2BUnitForm();
			getB2bUnitCustomPopulator().populate(b2bUnitModel, b2bUnitForm);

			return b2bUnitForm;
		}
		else
		{
			throw new MissingParameterFieldException("The parameter field should not be null or empty");
		}
	}
  //custom-code ends
	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.secureportaladdon.facades.B2BRegistrationFacade#registerCustomer(java.lang.String,
	 * java.lang.String)
	 */
	//custom-code start
	@Override
	public void registerCustomer(final String customerNumber, final String emailId)
			throws CustomerAlreadyExistsException, NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		if (customerNumber != null && customerNumber != "" && emailId != null && emailId != "")
		{
			final Transaction tx = getCurrentTransaction();
			tx.begin();

			boolean success = false;
			// Check if a user using the same email exist, if so we need to abort the current operation!
			final boolean userExists = userService.isUserExisting(emailId);
			if (userExists) {
				LOG.debug(String.format("user with uid '%s' already exists!", emailId));
				throw new CustomerAlreadyExistsException(String.format("User with uid '%s' already exists!", emailId));
			}
			
			try
			{

				final B2BCustomerModel b2bCustomerModel = modelService.create(B2BCustomerModel.class);
				b2bCustomerModel.setUid(emailId);
				b2bCustomerModel.setEmail(emailId);
				final B2BUnitModel b2bUnitModel = b2bRegistrationService.getB2BUnitByCustomerNumber(customerNumber);
				final List<AddressModel> addresses = (List<AddressModel>) b2bUnitModel.getAddresses();
				b2bCustomerModel.setName(b2bUnitModel.getDisplayName());
				b2bCustomerModel.setLoginDisabled(true);
				b2bCustomerModel.setAddresses(addresses);
				b2bCustomerModel.setDefaultB2BUnit(b2bUnitModel);
				modelService.save(b2bCustomerModel);
				b2bRegistrationService.registerCustomer(b2bCustomerModel);
				tx.commit();
				success = true;
			}
			finally
			{
				if (!success)
				{
					tx.rollback();
				}
			}

		}

		else
		{
			throw new MissingParameterFieldException("The parameter fields are missing or null.");
		}
	}

	public Transaction getCurrentTransaction()
	{
		return Transaction.current();
	}
	@Override
	public boolean validateToken(String token) throws TokenInvalidatedException {
		
		{
			return b2bRegistrationService.checkTokenExpiry(token);
		}

		
	}
	
	// save the newly entered password of the customer
	@Override
	public void saveB2BCustomer(final String token, final String pwd,String titleCode,String name) throws TokenInvalidatedException, CustomerAlreadyExistsException, NoSalesRepFoundException{
		
		b2bRegistrationService.saveB2BCustomer(token, pwd,titleCode,name);
		
	}

	
	// save the prospective customer details
	@Override
	public void saveProspectiveCustomer(ProspectiveCustomerData prospectiveCustomer) throws CustomerAlreadyExistsException,DuplicateUidException {
		validateParameterNotNullStandardMessage("prospectiveCustomerData", prospectiveCustomer);
		final ProspectiveCustomerRegModel prospectiveCustomerRegModel = modelService.create(ProspectiveCustomerRegModel.class);
		// set the customer value
		LOG.debug("Setting the attribute values for prospective customer- " + prospectiveCustomer.getName());
		prospectiveCustomerRegModel.setFullName(prospectiveCustomer.getName());
		prospectiveCustomerRegModel.setEmail(prospectiveCustomer.getEmail());
		prospectiveCustomerRegModel.setSalonName(prospectiveCustomer.getSalonName());
		prospectiveCustomerRegModel.setPhone(prospectiveCustomer.getPhone());
		prospectiveCustomerRegModel.setNoOfEmp(prospectiveCustomer.getNoOfEmp());
		prospectiveCustomerRegModel.setSalonSize(prospectiveCustomer.getSalonSize());
		prospectiveCustomerRegModel.setPosition(prospectiveCustomer.getPosition());
		prospectiveCustomerRegModel.setVatNo(prospectiveCustomer.getVatNo());
		final AddressModel address = modelService.create(AddressModel.class);
		addressReversePopulator.populate(prospectiveCustomer.getAddress(), address);
		address.setOwner(userService.getAnonymousUser());
		prospectiveCustomerRegModel.setAddress(address);
		// calling the service method to save the prospect customer
			b2bRegistrationService.saveProspectiveCustomer(prospectiveCustomerRegModel);
			LOG.debug("Successfuly saved the prospective customer- " + prospectiveCustomer.getEmail());
		
	}
	
	
	
	

	
	//custom-code ends
}