/**
 *
 */
package com.salon.facades.secureportaladdon.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import com.salon.facades.addon.data.B2BUnitForm;


/**
 * @author 1DigitalS
 *
 */
public class B2BUnitCustomPopulator implements Populator<B2BUnitModel, B2BUnitForm>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final B2BUnitModel source, final B2BUnitForm target) throws ConversionException
	{
		// YTODO Auto-generated method stub
		target.setCustomerNumber(source.getUid());
		final List<String> emails = (List<String>) source.getEmailIds();
		target.setEmailIds(emails);

	}

}
