package com.salon.facades.wishlist;

import java.util.List;

import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;
import com.salon.facades.wishEntries.data.Wishlist2Data;



public interface SalonWishlistFacade {
	
	public List<Wishlist2Data> getCurrentCustomerAllWishList();
	public List<Wishlist2Data> getWishlists();
	public void addToWishlist(String wishListName ,String description,String code,Boolean isNewWishlist);
	public void removeProductFromwishList(String wishListId,String productCode) throws NoSuchWishListFoundException ;
	public void removeAllProductFromwishList(String wishListName)throws NoSuchWishListFoundException;
	
	public void removeWishList(String wishListName);
	public  Wishlist2Data getMatchedWishlist(String wishlistName);
	public Wishlist2Data createWishlist(String name, String description);
	public Wishlist2Data getWishlistById(String wishListId) throws NoSuchWishListFoundException ;
	public Wishlist2Data editwishlistName(String NewWishListName,String wishListId)throws WishListAlreadyExistException;
	

}
