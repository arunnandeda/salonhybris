package com.salon.facades.wishlist.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.salon.core.service.wishlist.SalonWishlistService;
import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;
import com.salon.facades.wishEntries.data.Wishlist2Data;
import com.salon.facades.wishlist.SalonWishlistFacade;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BCustomerService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

public class SalonWishlistFacadeImpl implements SalonWishlistFacade {

	private static final Logger LOG = LoggerFactory.getLogger(SalonWishlistFacadeImpl.class);

	private static final String DEFAULT_DESCRIPTION = "Default wishlist";
	private static final Wishlist2EntryPriority DEFAULT_PRIORITY = Wishlist2EntryPriority.MEDIUM;
	private static final int DEFAULT_DESIRED = 1;
	private static final String DEFAULT_COMMENTS = "";

	@Resource
	private SalonWishlistService salonWishlistService;

	@Autowired
	private DefaultB2BCustomerService defaultB2BCustomerService;

	@Resource
	private ProductService productService;

	@Autowired
	private Converter<Wishlist2Model, Wishlist2Data> wishlist2Converter;

	/**
	 * Give all wish list of customer current user
	 */
	@Override
	public List<Wishlist2Data> getCurrentCustomerAllWishList() {
		LOG.debug("Getting all wishList list for current User");
		List<Wishlist2Data> wishlist2Data = new ArrayList<>();
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (currentCustomer != null) {

			List<Wishlist2Model> customerWishList = salonWishlistService.getWishlists(currentCustomer);

			if (CollectionUtils.isNotEmpty(customerWishList)) {

				wishlist2Data.addAll(Converters.convertAll(customerWishList, getWishlist2Converter()));
			}

		}
		return wishlist2Data;
	}

	/**
	 * Give all wish list of customer if wish list is not there then it will
	 * create default wish List with current user name
	 */
	@Override
	public List<Wishlist2Data> getWishlists() {
		LOG.debug("get Wish list for current User");
		List<Wishlist2Data> wishlist2Data = new ArrayList<>();

		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (currentCustomer != null) {
			LOG.debug("current user is.." + currentCustomer.getName());
			// wish list is not there then it will create default wishList
			if (!salonWishlistService.hasDefaultWishlist(currentCustomer)) {

				salonWishlistService.createDefaultWishlist(currentCustomer, currentCustomer.getName() + " wishList",
						DEFAULT_DESCRIPTION);

			}
			List<Wishlist2Model> wishlists = salonWishlistService.getWishlists(currentCustomer);
			wishlist2Data.addAll(Converters.convertAll(wishlists, getWishlist2Converter()));
		}
		return wishlist2Data;
	}

	/**
	 * Add product to the given wishList Here If wish list is not there then it
	 * will create the wishList and will add the product. if boolean is TRUE
	 * means newWishList otherwise existing wishList.
	 * 
	 * @param wishName
	 *            wishName
	 * @param description
	 *            description
	 * @param productCode
	 *            productCode
	 * @param isNewWishlist
	 *            isNewWishlist(true/false)
	 * @return void
	 */
	@Override
	public void addToWishlist(String wishName, String description, String productCode, Boolean isNewWishlist) {

		LOG.debug("Add to wish List " + wishName);
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(wishName) && StringUtils.isNotEmpty(productCode)) {
			Wishlist2Model wishlist2Model = null;
			// if wish List is new then it will create wishList first and add
			// the product
			if (BooleanUtils.isTrue(isNewWishlist)) {
				wishlist2Model = salonWishlistService.createWishlist(currentCustomer, wishName, description);
			} else {
				wishlist2Model = salonWishlistService.getWishListByName(wishName, currentCustomer);
			}

			salonWishlistService.addWishlistEntry(wishlist2Model, productService.getProductForCode(productCode),
					DEFAULT_DESIRED, DEFAULT_PRIORITY, DEFAULT_COMMENTS);
		}
	}

	/**
	 * Remove product from particular wish list
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * @param productCode
	 *            productCode
	 * @return void
	 */
	@Override
	public void removeProductFromwishList(String wishListId, String productCode) throws NoSuchWishListFoundException {

		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(wishListId)) {
			{
				Wishlist2Model wishlist = salonWishlistService.getWishListById(wishListId, currentCustomer);

				LOG.debug(productCode + "..." + wishListId);
				if (null != wishlist) {
					salonWishlistService.removeWishlistEntryForProduct(productService.getProductForCode(productCode),
							wishlist);
				}
			}
		}

	}

	/**
	 * Removing wish list
	 * 
	 * @param wishListName
	 *            wishListName
	 * @return void
	 */
	@Override
	public void removeWishList(String wishListName) {
		LOG.debug("removeing wish list." + wishListName);

		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(wishListName)) {
			salonWishlistService.removeWishlist(wishListName, currentCustomer);
		}

	}

	/**
	 * removing all product wishList entry from Particular wishList
	 * 
	 * @param wishListId
	 *            wishListId
	 * @return void
	 */

	@Override
	public void removeAllProductFromwishList(String wishListId) throws NoSuchWishListFoundException {

		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(wishListId)) {
			Wishlist2Model wishlist = salonWishlistService.getWishListById(wishListId, currentCustomer);
			if (null != wishlist) {
				salonWishlistService.removeAllProductFromWishList(wishlist);
			}
		}

	}

	/**
	 * creating new List
	 * 
	 * @param user
	 *            name
	 * @param description
	 *            description
	 * @return void Wishlist2Data(null/Wishlist2Data) If wishList already exist
	 *         then it will return null
	 */

	@Override
	public Wishlist2Data createWishlist(String name, String description) {
		LOG.debug("Creating  wish list.." + name);
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(description)) {

			// If the wish List already exists with given name
			Wishlist2Model existingWishList = salonWishlistService.getWishListByName(name, currentCustomer);
			if (null == existingWishList) {
				Wishlist2Model wishlist2Model = salonWishlistService.createWishlist(currentCustomer, name, description);
				return wishlist2DataConverter(wishlist2Model);
			}
		}
		return null;
	}

	/**
	 * Getting wishList by wishListId(pk) and current customer
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * @return void Wishlist2Data(null/Wishlist2Data)
	 */
	@Override
	public Wishlist2Data getWishlistById(String wishListId) throws NoSuchWishListFoundException {

		LOG.debug("Fetch wish List by id(pk)");
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (currentCustomer != null && StringUtils.isNotEmpty(wishListId)) {

			Wishlist2Model wishlist2Model = salonWishlistService.getWishListById(wishListId, currentCustomer);
			return wishlist2DataConverter(wishlist2Model);

		}
		return null;
	}

	/**
	 * Renaming wish List name
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * 
	 * @param newWishListName
	 *            newWishListName
	 * @return void Wishlist2Data(null/Wishlist2Data)
	 */
	@Override
	public Wishlist2Data editwishlistName(String newWishListName, String wishListId)
			throws WishListAlreadyExistException {
		LOG.debug("edit  wish List Name");
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (currentCustomer != null && StringUtils.isNotEmpty(newWishListName) && StringUtils.isNotEmpty(wishListId)) {

			Wishlist2Model wishlist2Model = salonWishlistService.editWishlistName(newWishListName, wishListId,
					currentCustomer);
			return wishlist2DataConverter(wishlist2Model);
		}
		return null;
	}

	/**
	 * Getting match wish name
	 * 
	 * @param wishlistName
	 *            wishlistName
	 */
	@Override
	public Wishlist2Data getMatchedWishlist(String wishlistName) {
		LOG.debug("Fetch match wish List by Name");
		final B2BCustomerModel currentCustomer = getCurrentB2BCustomer();
		if (null != currentCustomer && StringUtils.isNotEmpty(wishlistName)) {
			Wishlist2Model wishlist2Model = salonWishlistService.getWishListByName(wishlistName, currentCustomer);

			return wishlist2DataConverter(wishlist2Model);
		}
		return null;
	}

	private B2BCustomerModel getCurrentB2BCustomer() {
		final B2BCustomerModel currentCustomer = defaultB2BCustomerService.getCurrentB2BCustomer();
		return null != currentCustomer ? currentCustomer : null;
	}

	private Wishlist2Data wishlist2DataConverter(Wishlist2Model wishlist2Model) {
		if (null != wishlist2Model) {
			return wishlist2Converter.convert(wishlist2Model);
		}
		return null;
	}

	public Converter<Wishlist2Model, Wishlist2Data> getWishlist2Converter() {
		return wishlist2Converter;
	}

	public void setWishlist2Converter(Converter<Wishlist2Model, Wishlist2Data> wishlist2Converter) {
		this.wishlist2Converter = wishlist2Converter;
	}

	public SalonWishlistService getSalonWishlistService() {
		return salonWishlistService;
	}

	public void setSalonWishlistService(SalonWishlistService salonWishlistService) {
		this.salonWishlistService = salonWishlistService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

}
