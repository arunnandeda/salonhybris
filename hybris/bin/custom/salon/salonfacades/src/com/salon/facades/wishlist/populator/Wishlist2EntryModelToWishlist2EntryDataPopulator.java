package com.salon.facades.wishlist.populator;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.salon.facades.wishEntries.data.Wishlist2EntryData;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

//populatore to convert Wishlist2EntryModel to Wishlist2EntryData
public class Wishlist2EntryModelToWishlist2EntryDataPopulator implements Populator<Wishlist2EntryModel, Wishlist2EntryData> {
	private Converter<ProductModel, ProductData> productConverter;

	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;
	
	private static final Logger LOG = LoggerFactory.getLogger(Wishlist2EntryModelToWishlist2EntryDataPopulator.class);

	@Override
	public void populate(final Wishlist2EntryModel source, final Wishlist2EntryData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		Collection<ProductOption> options=Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY,
				ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.CATEGORIES,
				ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
				ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY);
		
		LOG.debug("wishList entry model to data populator");
		ProductData productData = getProductConverter().convert(source.getProduct());
		getProductConfiguredPopulator().populate(source.getProduct(), productData,options);
				
		
		target.setProduct(productData);
		target.setAddedDate(source.getAddedDate());
	}

	public ConfigurablePopulator<ProductModel, ProductData, ProductOption> getProductConfiguredPopulator() {
		return productConfiguredPopulator;
	}

	public void setProductConfiguredPopulator(
			ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator) {
		this.productConfiguredPopulator = productConfiguredPopulator;
	}

	protected Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	@Required
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

}


	


