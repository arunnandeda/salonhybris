package com.salon.facades.wishlist.populator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.salon.facades.wishEntries.data.Wishlist2Data;
import com.salon.facades.wishEntries.data.Wishlist2EntryData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
//populator to convert wishList model to wishListData
public class Wishlist2ModelToWishlist2DataPopulator implements Populator<Wishlist2Model, Wishlist2Data>{

	private Converter<Wishlist2EntryModel, Wishlist2EntryData> wishlistEntryConverter;

	private static final Logger LOG = LoggerFactory.getLogger(Wishlist2ModelToWishlist2DataPopulator.class);
	@Override
	public void populate(final Wishlist2Model source, final Wishlist2Data target) throws ConversionException
	{
		LOG.debug("wishList  model to data");
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setId(source.getPk().toString());
		target.setName(source.getName());
		target.setEntries(getWishlistEntryConverter().convertAll(source.getEntries()));
		target.setCreationtime(source.getCreationtime());
	}

	protected Converter<Wishlist2EntryModel, Wishlist2EntryData> getWishlistEntryConverter()
	{
		return wishlistEntryConverter;
	}

	@Required
	public void setWishlistEntryConverter(final Converter<Wishlist2EntryModel, Wishlist2EntryData> wishlistEntryConverter)
	{
		this.wishlistEntryConverter = wishlistEntryConverter;
	}

}
