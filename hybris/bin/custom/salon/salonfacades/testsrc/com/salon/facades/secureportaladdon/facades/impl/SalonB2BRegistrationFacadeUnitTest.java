/**
 *
 */
package com.salon.facades.secureportaladdon.facades.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.test.TransactionTest;
import de.hybris.platform.tx.Transaction;

import org.junit.Before;
import org.junit.Test;

import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.core.secureportaladdon.services.impl.DefaultB2BRegistrationService;


/**
 * @author Arajatsp
 *
 */

//@UnitTest
public class SalonB2BRegistrationFacadeUnitTest extends TransactionTest
{
	private DefaultB2BRegistrationFacade defaultB2BRegistrationFacade;

	private DefaultB2BRegistrationService defaultB2BRegistrationService;

	private ModelService modelService;

	private UserService userService;
	/*
	 * private B2BUnitConverter b2bUnitConverter;
	 *
	 * private B2BUnitData b2bUnitData;
	 */
	private B2BUnitModel b2bUnitModel;

	private B2BCustomerModel b2bCustomerModel;

	final String customerNumber = "12341";

	final String emailId = "raj@tc.com";

	@Before
	public void setUp()
	{
		defaultB2BRegistrationFacade = new DefaultB2BRegistrationFacade();

		defaultB2BRegistrationFacade
				.setB2BRegistrationService(defaultB2BRegistrationService = mock(DefaultB2BRegistrationService.class));

		defaultB2BRegistrationFacade.setModelService(modelService = mock(ModelService.class));

		defaultB2BRegistrationFacade.setUserService(userService = mock(UserService.class));
	}

	private B2BCustomerModel createB2BCustomerModel()
	{

		final B2BCustomerModel b2bCustomer = new B2BCustomerModel();

		b2bCustomer.setName("Test Name");
		b2bCustomer.setUid("test@gmail.com");
		final B2BUnitModel b2bUnitModel = createB2BUnitModel();
		b2bCustomer.setDefaultB2BUnit(b2bUnitModel);
		b2bCustomer.setName(b2bUnitModel.getName());

		return b2bCustomer;

	}

	@Test(expected = CustomerAlreadyExistsException.class)
	public void salonRegisterTestExpectedException()
			throws CustomerAlreadyExistsException, NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		final B2BCustomerModel b2bCustomer = createB2BCustomerModel();
		final B2BUnitModel b2bUnitModel = createB2BUnitModel();
		when(userService.isUserExisting(emailId)).thenReturn(true);

		when(modelService.create(B2BCustomerModel.class)).thenReturn(b2bCustomer);

		when(defaultB2BRegistrationService.getB2BUnitByCustomerNumber(customerNumber)).thenReturn(b2bUnitModel);

		defaultB2BRegistrationFacade.registerCustomer(customerNumber, emailId);
	}

	@Test
	public void testRegisterCustomer()
			throws CustomerAlreadyExistsException, NoSuchB2BUnitFoundException, MissingParameterFieldException
	{
		final B2BCustomerModel b2bCustomer = createB2BCustomerModel();
		final B2BUnitModel b2bUnitModel = createB2BUnitModel();
		when(userService.isUserExisting(emailId)).thenReturn(false);

		when(modelService.create(B2BCustomerModel.class)).thenReturn(b2bCustomer);

		when(defaultB2BRegistrationService.getB2BUnitByCustomerNumber(customerNumber)).thenReturn(b2bUnitModel);

		defaultB2BRegistrationFacade.registerCustomer(customerNumber, emailId);

		verify(modelService).save(b2bCustomer);
		verify(defaultB2BRegistrationService).registerCustomer(b2bCustomer);
	}

	private B2BUnitModel createB2BUnitModel()
	{
		final B2BUnitModel b2bUnitModel = new B2BUnitModel();
		b2bUnitModel.setUid("12321");
		b2bUnitModel.setName("Salon Chennai");
		return b2bUnitModel;
	}

}
