/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.initialdata.constants;

/**
 * Global class for all SalonInitialData constants.
 */
public final class SalonInitialDataConstants extends GeneratedSalonInitialDataConstants
{
	public static final String EXTENSIONNAME = "saloninitialdata";

	private SalonInitialDataConstants()
	{
		//empty
	}
}
