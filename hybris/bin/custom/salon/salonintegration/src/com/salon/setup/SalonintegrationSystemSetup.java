/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.setup;

import static com.salon.constants.SalonintegrationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.salon.constants.SalonintegrationConstants;
import com.salon.service.SalonintegrationService;


@SystemSetup(extension = SalonintegrationConstants.EXTENSIONNAME)
public class SalonintegrationSystemSetup
{
	private final SalonintegrationService salonintegrationService;

	public SalonintegrationSystemSetup(final SalonintegrationService salonintegrationService)
	{
		this.salonintegrationService = salonintegrationService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		salonintegrationService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SalonintegrationSystemSetup.class.getResourceAsStream("/salonintegration/sap-hybris-platform.png");
	}
}
