/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.setup;

import static com.salon.constants.SalonpaymentConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.salon.constants.SalonpaymentConstants;
import com.salon.service.SalonpaymentService;


@SystemSetup(extension = SalonpaymentConstants.EXTENSIONNAME)
public class SalonpaymentSystemSetup
{
	private final SalonpaymentService salonpaymentService;

	public SalonpaymentSystemSetup(final SalonpaymentService salonpaymentService)
	{
		this.salonpaymentService = salonpaymentService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		salonpaymentService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SalonpaymentSystemSetup.class.getResourceAsStream("/salonpayment/sap-hybris-platform.png");
	}
}
