module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        less: {
            files: ['web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less','web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
                    'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-*/less/*', 'web/webroot/WEB-INF/_ui-src/**/themes/**/less/*.less'],
            tasks: ['less'],
        },
        fonts: {
            files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/fonts/*'],
            tasks: ['sync:syncfonts'],
        },
        ybasejs: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/**/*.js'],
            tasks: ['sync:syncybase'],
        },
        jquery: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
            tasks: ['sync:syncjquery'],
        },
    },
    less: {
        default: {
            files: [
                {
                    expand: true,
                    cwd: 'web/webroot/WEB-INF/_ui-src/',
                    src: '**/themes/**/less/style.less',
                    dest: 'web/webroot/_ui/',
                    ext: '.css',
                    rename:function(dest,src){
                       var nsrc = src.replace(new RegExp("/themes/(.*)/less"),"/theme-$1/css");
                       return dest+nsrc;
                    }
                }
            ]
        },
    },

    sync : {
    	syncfonts: {
    		files: [{
                expand: true,
    			cwd: 'web/webroot/WEB-INF/_ui-src/',
    			src: '**/themes/**/fonts/*',
    			dest: 'web/webroot/_ui/',
    			rename:function(dest,src){
                	var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
                	return dest+nsrc;
             }
    		}]
    	},
    	syncybase: {
    		files: [{
    			cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
    			src: '**/*.js',
    			dest: 'web/webroot/_ui/responsive/common/js',
    		}]
    	},
    	syncjquery: {
    		files: [{
    			cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
    			src: 'jquery*.js',
    			dest: 'web/webroot/_ui/responsive/common/js',
    		}]
    	}
    },
    sass: {                              // Task
        dist: {                            // Target
          options: {                       // Target options
            style: 'expanded'
          },
          files: {                         // Dictionary of files
            'web/webroot/WEB-INF/_ui-src/responsive/lib/sass/salon-sass-compiled.less' : 'web/webroot/WEB-INF/_ui-src/responsive/lib/sass/app.scss'       // 'destination': 'source'
          }
        }
      },
      autoprefixer:{
    	  options: {
    		    // // Task-specific options go here.
    		         browsers: ["last 2 versions","ie >= 9","ios >= 7"]
    		      },
    	  dist:{
    	    files:{
    	      'web/webroot/WEB-INF/_ui-src/responsive/lib/sass/salon-sass-compiled.less':'web/webroot/WEB-INF/_ui-src/responsive/lib/sass/salon-sass-compiled.less'
    	    }
    	  }
    	},
      
    postcss: {
        options: {
            map: true,
            processors: [
                require('autoprefixer')
            ]
        },
        dist: {
            src: 'web/webroot/WEB-INF/_ui-src/responsive/lib/sass/*.less'
        }
    }
    
});
 
  // Plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-postcss');


  // Default task(s).
//  grunt.registerTask('default', ['less', 'sync','postcss:dist'])
  //Currently we will use this available nodejs & grunt help to compile the SASS files and also autoprefix the css. 
  // Currently the OOTB styles as well as the styles provided by the design agency is included in styles.css.
  //Eventually we will have to be able to remove the OOTB styles and use only the ones provided by the design agency.
  grunt.registerTask('default', ['sass','autoprefixer'])



};