package com.salon.storefront.controllers.cms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.salon.core.model.WishListDropDownComponentModel;
import com.salon.facades.wishEntries.data.Wishlist2Data;
import com.salon.facades.wishlist.SalonWishlistFacade;
import com.salon.storefront.controllers.ControllerConstants;


/*
 * controller display customer wish List  in drop-down on header  
 */
@Controller("WishListDropDownComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.wishListDropDownComponent)
public class WishListDropDownComponentController extends AbstractAcceleratorCMSComponentController<WishListDropDownComponentModel> {

	private static final Logger LOG = LoggerFactory.getLogger(WishListDropDownComponentController.class);
	
	@Autowired
	SalonWishlistFacade salonWishlist2Facade;

	@Override
	protected void fillModel(HttpServletRequest request, Model model, WishListDropDownComponentModel component) {

		LOG.debug("customer drop down wish List component");

		List<Wishlist2Data> wishlist2EntryData = salonWishlist2Facade.getCurrentCustomerAllWishList();
		
		model.addAttribute("allWishList", wishlist2EntryData);
		
	}

}
