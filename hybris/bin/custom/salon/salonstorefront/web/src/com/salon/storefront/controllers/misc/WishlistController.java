package com.salon.storefront.controllers.misc;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.salon.core.wishlist.exception.NoSuchWishListFoundException;
import com.salon.core.wishlist.exception.WishListAlreadyExistException;
import com.salon.facades.wishEntries.data.Wishlist2Data;
import com.salon.facades.wishEntries.data.Wishlist2EntryData;
import com.salon.facades.wishlist.SalonWishlistFacade;
import com.salon.storefront.controllers.ControllerConstants;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

@Controller
@RequestMapping({ "/wishlist", "/my-account" })
public class WishlistController extends AbstractPageController {

	private static final Logger LOG = LoggerFactory.getLogger(WishlistController.class);

	private static final String DEFAULT_DESCRIPTION = "My default wishlist";
	private static final String CUSTOMER_ALL_WISHLIST = "/my-account/favourites";
	private static final String FAVORITES_PAGE = "favorite";
	private static final String ALL_FAVORITES_LIST_PAGE = "favorites";
	private static final String WISHLIST_PATH_VARIABLE_PATTERN = "/{wishListId:.*}";
	private static final String WISHLIST_FAVORITE = "/my-account/favourite/";

	Pattern WISHLIST_NAME_REGEX = Pattern.compile("^[a-zA-Z0-9][a-zA-Z0-9_ -]*$");
	Pattern WISHLIST_ID_REGEX = Pattern.compile("^[0-9 ]*$");
	@Resource
	SalonWishlistFacade salonWishlistFacade;

	/**
	 * Give current customer all wish list
	 * 
	 */
	@RequestMapping(value = "/favourites", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCustomerWishList(final Model model) throws CMSItemNotFoundException {
		LOG.debug("Gettting wishList for customer");

		List<Wishlist2Data> wishlist2EntryData = salonWishlistFacade.getCurrentCustomerAllWishList();
		model.addAttribute("customerWishList", wishlist2EntryData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ALL_FAVORITES_LIST_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ALL_FAVORITES_LIST_PAGE));
		return getViewForPage(model);
	}

	/**
	 * @param productCode
	 *            productCode
	 * @param user
	 *            Give all existing wishlist on popup and if there are no wish
	 *            list then it will create default wishList with customer name
	 *            and bind the product to popup
	 */
	@RequestMapping(value = "/getWishList", method = RequestMethod.GET, produces = "application/json")
	public String fetchWishlists(@RequestParam("productCode") final String code, final Model model) {

		LOG.debug("Getting wish list for current customer ");
		List<Wishlist2Data> wishlist2Data = salonWishlistFacade.getWishlists();

		if (CollectionUtils.isNotEmpty(wishlist2Data)) {
			LOG.debug("Binding product for popup ");
			model.addAttribute("wishListData", wishlist2Data);
			model.addAttribute("productCode", code);
		}

		else {
			model.addAttribute("invalidUser", Boolean.TRUE);
		}

		return ControllerConstants.Views.Fragments.Wishlist.WishListPopUp;
	}

	/**
	 * Add a product to the wishlist
	 *
	 * 
	 * @param code
	 *            product code
	 * @param wishlistName
	 * 
	 * 
	 */

	@RequestMapping(value = "/add-to-wishlist", method = RequestMethod.POST, produces = "application/json")
	public String addToWishlist(@RequestParam("productCode") final String productCode,
			@RequestParam(value = "wishlistName", required = false) final String existingwishlistName,
			@RequestParam(value = "newWishlistName", required = false) final String newWishlistName,
			final Model model) {

		String wishListName = StringUtils.isNotBlank(newWishlistName) ? newWishlistName : existingwishlistName;

		LOG.debug("add Product to the  Wish list for current User....");

		/* if wish List is empty or contains spaecial character */
		if (!isvalid(wishListName, model)) {
			return ControllerConstants.Views.Fragments.Wishlist.wishlistSucessPopup;
		}

		Wishlist2Data customerwishlist = salonWishlistFacade.getMatchedWishlist(wishListName.trim());

		/* if new wish List Already exists */
		if (StringUtils.isNotBlank(newWishlistName) && customerwishlist != null) {

			model.addAttribute("message", getMessageSource().getMessage("text.wishlist.alredayExist",
					new Object[] { newWishlistName }, getI18nService().getCurrentLocale()));
			return ControllerConstants.Views.Fragments.Wishlist.wishlistSucessPopup;
		}
		/*
		 * if newWishlistName not Empty and Not exists then it will create
		 * wishlist and add the product
		 */
		else if (StringUtils.isNotBlank(newWishlistName) && customerwishlist == null) {
			LOG.debug("new wishList is.." + newWishlistName);

			addToNewWishList(newWishlistName.trim(), DEFAULT_DESCRIPTION, productCode, Boolean.TRUE, model);

		} else {
			/*
			 * if user select Existing wish list then it will first check
			 * product is already exist or not if not then it will add product
			 * to wish List
			 */

			addToExistingWishListWishList(existingwishlistName, DEFAULT_DESCRIPTION, productCode, Boolean.FALSE, model,
					customerwishlist);

		}

		return ControllerConstants.Views.Fragments.Wishlist.wishlistSucessPopup;
	}

	/**
	 * Remove product from the wishlist
	 * 
	 * @param code
	 *            product code
	 * @param wishlistId
	 *            wishlistId
	 */
	@RequestMapping(value = "/removeProduct", method = RequestMethod.POST, produces = "application/json")
	public String removeProductFromWishlist(@RequestParam("productCode") final String productCode,
			@RequestParam(value = "wishListId") final String wishListId, final Model model,
			final RedirectAttributes redirectAttrs) {

		if (StringUtils.isNotBlank(wishListId) && StringUtils.isNotBlank(productCode)) {
			try {
				salonWishlistFacade.removeProductFromwishList(wishListId, productCode);
			} catch (NoSuchWishListFoundException e) {
				LOG.error("wish List not found");
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER, "wisList.not.found",
						null);
			}
		}

		return REDIRECT_PREFIX + WISHLIST_FAVORITE + wishListId;

	}

	/**
	 * Remove wishlist
	 * 
	 * @param wishlistName
	 *            wishlist Name
	 * 
	 *            It will remove wish list
	 */
	@RequestMapping(value = "/removeWishList/{wishlistName}", method = RequestMethod.GET)
	public String removeWishlist(@PathVariable(value = "wishlistName") final String wishListName,final RedirectAttributes redirectAttrs) {

		LOG.debug("removing wish List " + wishListName);
		if (StringUtils.isNotBlank(wishListName)) {
			salonWishlistFacade.removeWishList(wishListName.trim());
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.wishlist.deleted", null);
		}

		return REDIRECT_PREFIX + CUSTOMER_ALL_WISHLIST;

	}

	/**
	 * Removeing wishlistEntry from wishList
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * 
	 */
	@RequestMapping(value = "/removeAllProduct" + WISHLIST_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String removeAllProductFromWishlist(@PathVariable(value = "wishListId") final String wishListId,
			final Model model, final RedirectAttributes redirectAttrs) {

		if (StringUtils.isNotBlank(wishListId)) {
			try {
				salonWishlistFacade.removeAllProductFromwishList(wishListId.trim());
			} catch (NoSuchWishListFoundException e) {
				LOG.error("wish List not found");
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER, "wisList.not.found",
						null);
			} catch (ModelSavingException e) {
				LOG.error("Failed to remove product");
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"text.wishlist.error", null);
			}
		}

		return REDIRECT_PREFIX + WISHLIST_FAVORITE + wishListId;

	}

	/**
	 * Give particualr wish list by id
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * 
	 */
	@RequestMapping(value = "/favourite" + WISHLIST_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String getWishListInfo(final Model model, @PathVariable("wishListId") final String wishListId,
			final RedirectAttributes redirectAttrs) throws CMSItemNotFoundException {

		LOG.debug("favourite list");

		// validation for number
		if (StringUtils.isNotBlank(wishListId) && !WISHLIST_ID_REGEX.matcher(wishListId).matches()) {
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"system.error.page.not.found", null);
			return REDIRECT_PREFIX + CUSTOMER_ALL_WISHLIST;
		}
		if (StringUtils.isNotBlank(wishListId)) {
			Wishlist2Data wishlistData;
			try {
				wishlistData = salonWishlistFacade.getWishlistById(wishListId.trim());
				model.addAttribute("wishListData", wishlistData);
			} catch (NoSuchWishListFoundException e) {
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"system.error.page.not.found", null);
				return REDIRECT_PREFIX + CUSTOMER_ALL_WISHLIST;
			}

		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(FAVORITES_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FAVORITES_PAGE));

		return getViewForPage(model);
	}

	/**
	 * Edit wish List name
	 * 
	 * @param wishListId
	 *            wishListId(pk)
	 * @param wishListName
	 *            wishListName
	 * 
	 */
	@RequestMapping(value = "/edit-wishlistname", method = RequestMethod.POST, produces = "application/json")
	@RequireHardLogIn
	public String editWishListName(final Model model, @RequestParam("wishListName") final String newWishListName,
			@RequestParam("wishListId") final String wishListId, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException, NoSuchWishListFoundException {

		LOG.debug("edit wish list name");

		// validation for special character
		if (StringUtils.isNotBlank(newWishListName)) {

			if (!WISHLIST_NAME_REGEX.matcher(newWishListName).matches()) {
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"wishlist.special.char.notallowed", null);
				return REDIRECT_PREFIX + WISHLIST_FAVORITE + wishListId;
			}
		}

		if (StringUtils.isNotBlank(newWishListName) && StringUtils.isNotBlank(wishListId)) {

			try {
				model.addAttribute("wishListData",
						salonWishlistFacade.editwishlistName(newWishListName.trim(), wishListId));
			} catch (WishListAlreadyExistException e) {

				LOG.error("wishlist exist with same name");
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"text.wishlist.already.found", null);
			}

		} else {
			LOG.debug("wishlist name empty");
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.wishlist.enter.wishlistname", null);
		}

		return REDIRECT_PREFIX + WISHLIST_FAVORITE + wishListId;
	}

	/**
	 * creating new List
	 * 
	 * @param wishListName
	 *            wishListName
	 * 
	 */

	@RequestMapping(value = "/create-wishlist", method = RequestMethod.POST, produces = "application/json")
	@RequireHardLogIn
	public String crateNewWishList(final Model model, @RequestParam("wishListName") final String newWishListName,
			final RedirectAttributes redirectAttrs) throws CMSItemNotFoundException {

		LOG.debug("create new wish list ");
		if (StringUtils.isNotBlank(newWishListName)) {

			// validation for special character
			if (!WISHLIST_NAME_REGEX.matcher(newWishListName).matches()) {
				GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"wishlist.special.char.notallowed", null);
			} else {
				Wishlist2Data wishlistData = salonWishlistFacade.createWishlist(newWishListName.trim(),
						DEFAULT_DESCRIPTION);

				if (null != wishlistData) {
					GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER,
							"text.wishlist.created", null);

				} else {
					GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"text.wishlist.wishlistalreadyExist", null);
				}
			}
		} else {
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.wishlist.enter.wishlistname", null);
		}

		return REDIRECT_PREFIX + CUSTOMER_ALL_WISHLIST;
	}

	private boolean productExistsInWishlist(Wishlist2Data wishlist, String productCode) {
		Iterator<Wishlist2EntryData> wishlistEntries = wishlist.getEntries().iterator();
		while (wishlistEntries.hasNext()) {
			Wishlist2EntryData wishlistEntry = wishlistEntries.next();
			if (wishlistEntry.getProduct().getCode().equals(productCode)) {
				return true;
			}
		}
		return false;
	}

	/* Validation for wishList empty and special character */
	public Boolean isvalid(final String wishListName, final Model model) {
		if (StringUtils.isBlank(wishListName)) {
			model.addAttribute("message", getMessageSource().getMessage("text.wishlist.enter.wishlistname", null,
					getI18nService().getCurrentLocale()));
			return Boolean.FALSE;
		} else if (!WISHLIST_NAME_REGEX.matcher(wishListName).matches()) {
			model.addAttribute("message", getMessageSource().getMessage("wishlist.special.char.notallowed", null,
					getI18nService().getCurrentLocale()));
			return Boolean.FALSE;
		}
		return Boolean.TRUE;

	}

	/*
	 * This method will add product to New WishList
	 */
	public void addToNewWishList(String wishListName, String description, String productCode, Boolean isNew,
			Model model) {
		LOG.debug("Add product to new wishList." + wishListName);
		if (productCode.contains(",")) {
			String[] code = productCode.split(",");

			LOG.debug("Adding multiple product to wishList." + wishListName);
			for (String prodCode : code) {
				addToCustomerWishList(wishListName, description, prodCode, isNew);
				
				//Once product add into new wishList again It should not crate newWishList means (isNew) will be False
				isNew=Boolean.FALSE;
			}

		} else {

			addToCustomerWishList(wishListName, description, productCode, isNew);

		}
		model.addAttribute("message", getMessageSource().getMessage("text.wishlist.addPorduct",
				new Object[] { wishListName }, getI18nService().getCurrentLocale()));
	}

	/*
	 * This method will add product to existing wishList and here we are
	 * checking if product already exists into wishlist or not.
	 */
	public void addToExistingWishListWishList(String existingwishlistName, String description, String productCode,
			Boolean isNew, Model model, Wishlist2Data customerwishlist) {
		LOG.debug(" Add to existing wishlist.." + existingwishlistName);
		Boolean alreadyExistProduct = Boolean.FALSE;
		if (null != customerwishlist && StringUtils.isNotBlank(productCode)
				&& StringUtils.isNotBlank(existingwishlistName)) {

			// If multiple Product we are adding.
			if (productCode.contains(",")) {
				LOG.debug("Adding multiple product to Existing wishList." + existingwishlistName);
				String[] code = productCode.split(",");
				int size = code.length;
				int counter = 0;
				for (String prodCode : code) {

					if (productExistsInWishlist(customerwishlist, prodCode)) {
						counter++;

					} else {
						addToCustomerWishList(existingwishlistName, description, prodCode, isNew);
					}

				}
				if (counter == 0) {
					model.addAttribute("message", getMessageSource().getMessage("text.wishlist.addPorduct",
							new Object[] { existingwishlistName }, getI18nService().getCurrentLocale()));
				} else {
					model.addAttribute("message", getMessageSource().getMessage("text.wishlist.addPorduct.available",
							new Object[] { size - counter, counter }, getI18nService().getCurrentLocale()));
				}

			}
			// Only single product adding to existing wishList
			else {

				alreadyExistProduct = productExistsInWishlist(customerwishlist, productCode);

				if (BooleanUtils.isTrue(alreadyExistProduct)) {
					model.addAttribute("existingProduct", alreadyExistProduct);
				}

				else {

					addToCustomerWishList(existingwishlistName.trim(), description, productCode, isNew);
					model.addAttribute("message", getMessageSource().getMessage("text.wishlist.addPorduct",
							new Object[] { existingwishlistName }, getI18nService().getCurrentLocale()));
				}
			}
		}

	}

	/*
	 * Add product to wishList common method
	 */
	public void addToCustomerWishList(String wishListName, String description, String productCode, Boolean isNew) {
		salonWishlistFacade.addToWishlist(wishListName.trim(), description, productCode, isNew);

	}

}
