/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.salon.storefront.secureportaladdon.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.customerticketingfacades.TicketFacade;
import de.hybris.platform.customerticketingfacades.data.TicketCategory;
import de.hybris.platform.customerticketingfacades.data.TicketData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.salon.core.secureportaladdon.exceptions.CustomerAlreadyExistsException;
import com.salon.core.secureportaladdon.exceptions.MissingParameterFieldException;
import com.salon.core.secureportaladdon.exceptions.NoSalesRepFoundException;
import com.salon.core.secureportaladdon.exceptions.NoSuchB2BUnitFoundException;
import com.salon.facades.addon.data.B2BUnitForm;
import com.salon.facades.data.ProspectiveCustomerData;
import com.salon.facades.secureportaladdon.data.B2BRegistrationData;
import com.salon.facades.secureportaladdon.facades.B2BRegistrationFacade;
import com.salon.storefront.secureportaladdon.constant.SecureportaladdonWebConstants;
import com.salon.storefront.secureportaladdon.forms.B2BRegistrationForm;
import com.salon.storefront.secureportaladdon.forms.ProspectCustomerRegistrationForm;
import com.salon.storefront.secureportaladdon.forms.RegistrationForm;
import com.salon.storefront.validators.ProspectiveCustomerRegValidator;
import com.salon.storefront.validators.SalonRegistrationValidator;

/**
 * Registration page controller: Handles Get and Post request and dispatches relevant work flow facades and necessary
 * services
 */
@RequestMapping(value = SecureportaladdonWebConstants.RequestMappings.ACCOUNT_REGISTRATION)
public class B2BRegistrationController extends AbstractB2BRegistrationController
{

	private static final Logger LOG = LoggerFactory.getLogger(B2BRegistrationController.class);
	public static final String GET_REGISTER_PAGE = "registration";
	private static final String B2B_REGISTRATION_CMS_PAGE = "B2BRegistrationPage";
	private static final String B2B_REGISTRATION_PAGE = "B2BRegistrationPage";
	private static final String PROSPECT_REGISTRATION_PAGE = "prospectiveCustomerRegistration";
	private static final String CALL_ME_BACK = "Call back request";
	private static final String CUSTOMER_NAME = "Customer name: ";
	private static final String CONTACT =  "Contact: ";
	private static final String PREFERRED_TIME = "Preferred time to contact : ";
	private static final String REASON = "Reason : ";
	private final static class MessageKeys
	{
		public static final String REGISTER_SUBMIT_CONFIRMATION = "text.secureportal.register.submit.confirmation";
		public static final String REGISTER_ACCOUNT_EXISTING = "text.secureportal.register.account.existing";
		public static final String SCP_LINK_CREATE_ACCOUNT = "text.secureportal.link.createAccount";
	}

	private static final String HOME_REDIRECT = REDIRECT_PREFIX + ROOT;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "b2bRegistrationFacade")
	private B2BRegistrationFacade b2bRegistrationFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "securePortalRegistrationValidator")
	private Validator registrationValidator;

	@Resource(name = "b2bRegistrationValidator")
	private SalonRegistrationValidator b2bRegistrationValidator;

	@Resource(name = "prospectiveCustomerRegValidator")
	private ProspectiveCustomerRegValidator prospectiveCustomerRegValidator;
	
	@Resource(name = "defaultTicketFacade")
	private TicketFacade ticketFacade;
	
	

	// country attribute to use in the prospect customer form for  countr field
	@ModelAttribute("country")
	public List<CountryData> getCountry()
	{
		return checkoutFacade.getDeliveryCountries();
	}


	
	//render the register page 
	@RequestMapping(method = RequestMethod.GET)
	public String showRegistrationPage(final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{

		if (getCmsSiteService().getCurrentSite().isEnableRegistration())
		{
			final ContentPageModel getRegisterPage = getContentPageForLabelOrId(getRegistrationCmsPage());
			storeCmsPageInModel(model, getRegisterPage);
			setUpMetaDataForContentPage(model, getRegisterPage);
			return getViewForPage(model);
		}
		return HOME_REDIRECT;
	}
	
	
	

	@RequestMapping(method = RequestMethod.POST)
	public String submitRegistration(final RegistrationForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpSession session, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{

		populateModelCmsContent(model, getContentPageForLabelOrId(getRegistrationCmsPage()));
		model.addAttribute(form);

		getRegistrationValidator().validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			return getRegistrationView();
		}

		try
		{
			b2bRegistrationFacade.register(convertFormToData(form));
		}
		catch (final CustomerAlreadyExistsException e) //NOSONAR
		{
			LOG.error("Failed to register account. Account already exists.");
			GlobalMessages.addErrorMessage(model, Localization.getLocalizedString(MessageKeys.REGISTER_ACCOUNT_EXISTING));
			return getRegistrationView();
		}

		GlobalMessages.addInfoMessage(model, Localization.getLocalizedString(MessageKeys.REGISTER_SUBMIT_CONFIRMATION));

		return getDefaultLoginPage(false, session, model);
	}
	
	


	/**
	 * @param form
	 *           Form data as submitted by user
	 * @return A DTO object built from the form instance
	 */
	protected B2BRegistrationData convertFormToData(final RegistrationForm form)
	{
		final B2BRegistrationData registrationData = new B2BRegistrationData();
		BeanUtils.copyProperties(form, registrationData);
		return registrationData;
	}

	@Override
	protected Validator getRegistrationValidator()
	{
		return registrationValidator;
	}

	@Override
	protected String getRegistrationView()
	{
		return SecureportaladdonWebConstants.Views.REGISTRATION_PAGE;
	}

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Override
	protected String getRegistrationCmsPage()
	{
		return SecureportaladdonWebConstants.CMS_REGISTER_PAGE_NAME;
	}

	@Override
	protected void populateModelCmsContent(final Model model, final ContentPageModel contentPageModel)
	{

		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);

		final Breadcrumb registrationBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage(MessageKeys.SCP_LINK_CREATE_ACCOUNT, null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(registrationBreadcrumbEntry));
	}

	
	@Override
	protected String getView()
	{
		return SecureportaladdonWebConstants.Views.LOGIN_PAGE;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return HOME_REDIRECT;
	}

	// show list of emails related to the customer number
	@ResponseBody
	@RequestMapping(value = "/showEmails", method = RequestMethod.POST)
	public ResponseEntity<?> submitCustomerRegistration(@RequestParam("customerNumber") final String customerNumber,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		B2BUnitForm b2bUnitData = null;
		LOG.info(" Showing associated emails for customer with B2BUnit ID : " + customerNumber + ".");
		try
		{
			b2bUnitData = b2bRegistrationFacade.getRegisteredEmails(customerNumber);
		}
		catch (final NoSuchB2BUnitFoundException e)
		{
			LOG.error("Failed to register account. B2B unit cannot be found.");
			return ResponseEntity.status(400).body(getMessageSource().getMessage(("text.b2bunit.not.found"),null,getI18nService().getCurrentLocale()));		}
		catch (final MissingParameterFieldException e)
		{
			LOG.error("Failed to register account. The parameter field is empty or null.");
			return ResponseEntity.status(400).body(getMessageSource().getMessage(("text.parameter.not.null"),null,getI18nService().getCurrentLocale()));
		}
		return ResponseEntity.ok(b2bUnitData);
	}

	// customer registration method
	@RequestMapping(value = "/salonCustomerRegistration", method = RequestMethod.POST)
	public ResponseEntity<?> salonCustomerRegistration(@RequestParam("customerNumber") final String customerNumber,
			@RequestParam("emailId") final String email, final HttpServletRequest request, final Model model)
			throws CMSItemNotFoundException
	{
		LOG.info("Registering customer with email ID and B2BUnit ID  : " + customerNumber + " " + email + "respectively.");
		try
		{
			
			b2bRegistrationFacade.registerCustomer(customerNumber, email);
		}
		catch (final CustomerAlreadyExistsException e) //NOSONAR
		{
			LOG.error("Failed to register account. Account already exists.");
			return ResponseEntity.status(400).body(getMessageSource().getMessage(("text.customer.exists"),null,getI18nService().getCurrentLocale()));
		}
		catch (final NoSuchB2BUnitFoundException e)
		{
			LOG.error("Failed to register account.B2BUnit can not be found.");
			return ResponseEntity.status(400).body(getMessageSource().getMessage(("text.b2bunit.not.found"),null,getI18nService().getCurrentLocale()));
			}
		catch (final MissingParameterFieldException e)
		{
			LOG.error("Failed to register account. The parameter field is empty or null.");
			return ResponseEntity.status(400).body(getMessageSource().getMessage(("text.parameter.not.null"),null,getI18nService().getCurrentLocale()));
		}
		return ResponseEntity.ok(getMessageSource().getMessage(("text.register.message"),null,getI18nService().getCurrentLocale()));
	}

	/* Token Validation */

	@RequestMapping(value = "/account/activate", method = RequestMethod.GET)

	public String doRegistration(@RequestParam("token") final String token, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(token))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"b2bRegister.token.invalidated");
			return REDIRECT_PREFIX + "/login";
		}
		if (!StringUtils.isBlank(token))
		{
			final B2BRegistrationForm form = new B2BRegistrationForm();
			form.setToken(token);
			model.addAttribute(form);
			storeCmsPageInModel(model, getContentPageForLabelOrId(B2B_REGISTRATION_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(B2B_REGISTRATION_CMS_PAGE));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("b2bRegister.title"));

			try
			{
				if (b2bRegistrationFacade.validateToken(token))
				{
					final ContentPageModel getB2BRegisterPage = getContentPageForLabelOrId(B2B_REGISTRATION_PAGE);
					storeCmsPageInModel(model, getB2BRegisterPage);
					setUpMetaDataForContentPage(model, getB2BRegisterPage);
					return getViewForPage(model);

				}
				else
				{
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"b2bRegister.token.invalidated");
				}
			}
			
			catch (final Exception e){
				 if (e instanceof TokenInvalidatedException )
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"b2bRegister.token.invalidated");

			}
				 if(e instanceof NoSalesRepFoundException ){
					 LOG.warn("No Sales rep found for this b2b customer" + e.getMessage());
				 }
				 if(e instanceof IllegalArgumentException){
					 GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								"b2bRegister.token.invalidated");
				 }
				 else{
						GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								"text.secureportal.register.error");

					}
				 }
			}
			

		return REDIRECT_PREFIX + "/login";
	}


	// registered customer form submission
	@RequestMapping(value = "/account/activate", method = RequestMethod.POST)
	public String submitRegistration(final B2BRegistrationForm form, final BindingResult bindingResult, final Model model,
			 final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		b2bRegistrationValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			// DSPLAYING THE ERROR IN THE FORM
			prepareErrorMessage(model, B2B_REGISTRATION_CMS_PAGE);
			return getViewForPage(model);
		}
		if (!StringUtils.isBlank(form.getToken()))
		{
			try
			{

				b2bRegistrationFacade.saveB2BCustomer(form.getToken(), form.getPwd(), form.getTitleCode(), form.getName());
				model.addAttribute("pageType","register");
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation");
				return REDIRECT_PREFIX + "/login";
			}
			
			catch (final Exception e){
				 if (e instanceof TokenInvalidatedException )
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"b2bRegister.token.invalidated");
				LOG.error("Token invalid exception has occurred : " + e.getMessage());
			}
				 else if(e instanceof NoSalesRepFoundException ){
					 LOG.warn("No sales rep associated with this custmer : " + e.getMessage());
				 }
				 else if(e instanceof CustomerAlreadyExistsException ){
					 GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								"customer already exists"); 
					 LOG.error("Exception occurred as customer exists : " + e.getMessage());
				 }
				 else if(e instanceof UnknownIdentifierException){
					 LOG.error(e.getMessage());
				 }
				 else{
						GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
								"text.secureportal.register.error");
						LOG.error("Exception occurred on registering b2b customer : " + e.getMessage());
					}
				 }
		}
		return REDIRECT_PREFIX + "/login";

	}
	
	// Render Prospective Customer Form Page 
	@RequestMapping(value = "/newaccount", method = RequestMethod.POST)
	public String doNewRegistration(@ModelAttribute("name") String name, @ModelAttribute("email") String email,final Model model, HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
		LOG.debug("Redirecting to the registratration form page");
		LOG.debug("name - " +name+ " " + "email - " + email);
		ProspectCustomerRegistrationForm form = new ProspectCustomerRegistrationForm();
		form.setName(name);
		form.setEmail(email);
		model.addAttribute(form);
		final ContentPageModel getProspectCustomerRegisterPage = getContentPageForLabelOrId(PROSPECT_REGISTRATION_PAGE);
		storeCmsPageInModel(model, getProspectCustomerRegisterPage);
		setUpMetaDataForContentPage(model, getProspectCustomerRegisterPage);
		return getViewForPage(model);
	}

	// Submit the prospective customer informations
	@RequestMapping(value="/newaccount/submit", method=RequestMethod.POST)
	public String submitNewCustomerInfo(@ModelAttribute("prospectCustomerRegistrationForm")ProspectCustomerRegistrationForm form,final Model model, final BindingResult bindingResult,final RedirectAttributes redirectModel) throws CMSItemNotFoundException, CustomerAlreadyExistsException{
		
		//validate prospect customer
		prospectiveCustomerRegValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, PROSPECT_REGISTRATION_PAGE);
		    model.addAttribute("prospectCustomerRegistrationForm",form);
			return getViewForPage(model);
		}
		// set the entire form value to the data
		final ProspectiveCustomerData  prospectiveCustomer = new ProspectiveCustomerData();
		prospectiveCustomer.setName(form.getName());
		prospectiveCustomer.setEmail(form.getEmail());
		prospectiveCustomer.setSalonName(form.getSalonName());
		prospectiveCustomer.setNoOfEmp(form.getNoOfEmp());
		prospectiveCustomer.setSalonSize(form.getSalonSize());
		prospectiveCustomer.setPosition(form.getPosition());
		prospectiveCustomer.setPhone(form.getPhone());
		prospectiveCustomer.setVatNo(form.getVatNo());
		//set the address value
		final AddressData address = new AddressData();
		//street as line1
		address.setLine1(form.getAddress().getLine1());
		address.setTown(form.getAddress().getTownCity());
		address.setPostalCode(form.getAddress().getPostcode());
		address.setDistrict(form.getDistrict());
		CountryData country = new CountryData();
		country.setIsocode(form.getAddress().getCountryIso());
		address.setCountry(country);
		address.setRegion(null);
		prospectiveCustomer.setAddress(address);
		//in the facade send the data 
		try {
			b2bRegistrationFacade.saveProspectiveCustomer(prospectiveCustomer);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.request.confirmed");
			LOG.debug("Creating a prospective customer" + prospectiveCustomer.getEmail());
			return REDIRECT_PREFIX + "/login";
		} catch (final Exception e) {
			if (e instanceof CustomerAlreadyExistsException) {
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "email.exists");
				LOG.error(e.getMessage());
				LOG.error("Error occurred in saving customer with email id :" + prospectiveCustomer.getEmail());
				return REDIRECT_PREFIX + "/register";
			}
			if (e instanceof RuntimeException) {
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"text.error.occured");
				LOG.error(e.getMessage());
				LOG.error("Error occurred in saving customer with email id :" + prospectiveCustomer.getEmail());
				return REDIRECT_PREFIX + "/register";
			}
		}
		return REDIRECT_PREFIX + "/login";
	}

	// to raise ticket for the call me back request
	@RequestMapping(value="/callmeback", method=RequestMethod.POST)
	public String addSupportTicket(@RequestParam("fullName") final String name,@RequestParam("phoneNo") final String phone,
			@RequestParam("time") final String time,@RequestParam("reason") final String reason,final RedirectAttributes redirectModel){
		
			final TicketData ticketData = new TicketData();
			ticketData.setSubject(CALL_ME_BACK);
			ticketData.setMessage(CUSTOMER_NAME + name + "  " + CONTACT + phone + " "
			+ PREFERRED_TIME + time + " " + REASON + reason);
			ticketData.setTicketCategory(TicketCategory.ENQUIRY);
			try{
			ticketFacade.createTicket(ticketData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"callback.request.confirmed");
			LOG.debug("Created the call me back request event ticket successfully");
			return REDIRECT_PREFIX + "/register";
			}
			catch(final RuntimeException e){
				LOG.error("Error occured in registering for call back : " + e.getMessage());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "callback.request.failed");
				return REDIRECT_PREFIX + "/login";
				
			}	
	}
	
	
	protected void prepareErrorMessage(final Model model, final String page) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(page));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
	}

}
