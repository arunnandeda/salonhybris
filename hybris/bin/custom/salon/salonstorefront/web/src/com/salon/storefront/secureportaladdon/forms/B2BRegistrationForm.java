package com.salon.storefront.secureportaladdon.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


public class B2BRegistrationForm
{

	private String titleCode;
	private String name;
	private String token;
	private String pwd;
	private String checkPwd;

	public B2BRegistrationForm()
	{

	}

	/**
	 * @return the titleCode
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the firstLastName
	 */
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getName()
	{
		return name;
	}

	/**
	 * @param firstLastName
	 *           the firstLastName to set
	 */
	public void setName(final String firstAndLastName)
	{
		this.name = firstAndLastName;
	}


	/**
	 * @return the pwd
	 */
	@NotNull(message = "{b2bRegister.pwd.invalid}")
	@Size(min = 8, max = 255, message = "{b2bRegister.pwd.invalid}")
	public String getPwd()
	{
		return pwd;
	}

	/**
	 * @param pwd
	 *           the pwd to set
	 */
	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	/**
	 * @return the checkPwd
	 */
	@Size(max = 255, message = "{b2bRegister.pwd.invalid}")
	public String getCheckPwd()
	{
		return checkPwd;
	}

	/**
	 * @param checkPwd
	 *           the checkPwd to set
	 */
	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	/**
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * @param token
	 *           the token to set
	 */
	public void setToken(final String token)
	{
		this.token = token;
	}
}
