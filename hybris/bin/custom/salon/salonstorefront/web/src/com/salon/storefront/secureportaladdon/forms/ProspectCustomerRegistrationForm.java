package com.salon.storefront.secureportaladdon.forms;

import org.hibernate.validator.constraints.NotEmpty;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class ProspectCustomerRegistrationForm {

	private String name;
	private String email;
	private String phone;
	
	private String salonName;
	private String noOfEmp;
	private String salonSize;
	private String position;
	private String vatNo;

	private String district;
	private AddressForm address;
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getSalonName() {
		return salonName;
	}
	public void setSalonName(String salonName) {
		this.salonName = salonName;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getNoOfEmp() {
		return noOfEmp;
	}
	public void setNoOfEmp(String noOfEmp) {
		this.noOfEmp = noOfEmp;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getSalonSize() {
		return salonSize;
	}
	public void setSalonSize(String salonSize) {
		this.salonSize = salonSize;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public String getVatNo() {
		return vatNo;
	}
	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}
	
	@NotEmpty(message = "{text.secureportal.register.field.mandatory}")
	public AddressForm getAddress() {
		return address;
	}
	public void setAddress(AddressForm address) {
		this.address = address;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	
}
