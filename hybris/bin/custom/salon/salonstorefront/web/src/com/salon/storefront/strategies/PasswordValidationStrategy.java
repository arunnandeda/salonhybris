package com.salon.storefront.strategies;

import org.springframework.validation.Errors;


public interface PasswordValidationStrategy
{

	//validate password pattern
	public boolean validateUserPassword(String pwd);

	//validate password length
	public void validatePasswordLength(Errors errors, String pwd);

	//validate password does not contain username
	public void checkPassword(Errors errors, String pwd, String name);
}