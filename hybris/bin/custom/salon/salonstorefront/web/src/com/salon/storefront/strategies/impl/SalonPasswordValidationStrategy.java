package com.salon.storefront.strategies.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import com.salon.storefront.strategies.PasswordValidationStrategy;



public class SalonPasswordValidationStrategy implements PasswordValidationStrategy
{

	// match if the password has alphanumeric characters
	public static final Pattern PASSWORD_REGEX = Pattern
			.compile("^(?=.*[0-9])(?=.*[a-z]).{6,30}$");
	//^(?=.*[0-9])(?=.*[a-z]).{2,30}$
	//^(?=\\D*\\d)(?=[^a-z]*)(?=[^A-Z]*)[a-zA-Z0-9~@#$%^&*+=`|{}:;!.?\\\"()\\[\\]-]{6,30}$
	@Override
	public void checkPassword(final Errors errors, final String pwd, final String name)
	{
		if (pwd.indexOf(name.toLowerCase()) != -1)
		{
			errors.rejectValue("pwd", "text.password.username");
		}

	}

	@Override
	public boolean validateUserPassword(final String pwd)
	{
		final Matcher matcher = PASSWORD_REGEX.matcher(pwd);
		return matcher.matches();
	}

	@Override
	public void validatePasswordLength(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd))
		{
			errors.rejectValue("pwd", "text.password.empty");
		}
		else if (StringUtils.length(pwd) < 6)
		{
			errors.rejectValue("pwd", "text.b2bRegister.password.length");
		}
		else if(StringUtils.length(pwd) >30){
			errors.rejectValue("pwd", "text.password.length");
		}
		
		else if (!this.validateUserPassword(pwd))
		{
			errors.rejectValue("pwd", "text.password.pattern");
		}

	}


}