package com.salon.storefront.validators;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.salon.storefront.secureportaladdon.forms.B2BRegistrationForm;
import com.salon.storefront.secureportaladdon.forms.ProspectCustomerRegistrationForm;
@Component("prospectiveCustomerRegValidator")
public class ProspectiveCustomerRegValidator implements Validator {

	// match if the contact has alphanumeric characters
		public static final Pattern NUMERIC_REGEX = Pattern
				.compile("[0-9]+");

		
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return B2BRegistrationForm.class.equals(aClass);
	}
	
	@Override
	public void validate(final Object object, final Errors errors)
	{
	
		final ProspectCustomerRegistrationForm form = (ProspectCustomerRegistrationForm) object;
		String salonName = form.getSalonName();
		String phone = form.getPhone();
		String noOfEmp = form.getNoOfEmp();
		String salonSize = form.getSalonSize();
		String vatNo = form.getVatNo();
		String street = form.getAddress().getLine1();
		String city =  form.getAddress().getTownCity();
		String zip = form.getAddress().getPostcode();
		String district = form.getDistrict();
		String country = form.getAddress().getCountryIso();
		
		
		validateSalonName(errors,salonName);
		validateVatNo(errors,vatNo);
		validateStreet(errors,street);
		validateCity(errors,city);
		validateZip(errors,zip);
		validateDistrict(errors,district);
		validateCountry(errors,country);
		validatePhone(errors,phone);
		validateSalonSize(errors,salonSize);
		validateEmpSize(errors,noOfEmp);
		
	}
	
	//validate salon name
	protected void validateSalonName(final Errors errors, final String salonName){
		if (StringUtils.isBlank(salonName)){
			errors.rejectValue("salonName","salon.name.invalid");
		}
		else if (StringUtils.length(salonName) > 255)
		{
			errors.rejectValue("salonName", "salon.name.invalid");
		}
	}
	
	//validate vat no
	protected void validateVatNo(final Errors errors, final String vatNo){
		if (StringUtils.isBlank(vatNo)){
			errors.rejectValue("vatNo","salon.vat.invalid");
		}
		else if (StringUtils.length(vatNo) > 255 || !validateNumbericValue(vatNo))
		{
			errors.rejectValue("vatNo", "salon.vat.invalid");
		}
	}
	
	// validate street
	protected void validateStreet(final Errors errors, final String street){
		if (StringUtils.isBlank(street)){
			errors.rejectValue("address.line1","salon.street.invalid");
		}
		else if (StringUtils.length(street) > 255)
		{
			errors.rejectValue("address.line1", "salon.street.invalid");
		}
	}
	
	//validate city
	protected void validateCity(final Errors errors, final String city){
		if (StringUtils.isBlank(city)){
			errors.rejectValue("address.townCity","salon.city.invalid");
		}
		else if (StringUtils.length(city) > 255)
		{
			errors.rejectValue("address.townCity", "salon.city.invalid");
		}
	}
	
	//validate zip
		protected void validateZip(final Errors errors, final String zip){
			if (StringUtils.isBlank(zip)){
				errors.rejectValue("address.postcode","salon.zip.invalid");
			}
			else if (StringUtils.length(zip) > 255 || !validateNumbericValue(zip))
			{
				errors.rejectValue("address.postcode", "salon.zip.invalid");
			}
		}
		
		//validate district
		protected void validateDistrict(final Errors errors, final String district){
			if (StringUtils.isBlank(district)){
				errors.rejectValue("district","salon.district.invalid");
			}
			else if (StringUtils.length(district) > 255)
			{
				errors.rejectValue("district", "salon.district.invalid");
			}
		}
		
		//validate country
		protected void validateCountry(final Errors errors, final String country){
			if (StringUtils.isBlank(country)){
				errors.rejectValue("address.countryIso","salon.country.invalid");
			}
			else if (StringUtils.length(country) > 255)
			{
				errors.rejectValue("address.countryIso", "salon.country.invalid");
			}
		}
		
		// validate phone number
		protected void validatePhone(final Errors errors, final String phone){
		 if (StringUtils.length(phone) > 255)
			{
			 	 if(!validateNumbericValue(phone)){
			 		errors.rejectValue("phone", "salon.phone.invalid");
			 	 }
			}
		}
		// validate Salon Size
		protected void validateSalonSize(final Errors errors, final String salonSize){
		if (StringUtils.length(salonSize) > 255) {
			if (!validateNumbericValue(salonSize)) {
				errors.rejectValue("salonSize", "salon.salonsize.invalid");
			}
		}

	}

		// validate emp size
		protected void validateEmpSize(final Errors errors, final String noOfEmp){
			 if (StringUtils.length(noOfEmp) > 255)
				{
				 	if(!validateNumbericValue(noOfEmp)){
					errors.rejectValue("noOfEmp", "salon.empsize.invalid");
				 	}
				}
			}
		

		//validate the numeric value
		public boolean validateNumbericValue(final String value)
		{
			final Matcher matcher = NUMERIC_REGEX.matcher(value);
			return matcher.matches();
		}

	
}
