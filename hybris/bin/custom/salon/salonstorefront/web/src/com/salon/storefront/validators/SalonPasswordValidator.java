package com.salon.storefront.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.salon.storefront.strategies.PasswordValidationStrategy;



@Component("salonPasswordValidator")
public class SalonPasswordValidator implements Validator
{

	@Autowired
	private PasswordValidationStrategy salonPasswordValidatorStrategy;

	private static final String UPDATE_PWD_INVALID = "updatePwd.pwd.invalid";

	public SalonPasswordValidator()
	{
		super();
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePwdForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{


		final UpdatePwdForm updatePasswordForm = (UpdatePwdForm) object;

		final String pwd = updatePasswordForm.getPwd();
		final String checkPwd = updatePasswordForm.getCheckPwd();


		if (StringUtils.isEmpty(pwd))
		{
			errors.rejectValue("pwd", UPDATE_PWD_INVALID);
		}

		comparePasswords(errors, pwd, checkPwd);
		salonPasswordValidatorStrategy.validatePasswordLength(errors, pwd);


	}

	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "updatePwd.pwd.invalid");
			}
		}
	}





}