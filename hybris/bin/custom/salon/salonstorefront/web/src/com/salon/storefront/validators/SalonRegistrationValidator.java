package com.salon.storefront.validators;



import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.salon.storefront.secureportaladdon.forms.B2BRegistrationForm;
import com.salon.storefront.strategies.PasswordValidationStrategy;


@Component("b2bRegistrationValidator")
public class SalonRegistrationValidator implements Validator
{

	@Autowired
	private PasswordValidationStrategy salonPasswordValidatorStrategy;


	public SalonRegistrationValidator()
	{
		super();
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return B2BRegistrationForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final B2BRegistrationForm registerForm = (B2BRegistrationForm) object;

		final String titleCode = registerForm.getTitleCode();
		final String name = registerForm.getName();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();


		salonPasswordValidatorStrategy.checkPassword(errors, pwd, name);
		salonPasswordValidatorStrategy.validateUserPassword(pwd);
		salonPasswordValidatorStrategy.validatePasswordLength(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);
		validateName(errors, name);
		validateTitleCode(errors, titleCode);


	}


	//compare password
	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}


	//validate name
	protected void validateName(final Errors errors, final String name)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue("name", "register.name.invalid");
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue("name", "register.name.invalid");
		}
	}

	//validate titel
	protected void validateTitleCode(final Errors errors, final String titleCode)
	{
		if (StringUtils.isEmpty(titleCode))
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
		else if (StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
	}


}