
package com.salon.storefront.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.salon.storefront.controllers.pages.AccountPageController;


@UnitTest
public class PasswordValidationStrategyUnitTest
{
	private final SalonPasswordValidationStrategy salonPasswordValidator = new SalonPasswordValidationStrategy();
	@Mock
	private RegistrationValidator registrationValidator;
	@Mock
	private Validator validator;
	private RegisterForm registerForm;
	private RegisterForm registerForm2;
	@Mock
	private Errors errors;
	private static final Logger LOG = LoggerFactory.getLogger(AccountPageController.class);

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		this.registerForm = new RegisterForm();
		this.registerForm.setFirstName("Test");
		this.registerForm.setLastName("User");
		this.registerForm.setCheckPwd("qwerty!@#$");
		this.registerForm.setPwd("qwerty!@#$");
		this.registerForm.setEmail("Test@testsite.com");
		this.registerForm.setMobileNumber("987654325");
		this.registerForm.setTitleCode("1234");
		this.registerForm2 = new RegisterForm();
		this.registerForm2.setFirstName("Test");
		this.registerForm2.setLastName("User2");
		this.registerForm2.setCheckPwd("123qwerty!@#$");
		this.registerForm2.setPwd("123qwerty!@#$");
		this.registerForm2.setEmail("test2@testsite.com");
		this.registerForm2.setMobileNumber("8987654325");
		this.registerForm2.setTitleCode("1234");
		LOG.info("The password is :" + this.registerForm.getPwd());
		LOG.info("The second password is : " + this.registerForm2.getPwd());
	}

	@Test
	public void testPasswordInputFail() throws Exception
	{
		final String pwd = this.registerForm.getPwd();
		LOG.info("The password at line 68 is :" + pwd);
		Assert.assertFalse("Password does not have numbers", this.salonPasswordValidator.validateUserPassword(pwd));
	}

	@Test
	public void testPasswordInputPass()
	{
		final String pwd2 = this.registerForm2.getPwd();
		LOG.info("The password is 76:" + pwd2);
		LOG.info("validate User Password" + this.salonPasswordValidator.validateUserPassword(pwd2));
		Assert.assertTrue("Password is alphanumeric", this.salonPasswordValidator.validateUserPassword(pwd2));
	}

	@Test
	public void testPasswordLength()
	{
		final String pwd = this.registerForm.getPwd();
		final String firstName = this.registerForm.getFirstName();
		final String lastName = this.registerForm.getLastName();
		boolean output = false;
		if (pwd.indexOf(firstName.toLowerCase()) != -1 || pwd.indexOf(lastName.toLowerCase()) != -1)
		{
			final int err = this.errors.toString().length();
			if (err != 0)
			{
				output = true;
			}
		}

		Assert.assertTrue("error returned", output);
	}

	@Test
	public void testRegexExpPass()
	{
		final String pwd = this.registerForm2.getPwd();
		LOG.info("The password is line 108 :" + pwd);
		final boolean pass = this.salonPasswordValidator.validateUserPassword(pwd);
		LOG.info(pass + " ------------------");
		Assert.assertEquals(Boolean.valueOf(true), Boolean.valueOf(pass));
	}
}