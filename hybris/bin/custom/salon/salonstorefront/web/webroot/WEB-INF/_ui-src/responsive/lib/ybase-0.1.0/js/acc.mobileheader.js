ACC.mobileHeader = {

    _autoload: [
        "setMobileMenu"
    ],
    setMobileMenu: function () {
    	/* menu mobile */
    	  $('.burger-button').on('click', function(){
    	    $('.hamburger-icon', this).toggleClass('closed');
    	    $('.menu-mobile').toggleClass('open closed');
    	    $('body').toggleClass('no-scroll');
    	  });

    	  /* mobile submenu */
    	  $('.nav-link.toggle').on('click', function(){
    	    $(this).toggleClass('open closed');
    	  });
    }
    };