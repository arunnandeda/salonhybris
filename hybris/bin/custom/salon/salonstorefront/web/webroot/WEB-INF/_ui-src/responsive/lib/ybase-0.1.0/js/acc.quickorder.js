var ACC = ACC || {}; // make sure ACC is available

if ($("#quickOrder").length > 0) {
    ACC.quickorder = {
        _autoload: [
            "bindClearQuickOrderRow",
            "bindAddSkuInputRow",
            "bindResetFormBtn",
            "bindAddToCartClick",
            "bindquickOrderGuidence",
            "bindQuickProductSearchAutocomplete",
            "bindQuickOrderRow",
        ],

        $quickOrderContainer: $('.js-quick-order-container'),
        $quickOrderMinRows: Number($('.js-quick-order-container').data('quickOrderMinRows')),
        $quickOrderMaxRows: Number($('.js-quick-order-container').data('quickOrderMaxRows')),
        $productExistsInFormMsg: $('.js-quick-order-container').data('productExistsInFormMsg'),
        $quickOrderLeavePageMsg: $('#quickOrder').data('gridConfirmMessage'),
        $hiddenSkuInput: 'input.js-hidden-sku-field',
        $addToCartBtn: $('#js-add-to-cart-quick-order-btn-top, #js-add-to-cart-quick-order-btn-bottom'),
        $resetFormBtn: $('#js-reset-quick-order-form-btn-top, #js-reset-quick-order-form-btn-bottom'),
        $productInfoContainer: '.js-product-info',
        $skuInputField: '.js-sku-input-field',
        $qtyInputField: '.js-quick-order-qty',
        $jsLiContainer: 'div.js-li-container',
        $removeQuickOrderRowBtn: '.js-remove-quick-order-row',
        $skuValidationContainer: '.js-sku-validation-container',
        $qtyValidationContainer: '.js-qty-validation-container_',
        $productItemTotal: '.js-quick-order-item-total',
        $productcurrency: '.js-quick-order-item-currency',
        $classHasError: 'has-error',
        $productListContainer:'.js-product-container',
   
      

        bindResetFormBtn: function () {
            ACC.quickorder.$resetFormBtn.on("click", ACC.quickorder.clearForm);
        },

        bindAddToCartClick: function () {
            ACC.quickorder.$addToCartBtn.on("click", ACC.quickorder.addToCart);
        },

        bindAddSkuInputRow: function () {
         /*   $(ACC.quickorder.$skuInputField).on("focusout", ACC.quickorder.handleFocusOutOnSkuInput);*/
        },

        bindClearQuickOrderRow: function () {
        	$(document).on("click",".js-remove-quick-order-row",function(event){
				$(this).parent().parent().remove(); 
				ACC.quickorder.enableDisableAddToCartBtn();
			});
          /*  $(ACC.quickorder.$removeQuickOrderRowBtn).on("mousedown", ACC.quickorder.clearQuickOrderRow);*/
        },
	bindQuickOrderRow: function () {
			$(document).on("click",".js-quickOrderAddProduct",function(event){
				var productCoode=$(this).attr("data-Code");
				
			    ACC.quickorder.handleGetQuickProduct(event,productCoode);            
			});
		
        },
        addToCart: function () {
            //check if OOS and then add to cart
            ACC.quickorder.checkIfOutOfStockProductAndThenAddToCartPopup();
            
        },
         //method to check if the out of stock products exists if yes then call a pop-up
        checkIfOutOfStockProductAndThenAddToCartPopup: function ()
        {		var outofstockcount=0;
        		var prodCode=[];
        		var qty=[];
        		var prodName=[];
        		var prodImages=[];
        	$(ACC.quickorder.$qtyInputField).each(function () {        		
        		
        		 var stockstat=$(this).attr("data-stock-level-status"); 
        		 
        		 if(stockstat=="outOfStock"){
        			 outofstockcount++;   
        			 quantity = Number($(this).val());
        			 qty.push(quantity);
            		 productCode=$(this).attr("data-product-code").trim();
            		 productName=$(this).attr("data-name").trim();            		 
            		 prodName.push(productName);
            		 prodCode.push(productCode);
            		
            		 
        		}
        		 
        	});
 /*       			//out of stock Logic  to remove comma from the array elements 
        			var productCode=prodCode.join('\r\n');        			
        			var productCodes = [];
        			productCodes = productCode.split("\r\n");
        			var productName=prodName.join('\r\n');
        			var productNames=[];
        			productNames = productName.split("\r\n");
        		 	if(outofstockcount>0)
        			 {
        		 		var row="";
        		 		var i=0;
        		 		var outOfStockProducts="<div class='OutOf-Stock-popup'><table style='margin:5px;'><tr><th>PRODUCT CODE:</th><th>PRODUCT Name:</th></tr>";
        		 		while(outofstockcount>0)
        		 		{
        		 			
        		 			row=row+"<tr><td>"+productCodes[i]+"</td><td>"+productNames[i]+"</td></tr>"
        		 			outofstockcount--;
        		 			i++;
        		 		}
        		 		
        		 		outOfStockProducts= outOfStockProducts+row+"</table><br></div>";            
        
        		 	     var textbox="<div id='content'><input id='popuptext' type='text' placeholder='Enter Mail ID'/><input id='submitMailButton' type='button' value='Submit' onclick='ACC.quickorder.removeOutOfStockProd()'/></div>" ;  
        		 	    
        		 	   //var stockStatus="<div class='AddToCartOutOfStockProducts-OutOfStock'>"+$(this).attr("data-stock-level-status")+"</div>";
            			
            			var title ="out of stock products"+outOfStockProducts+textbox;
                   		$('#cboxTitle').css('padding', '20px 18px 15px');
    				 	$.colorbox({    						
    						inline:true,
    						maxWidth:"100%",
    						opacity:0.7,
    						width:"500px",
    						height:"500px",
    						title: title,
    						close:'<span class="glyphicon glyphicon-remove"></span>',
    						
    					});
    				 	
        		}
        	//default functionality for Add to cart in-stock products
        else  
//For out of stock check we commenting this line and above logic will check out of stock and it will display display out Of stock on pop-up
        	if(outofstockcount==0)
        	{*/
        		$.ajax({
                    url: ACC.quickorder.$quickOrderContainer.data('quickOrderAddToCartUrl'),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: ACC.quickorder.getJSONDataForAddToCart(),
                    async: false,
                    success: function (response) {
                        ACC.quickorder.handleAddToCartSuccess(response);
                        
                        /*Remove quantity after add product to cart*/
                       /* $(ACC.quickorder.$qtyInputField).val(0).trigger( "focusout" );*/
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // log the error to the console
                        console.log("The following error occurred: " + textStatus, errorThrown);
                    }
                });
        	/*}*/
        	
        },

		handleGetQuickProduct: function (event,productCode) {
            var parentLi =  ACC.quickorder.$jsLiContainer;
           //check if a quickOrderTemplate div is called 
           /* var len=$(".quickOrderTemplate").length;
            if($(".quickOrderTemplate").length>0)	{
            	
            	$(".quickOrderTemplate").last().css({"border-bottom":"thick","border-bottom-style": "solid"});            	
            	
            	}*/
           
            $(event.target).val(productCode);
                if (productCode.length > 0) {
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$productInfoContainer).remove();

                    //Checking for already exists product in quick order List
                    if (ACC.quickorder.isDuplicateProduct(productCode)) {
                    	
                    	//Getting similar existing product quantity 
                    	 var qtyInputField = ACC.quickorder.findElement(ACC.quickorder.$productListContainer, ACC.quickorder.$qtyInputField+productCode).val();
                    	
                    	 //Here Getting the current quantity on same product  and increasing by 1 and then I am triggering
                    	 //focusOut event that will increase price and total.
                    	 //Focusout action event will trigger on input field ,means this action will be same as when click
                    	 //on input field(quantity input) and out from input field it will increase/decrease price.
                    	 $(ACC.quickorder.$qtyInputField+productCode).val(++qtyInputField).trigger( "focusout" );
                    	
                    	
                    }
                    else {
                        ACC.quickorder.getAndDisplayProductInfo(event, parentLi, productCode);
                      
                        
                    }
                   
                }
                else {
                    $(event.target).removeClass(ACC.quickorder.$classHasError);
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text('');
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$productInfoContainer).remove();
                }
            
        },
         getAndDisplayProduct: function (result, parentLi, productCode,event) {
        	
			if (result['errorMsg'] != null && result['errorMsg'].length > 0) {
                $(event.target).addClass(ACC.quickorder.$classHasError);
                ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text(result['errorMsg']);
            }
            else {
                $(event.target).removeClass(ACC.quickorder.$classHasError);
                ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text('');
                
                $('#quickOrderRowTemplate').tmpl(result).insertAfter(ACC.quickorder.findElement(ACC.quickorder.$productListContainer,'.js-product-list-container'));
               var qtyInputField = ACC.quickorder.findElement(ACC.quickorder.$productListContainer, ACC.quickorder.$qtyInputField+productCode);
               qtyInputField.focusout(ACC.quickorder.handleFocusOutOnQtyInput);
                var stockLevelStatus = result.stock.stockLevelStatus.code;
           
                if (stockLevelStatus == "outOfStock") {
                    qtyInputField.val(0);
                    qtyInputField.prop('disabled', true);
                }
                else {
                	qtyInputField.focus().select();
                }
                ACC.quickorder.enableDisableAddToCartBtn();
            
       		}
        },
        bindQuickProductSearchAutocomplete: function ()
    	{
		$.widget( "custom.yautocomplete", $.ui.autocomplete, {
			_create:function(){
				
				// get instance specific options form the html data attr
				var option = this.element.data("options");
				// set the options to the widget
				this._setOptions({
					minLength: option.minCharactersBeforeRequest,
					displayProductImages: option.displayProductImages,
					delay: option.waitTimeBeforeRequest,
					autocompleteUrl: option.autocompleteUrl,
					source: this.source
				});
				
				// call the _super()
				$.ui.autocomplete.prototype._create.call(this);
				
				//Here we are setting width of drop down same as input field 
				$.ui.autocomplete.prototype._resizeMenu = function () {
					 var ul = this.menu.element;
					 ul.outerWidth(this.element.outerWidth());
					}
				
			},
			options:{
				cache:{}, // init cache per instance
				focus: function (){return false;}, // prevent textfield value replacement on item focus
				select: function (event, ui){
                   /* window.location.href = ui.item.url;*/
                }
			},
			_renderItem : function (ul, item){
				
				if ($(ul).closest('.dropdown--list-container').length == 0) {
					var wrapParent=$("<div>").addClass('dropdown--list-container');
					
										ul.addClass('dropdown--list')
												.wrap(wrapParent)
														;
									}
			
				
				if (item.type == "autoSuggestion"){
					return $("<li>")
							/*.data("item.autocomplete", item)
							.append(renderHtml)
							.appendTo(ul);*/
				}
				else if (item.type == "productResult"){

				/*	var renderHtml="<a href='" + item.url + "' class='product-item-link'>";*/
					var  renderHtml = "<div class='icon'><div class='bg-white'><img src='"+item.image+"' alt=''/>";
					renderHtml += "</div></div>";
					renderHtml += "<div class='details'><span class='product-name'>"+ item.name +"</span>";
                    renderHtml += "<span class='product-link'>Add to quick order</span></div>";
                  /*  renderHtml += 	"</a>";*/
					return $("<li class='js-quickOrderAddProduct dropdown--list--item product-item' data-code='"+item.code+"'>").data("item.autocomplete", item).append(renderHtml).appendTo(ul);
				}
			},
			source: function (request, response)
			{
				var self=this;
				var term = request.term.toLowerCase();
				if (term in self.options.cache)
				{
					return response(self.options.cache[term]);
				}

				$.getJSON(self.options.autocompleteUrl, {term: request.term}, function (data)
				{
					var autoSearchData = [];
					if(data.suggestions != null){
						$.each(data.suggestions, function (i, obj)
						{
							autoSearchData.push({
								name: obj.term,
								url: ACC.config.encodedContextPath + "/search?text=" + obj.term,
								type: "autoSuggestion"
							});
						});
					}
					if(data.products != null){
						$.each(data.products, function (i, obj)
						{
							autoSearchData.push({
								name: ACC.sanitizer.sanitize(obj.name),
								code: obj.code,
								desc: ACC.sanitizer.sanitize(obj.description),
								manufacturer: ACC.sanitizer.sanitize(obj.manufacturer),
								url:  ACC.config.encodedContextPath + obj.url,
								/*price: obj.price.formattedValue,*/
								price: (obj.price!=null && obj.price.formattedValue !=null)?obj.price.formattedValue:"",
								type: "productResult",
								image: (obj.images!=null && self.options.displayProductImages) ? obj.images[0].url : null // prevent errors if obj.images = null
							
							});
						});
					}
					self.options.cache[term] = autoSearchData;
					return response(autoSearchData);
				});
			}

		});

	
		$search = $(".js-quick-search-input");
		if($search.length>0){
		
			$search.yautocomplete();
			
			
			/*$(formEle).next(".dropdown--list--item").removeClass("ui-menu-item-wrapper");*/
		
		}
	
    	},
        handleAddToCartSuccess: function (response) {
            if ($(response.quickOrderErrorData).length > 0) {
                ACC.quickorder.disableBeforeUnloadEvent();
            }
            var lookup = {};
            response.quickOrderErrorData.forEach(function (el) {
                lookup[el.sku] = el.errorMsg;
            });

            $(ACC.quickorder.$qtyInputField).each(function () {
                var parentLi = ACC.quickorder.getCurrentParentLi(this);
                var sku = ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuInputField).val();
                var errorMsg = lookup[sku];

                if (errorMsg) {
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text(errorMsg);
                }
                else {
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$removeQuickOrderRowBtn).trigger("mousedown");
                }
            });

            ACC.quickorder.handleBeforeUnloadEvent();
            ACC.product.displayAddToCartPopup(response);
        },

        getJSONDataForAddToCart: function () {
            var skusAsJSON = [];
          
            $(ACC.quickorder.$qtyInputField).each(function () {
                var qty = Number($(this).val());
                var productCode=$(this).attr("data-product-code");
               if (qty > 0) {
                	  var sku = jQuery.trim(productCode);
                	 skusAsJSON.push({"product": {"code": sku}, "quantity": qty});
                }
               
            });
            return JSON.stringify({"cartEntries": skusAsJSON});
        },

        handleFocusOutOnSkuInput: function (event) {
        	var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
        	if (key == 13) {
        		$(event.target).focusout();
        	}
        	if (event.type == "focusout") {
        		ACC.quickorder.handleGetProduct(event);
        		ACC.quickorder.handleBeforeUnloadEvent();
        	}
        },

        handleFocusOutOnQtyInput: function (event) {
        	var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
        	if (key == 13) {	
        		event.preventDefault();
        		var parentLi = ACC.quickorder.getCurrentParentLi(event.target);        		
        		parentLi.next().find(ACC.quickorder.$skuInputField).focus();
        		$(event.target).focusout();
        	}
        	if (event.type == "focusout") {
        		ACC.quickorder.validateAndUpdateItemTotal(event);
        		ACC.quickorder.enableDisableAddToCartBtn();
        	}
        },

        clearForm: function () {
        	window.location.reload();
        },

        validateAndUpdateItemTotal: function (event) {
            var parentLi = ACC.quickorder.getQuickOrderCurrentParentLi(event.target);
            
           // var qtyValue = jQuery.trim(ACC.productorderform.filterSkuEntry($(event.target).val()));
            var qtyValue=$(event.target).val();
            var productCode=jQuery.trim($(event.target).attr("data-product-code"));
            //Checking currrent quantity 0 
            if(qtyValue == 0)
            	{
            	 
            	 $(event.target).addClass(ACC.quickorder.$classHasError);
                 var qtyValidationContainer = ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyValidationContainer+productCode);
                 
                 qtyValidationContainer.text(qtyValidationContainer.data('zeroQtyMsg'));
            	}
          //Checking validation for character.Allowing only Number.
            else if(! ACC.quickorder.isValidQtyInput(qtyValue))
        	{
            	 $(event.target).addClass(ACC.quickorder.$classHasError);
                var qtyValidationContainer = ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyValidationContainer+productCode);
                qtyValidationContainer.text(qtyValidationContainer.data('onlyNumericMsg'));
                $(event.target).val(1);
        	}
            
            else if (isNaN(qtyValue) || qtyValue == "") {
                qtyValue = 0;
                $(event.target).removeClass(ACC.quickorder.$classHasError);
                ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyValidationContainer+productCode).text('');
               $(event.target).val(0);
            }
            else {
                qtyValue = Number(qtyValue);
                $(event.target).val(qtyValue);
                var maxQty = jQuery.trim(ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyInputField+productCode).data('maxProductQty'));
                var stockLevelStatus = jQuery.trim(ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyInputField+productCode).data('stockLevelStatus'));
                maxQty = ($.isEmptyObject(maxQty) && stockLevelStatus == "inStock") ? "FORCE_IN_STOCK" : Number(maxQty);
                if (!isNaN(maxQty) && qtyValue > maxQty) {
                    $(event.target).addClass(ACC.quickorder.$classHasError);
                    var qtyValidationContainer = ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyValidationContainer+productCode);
                    
                    qtyValidationContainer.text(qtyValidationContainer.data('maxProductQtyMsg'));
                    qtyValue = maxQty;
                    $(event.target).val(maxQty);
                }
                else {
                    $(event.target).removeClass(ACC.quickorder.$classHasError);
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$qtyValidationContainer+productCode).text('');
                }
            }

            if (qtyValue > 0) {
                var itemPrice = parseFloat(ACC.quickorder.findElement(parentLi, '.js-product-price'+productCode).data('productPrice'));
                ACC.quickorder.findElement(parentLi, ACC.quickorder.$productItemTotal+productCode)
                    .html(ACC.productorderform.formatTotalsCurrency(itemPrice * qtyValue));
                
                var productformattedPrice=ACC.productorderform.formatTotalsCurrency(itemPrice* qtyValue);
                
                      
            }
            else {
            	
            	var itemPrice = parseFloat(ACC.quickorder.findElement(parentLi, '.js-product-price'+productCode).data('productPrice'));
                ACC.quickorder.findElement(parentLi, ACC.quickorder.$productItemTotal+productCode)
                    .html(ACC.productorderform.formatTotalsCurrency(itemPrice * 0));               
                var productformattedPrice=ACC.productorderform.formatTotalsCurrency(itemPrice* 1);
                //ACC.quickorder.findElement(parentLi, ACC.quickorder.$productItemTotal+productCode).text('');
            }
        },


        isValidQtyInput: function(quantityInput){
            var filteredQty = false;
            if (/^[\d]*$/.test(quantityInput)) {

            	console.log("invalid");
            	filteredQty=true;
            }
            else
            {
                filteredQty = false;
            }
            return filteredQty;
        },

        clearQuickOrderRow: function () {

        	$(this).parent().parent().remove(); 
        	$(document).on('click', '.js-remove-quick-order-row', function () { // <-- changes
        		
        		var arrelementsStockList=[];
            	var lengthOfTemplate=$(".quickOrderTemplate").length
            	//check if quickOrderTemplate  is there and remove the last row and add the thick border
        		if($(".quickOrderTemplate").length>0){       	
                	
                	$(".quickOrderTemplate").last().css({"border-bottom":"thick","border-bottom-style": "solid"});            	
                	                
        		}
        		var stockStatus=($(this).parent().parent().children()[1].childNodes[3].innerText).trim();   
        		if(stockStatus=="Out of Stock")
        			{
        					$(this).parent().parent().remove(); 				      				
        				        				
        				//iterate through the array 
        				
        				$( ".quickOrderTemplate" ).each(function( index ) {        					 
        					  productStockStatus=($(".quickOrderTemplate").children()[1].childNodes[3].innerText).trim();
        					  arrelementsStockList.push(productStockStatus);
        					  //console.log( index + ": " + ($(".quickOrderTemplate").children()[1].childNodes[3].innerText).trim());
        					});
        				
        				//console.log("productStockStatus "+arrelementsStockList.toString()); 
        				var test=$.inArray("Out of Stock", arrelementsStockList);
        				if(test==-1){
        					$('#js-add-to-cart-quick-order-btn-top').css("color","#fffff");
        				}
        				
        				//console.log("inArray "+test);
        			}
        			else{
        				   	alert($(this).parent().parent());	
        				$(this).parent().parent().remove();
        			}
        			ACC.quickorder.enableDisableAddToCartBtn();
        			ACC.quickorder.handleBeforeUnloadEvent();
        		
        	 });
        },

        isDuplicateProduct: function (productCode) {
            var exists = false;
           
            $(ACC.quickorder.$qtyInputField).each(function () {
            	var Code=jQuery.trim($(this).attr("data-product-code"));
           	
               if (Code == productCode) {
                   exists = true;
                   return false
               }
           });
            return exists;
        },

        getAndDisplayProductInfo: function (event, parentLi, productCode) {
            var url = ACC.config.encodedContextPath + '/quickOrder/productInfo?code=' + productCode;
            $.getJSON(url, function (result) {
                if (result.errorMsg != null && result.errorMsg.length > 0) {
                    $(event.target).addClass(ACC.quickorder.$classHasError);
                    ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text(result.errorMsg);
                }
                else {
                	var stockLevelStatus = result.productData.stock.stockLevelStatus.code;
                    if (stockLevelStatus == "outOfStock") {           
                        
                        //ACC.quickorder.quickOrderProductOutOfStockPopUp(result.productData);
                    	// grey out Add to Cart button and display the out of Stock Products
                    	$(event.target).removeClass(ACC.quickorder.$classHasError);
                        ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text('');
                        $('#quickOrderRowTemplate').tmpl(result.productData).insertAfter(ACC.quickorder.findElement(ACC.quickorder.$productListContainer,'.js-product-list-container'));
                      //check if quickOrderTemplate is created for last row add the thick border
                        if($(".quickOrderTemplate").length>0){           	
                        	
                        	$(".quickOrderTemplate").last().css({"border-bottom":"thick","border-bottom-style": "solid"});            	
                        	                          	
                        	}
                        var qtyInputField = ACC.quickorder.findElement(ACC.quickorder.$productListContainer, ACC.quickorder.$qtyInputField+productCode);
                        qtyInputField.focusout(ACC.quickorder.handleFocusOutOnQtyInput);
                        qtyInputField.focus().select();                              
                    	 ACC.quickorder.greyOutAddToCartButton(stockLevelStatus);
                    	
                                  
                    	}
    
                    else {
                    	  $(event.target).removeClass(ACC.quickorder.$classHasError);
                          ACC.quickorder.findElement(parentLi, ACC.quickorder.$skuValidationContainer).text('');
                          $('#quickOrderRowTemplate').tmpl(result.productData).insertAfter(ACC.quickorder.findElement(ACC.quickorder.$productListContainer,'.js-product-list-container'));
                        //check if quickOrderTemplate is created for last row add the thick border
                          if($(".quickOrderTemplate").length>0){           	
                          	
                          	$(".quickOrderTemplate").last().css({"border-bottom":"thick","border-bottom-style": "solid"});            	
                          	                          	
                          	}
                          var qtyInputField = ACC.quickorder.findElement(ACC.quickorder.$productListContainer, ACC.quickorder.$qtyInputField+productCode);
                          qtyInputField.focusout(ACC.quickorder.handleFocusOutOnQtyInput);
                    	qtyInputField.focus().select();
                    	ACC.quickorder.enableDisableAddToCartBtn();
                    }
                    ACC.quickorder.addProductListToFavorite();
                   // ACC.quickorder.enableDisableAddToCartBtn();
                }
            });
        },

        handleBeforeUnloadEvent: function () {
            if (ACC.quickorder.isAnySkuPresent()) {
                ACC.quickorder.disableBeforeUnloadEvent();
                ACC.quickorder.enableBeforeUnloadEvent();
            }
            else {
                ACC.quickorder.disableBeforeUnloadEvent();
            }
        },

        disableBeforeUnloadEvent: function () {
            $(window).off('beforeunload', ACC.quickorder.beforeUnloadHandler);
        },

        enableBeforeUnloadEvent: function () {
            $(window).on('beforeunload', ACC.quickorder.beforeUnloadHandler);
        },

        beforeUnloadHandler: function () {
            return ACC.quickorder.$quickOrderLeavePageMsg;
        },
		
        enableDisableAddToCartBtn : function() {
			var addToCartButtonEnabled = ACC.quickorder
					.shouldAddToCartBeEnabled();
			// if there are no items to add, disable addToCartBtn, otherwise,
			// enable it
			
			
			if (addToCartButtonEnabled) {
				ACC.quickorder.$addToCartBtn.removeAttr('disabled');
				ACC.quickorder.$resetFormBtn.removeAttr('disabled');
			
				$('.js-add-remove-disable').removeClass('ui-state-disabled');
				$('.js-quickOrderAddCart-no-product').addClass('hidden');
				$('.quickOrderAddCart-totalitem-and-price').css('display',
						'block');
				$('.quick-order-total-price').css('display', 'block');
				$('.quickOrderAddCart-totalitem').css('display', 'block');

			} else {
				ACC.quickorder.$addToCartBtn.attr('disabled', 'disabled');
				$('.js-quickOrderAddCart-no-product').removeClass('hidden');
				$('.quickOrderAddCart-totalitem-and-price').css('display',
						'none');
				$('.quick-order-total-price').css('display', 'none');
				$('.quickOrderAddCart-totalitem').css('display', 'none');
				$('.js-add-remove-disable').addClass('ui-state-disabled');

			}
		},
		 addProductListToFavorite: function () {
	           
	            var productList = [];
	            $(ACC.quickorder.$qtyInputField).each(function () {
	          
	                var productCode=$(this).attr("data-product-code").trim();
	               
	                if (productCode>0) {  
	                 
	                    productList.push(productCode);
	                }
	                	
	            });
	            
	            //Binding Product code  to the wishList PopUp attribute
	           $('#wishListPopup').attr('data-product-code', productList);
	            
	        },
		
        shouldAddToCartBeEnabled: function () {
            var sum = 0;
            var totalPrice=0;
            var enable = false;
            $(ACC.quickorder.$qtyInputField).each(function () {
            	
            	
                var str = this.value.trim();
                var productCode=$(this).attr("data-product-code").trim();
                var productPrice=parseFloat($(this).attr("data-price")*str); 
                var stockStatus=$(this).attr("data-stock-level-status").trim();
                //Even though stockStatus is out of stock enable add to cart button
                if (productCode>0) {
                	if(stockStatus=="outOfStock")
    				{
    					enable=true;
    					//totalPrice +=productPrice;
    				}
                	else
                	{
                	// Numbers of items which can be added to cart
                    sum += str!=0?1:0;
                    if(sum>=1)
                    {
                    	enable=true;
                    }
                    totalPrice +=productPrice;
                    
                	}
                	                 
                }
                	
            });
            $(".quickOrderAddCart-TotalItem").html(sum);
            $(".quickOrderAddCart-TotalPrice").html(ACC.productorderform.formatTotalsCurrency(totalPrice));
            
            return enable;
        },
        //grey out Add to cart button
        greyOutAddToCartButton:function(stockLevelStatus)
        {
        	if(stockLevelStatus=='outOfStock')
        		{
        			$('#js-add-to-cart-quick-order-btn-top').css("color","#808080");
        		}
        	else if(stockLevelStatus=='inStock'){
        		
        			ACC.quickorder.enableDisableAddToCartBtn();
        		}
        	
        },

        isAnySkuPresent: function () {
            var present = false;
            $(ACC.quickorder.$skuInputField).each(function () {
                var str = jQuery.trim(this.value);  // .trim() may need a shim
                if (str) {
                    present = true;
                    return false;
                }
            });
            return present;
        },

        getCurrentParentLi: function (currentElement) {
            return $(currentElement).closest(ACC.quickorder.$jsLiContainer);
        },

        getQuickOrderCurrentParentLi: function (currentElement) {
            return $(currentElement).closest(ACC.quickorder.$productListContainer);
        },

        findElement: function (currentElement, toFind) {
            return $(currentElement).find(toFind);
        },

        findElementInCurrentParentLi: function (currentElement, toFind) {
            return $(currentElement).closest(ACC.quickorder.$jsLiContainer).find(toFind);
        },
         // remove Out of stock Products for Email Opt-In 
        removeOutOfStockProd:function()
        {
        	// close the pop-up and then delete the out of stock contents from the result list 
        	$(this).colorbox.close();
        	$(".quickOrderTemplate").each(function( index ) {        		
				var productStockStatus=($(this).children()[1].childNodes[3].innerText).trim();				
				  if(productStockStatus=='Out of Stock')
					  {
					  	$(this).remove();
					  	$('#js-add-to-cart-quick-order-btn-top').css("color","#fffff");
					  }
				  
				});
        	
        },

        bindquickOrderGuidence: function(){

			$(document).on("click","#quieckOrder_procedure",function(e){
				e.preventDefault();
			var button="<div class='col-md-7 col-sm-7 col-lg-7 pull-right'><button  id='cancelButton' type='button' class='btn btn default popokbutton' style='background:#D3D3D3'>Ok</button><div>"	
			var quickbox="<div class='col-md-4 col-sm-4 col-lg-4'><div class='quickorder-pop-emptybox'></div></div>";
            var header="<span class='quickheaderpop'>QUICK ORDER HELP</span>"
			var first="<div class='row'><div class='quickorder-pop-text'><div class='col-md-4 col-sm-4 col-lg-4'><B style='font-size: 34px;'>1.</B>To build your Quick Order, start by typing in a product name, article number, or description:</div>";
			var second="<div class='col-md-4 col-sm-4 col-lg-4'><B style='font-size: 34px;'>2.</B>Add as many items as you need, one at a time. You can edit the quantity in the list:</div>";
			var third="<div class='col-md-4 col-sm-4 col-lg-4'><B style='font-size: 34px;'>3.</B>When your order is complete, click  Add to Cart to edit delivery and payment options, or Express" +
			 		" Checkout to place your order immediately with your standard settings:</div></div><div>" +
			 		quickbox+quickbox+quickbox+"</div></div>";
			 		
			 
		   var title = header+first+second+third+button;
			 $('#cboxTitle').css('padding', '17px 18px 15px');
			 	$.colorbox({
					
					inline:true,
					maxWidth:"100%",
					opacity:0.7,
					width:"950px",
					height:"450px",
					title: title,
					close:'<span class="glyphicon glyphicon-remove"></span>',
					
				});
			})
			$(document).on("click","#cancelButton", function () {
        		$.colorbox.close();
    		});  
		},
		quickOrderProductOutOfStockPopUp : function(productData){
        	
      	  var productUrl=ACC.config.encodedContextPath+"/"+productData.url;
      	  var productName="<div class='name'>"+productData.name+"</div>";
      	  var productPrice="<div class='price'>"+productData.price.formattedValue+"</div>";
      	  var outofstock="</br></br><div class='QuickOrder-OutOfStock'>OUT OF STOCK</div>";
			  var productInfo="<div class='quick-view-popup'><a href="+productUrl+"><img class='product-image' src="+productData.images[0].url+">"+(productName+productPrice)+"</a></div>";
	
			   var title =productInfo+outofstock;
				 $('#cboxTitle').css('padding', '20px 18px 15px');
				 	$.colorbox({
						
						inline:true,
						maxWidth:"100%",
						opacity:0.7,
						width:"400px",
						height:"220px",
						title: title,
						close:'<span class="glyphicon glyphicon-remove"></span>',
						
					});
				
		}
    };
}
