//ACCOUNT REGISTRATION

var regex = /^[0-9]+$/;

// remove the current email results on changing the customer number
$("#customerNumberInput").on("keypress",
		function(e) {
			$(".validation-member").hide();
			$("#email_container").empty();
			if ($('input[name=customerEmail]:checked', '#email_container')
					.val() != null) {
				retrieveCustomerEmails();
				e.preventDefault();
			}
		});


// remove error from anywhere in the form if inout is focused
$('body').find(':input').on("focus",function(){
	var form = $('#validationMember').closest('form');
	form.find('.validation').addClass('hidden');
	form.find('.input_error').addClass('hidden');
});



//remove error from anywhere in the prospect register form if input is focused
$('body').find(':input').on("focus",function(){
var form = $('#submitNewInfo').closest('form');
form.find('.validation').addClass('hidden');
});

$("#validationMember").on(
		"click",
		function(t) {
			var form = $('#validationMember').closest('form');
			form.find('.input_error').addClass('hidden');
			// condition to check if customer id is there but email is not selected - 
			if ((($("#customerNumberInput").val() != null || 
					$("#customerNumberInput").val() != "") || $("#validationMember").hasClass("getCustomerEmails"))
					&& $('input[name=customerEmail]:checked',
							'#email_container').val() == null) {
				retrieveCustomerEmails();
			}
			// when user submits the email id and customer number
			else if ($('input[name=customerEmail]:checked',
					'#email_container').val() != null
					&& ($("#customerNumberInput").val() != null
					|| $("#customerNumberInput").val() != "")) {
				registerSalonCustomer();
			}
			// condition to check if customer id is null and email ids are displayed
			else if(($("#customerNumberInput").val() == null || $("#customerNumberInput").val() == "") &&  $('#email_container').children().length <= 0){
				retrieveCustomerEmails();
			}
			t.preventDefault();
		});

// get the list of e-mails associated with the customer number 
var retrieveCustomerEmails = function() {
	var customerNumber = $("#customerNumberInput").val().trim();
	if (customerNumber != null && customerNumber != '') {
		$("#email_container").empty();
		$("#error_customerExists").addClass('hidden')
		$
				.ajax({
					url :  $("#validationMember").data('entryGetEmailAction'),
					type : "POST",

					data : {
						"customerNumber" : customerNumber,
						"CSRFToken" : ACC.config.CSRFToken
					},
					success : function(response) {
						var emails = response['emailIds'];
						var content = '<div class="scrollable">'
						if(emails.length>0){
							
						$.each(emails, function(index, value) {
							content += 
									'<label class="email_value"><input name = "customerEmail" type="radio" name="email_group" value="'
											+ value
											+ '">'
											+ value + '</label><br>';
						});
							content+='</div>'
								$("#email_container").append(content);
						$(".btn.btn-primary.getCustomerEmails").addClass(
								'salonCustomerSubmit').removeClass(
								'getCustomerEmails');
						//$("#customerNumberInput").val("");
						$(".validation-member").css('display', 'block');
						/* $("#customerNumberInput").attr('readonly',true); */
						}
						else{
							$("#no_email_message").removeClass('hidden').css({"color":"#FF0000"});;
							}

					},
					error : function(error) {
						console.log(error);
						var error_response = error.responseJSON;
						$("#error_b2bunit").removeClass('hidden').html("*" + error_response).css({"color":"#FF0000"});;
					}
				});
	} else {
			$("#error_register").removeClass('hidden').addClass('err_register');
	}
};

// post request to generate the token link for the customer
var registerSalonCustomer = function() {
	var customerNumber = $("#customerNumberInput").val().trim();
	var emailId = $('input[name=customerEmail]:checked', '#email_container')
			.val();
	if ((customerNumber != null && customerNumber != '')
			&& (emailId != null && emailId != '')) {
		$.ajax({
			url :  $("#validationMember").data('entrySubmitEmailAction'),
			type : "POST",
			data : {
				"customerNumber" : customerNumber,
				"emailId" : emailId,
				"CSRFToken" : ACC.config.CSRFToken
			},
			success : function(response) {
				console.log("success");
				$(".btn.btn-primary.salonCustomerSubmit").addClass(
						'getCustomerEmails').removeClass('salonCustomerSubmit')
				$("#customerNumberInput").attr('readonly',true);
				$("#error_customerNo").addClass('hidden');
				$("#register_message").removeClass('hidden');
				$(".validation-member").addClass('hidden');
				$("#validationMember").addClass('hidden');
			},
			error : function(error) {
				$('input[name=customerEmail]:checked').prop('checked', false);
				console.log(error);
				var error_response = error.responseJSON;
				$("#error_customerExists").removeClass('hidden').html("*"+error_response).css({"color":"#FF0000","font-size":"12px"});
				
			}
		});
	} else {
		$("#error_customerNo").addClass('hidden');
		$("#error_reg").removeClass('hidden');
	}

};


//reset modal
$("#callMeBackModal").on("hidden.bs.modal", function(){
    $(".required_field").val("");
    $(".validation").hide();
});

//customer registration form error display

	$('input.required_field').on('focus keyup focusout', function() {
		if (!$(this).val()) {
			$(this).closest('.form-input-section').find('.validation').show();
		} else {
			$(this).closest('.form-input-section').find('.validation').hide();
		}
	});
	
	
	$('textarea.required_field').on('focus keyup focusout', function() {
		if (!$(this).val()) {
			$(this).closest('.form-input-section').find('.validation').show();
		} else {
			$(this).closest('.form-input-section').find('.validation').hide();
		}
	});

	$('select.required_field').on('keyup focusout', function() {
		if (!$(this).val()) {
			$(this).closest('.form-input-section').find('.validation').show();
		} else {
			$(this).closest('.form-input-section').find('.validation').hide();
		}
	});

//customer number field
$("#customerNumberInput").on("focusout keyup", function() {
	if (!$("#customerNumberInput").val() && $('#email_container').children().length <= 0) {
		$("#error_register").removeClass('hidden').css({"display":"block","position":"absolute", "left":"50%","top":"60%"});
	} else {
		$("#error_register").css("display", "none");
	}
});
// contact form modal validation

$("#contactInfo").on("click", function(t) {
	var fullname = $("#fullName").val();
	var contact = $("#phoneNo").val();
	var time = $("#time").val();
	var reason = $("#reason").val();
    var hasError = false;
	// validate the form
	var phone_regex = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
	
	// validation for name
	if(!fullname){
		$("#fullName").closest('.form-input-section').find('.validation').show();
		hasError = true;
	} else {
		$("#fullName").closest('.form-input-section').find('.validation').hide();
	}
	
	//validation for phone number
	if (!contact.match(phone_regex)) {
		$("#phoneNo").closest('.form-input-section').find('.validation').show();
		hasError = true;
	} else {
		$("#phoneNo").closest('.form-input-section').find('.validation').hide();
	}
	
	//validation for time
	if(!time){
		$("#time").closest('.form-input-section').find('.validation').show();
		hasError = true;
	} 
	
	else {
		$("#time").closest('.form-input-section').find('.validation').hide();
	}
	
	//validation for reason
	if(!reason) {
		$("#reason").closest('.form-input-section').find('.validation').show();
		hasError = true;
	} else {
		$("#reason").closest('.form-input-section').find('.validation').hide();
	}
	return !hasError;
});


/* account submenu desktop */
var loginCanOpen = true
$('.topbar-login--account a, .topbar-login--form a')
		.on(
				'focus click',
				function() {
					$('.topbar-login--account, .topbar-login--form').addClass(
							'active');
				});
$('.topbar-login--favorites a').on('focus click', function() {
	$('.topbar-login--favorites').addClass('active');
});
$('.topbar-login--account, .topbar-login--form').on(
		'mouseover',
		function() {
			if (loginCanOpen) {
				$('.topbar-login--account, .topbar-login--form').removeClass(
						'always-active');
				$('.topbar-login--account, .topbar-login--form').addClass(
						'active');
			}
		});
$('.topbar-login--favorites').on('mouseover', function() {
	if (loginCanOpen) {
		$('.topbar-login--favorites').removeClass('always-active');
		$('.topbar-login--favorites').addClass('active');
	}
});
$('.topbar-login--account, .topbar-login--favorites').on(
		'mouseout',
		function() {
			$('.topbar-login--account, .topbar-login--favorites').removeClass(
					'active');
		});
$('.topbar-login--account').on('mouseout', function() {
	$('.topbar-login--favorites').removeClass('active');
});
$('.topbar-login--account .icon-close, .topbar-login--form .icon-close').on(
		'click',
		function() {
			$('.topbar-login--account, .topbar-login--form').removeClass(
					'active');
			loginCanOpen = false;
			window.setTimeout(function() {
				loginCanOpen = true;
			}, 300);
		});
$('.topbar-login--favorites .icon-close').on('click', function() {
	$('.topbar-login--favorites').removeClass('active');
	loginCanOpen = false;
	window.setTimeout(function() {
		loginCanOpen = true;
	}, 300);
});

var notLoggedIn = $('[data-not-logged-in]').length > 0

if (notLoggedIn) {
	$('[data-not-logged-in] a:not([data-enable-not-logged])').on(
			'click',
			function(e) {
				e.preventDefault();
				$('.topbar-login--account, .topbar-login--form').addClass(
						'always-active');
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			});
	$('[data-not-logged-in] a:not([data-enable-not-logged])').on('click',
			function(e) {
				e.preventDefault();
				$('.topbar-login--favorites').addClass('always-active');
				$("html, body").animate({
					scrollTop : 0
				}, "slow");
			})
}
