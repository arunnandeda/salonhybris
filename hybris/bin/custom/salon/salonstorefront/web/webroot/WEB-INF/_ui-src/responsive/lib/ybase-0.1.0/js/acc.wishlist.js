/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 * 
 * 
 * Add to favorite and wishList drop down 
 */
var ACC = ACC || {}; // make sure ACC is available

    ACC.wishList = {
       
    		_autoload: [
    		            "bindRemove",
    		            "bindWishListPopup",
    		            "bindAddFavoritePopUpValidation",
    		            "bindWishProductInputChange",
    		            "bindWishListDropDownOther",
    		            "bindAddtoCartClick",
    		            "bindNewWishListPopUp"
    		        ],
    		        
$qtyInputField: '.js-wishlist-product-qty',
$addToCartBtn: '#js-add-to-cart-wish-order-btn',
$productCount:'tbody #productListcount',

	bindAddtoCartClick : function() {
		$(ACC.wishList.$addToCartBtn).on("click", ACC.wishList.addToCart);
	},
	
	bindWishProductInputChange : function() {
		$(ACC.wishList.$qtyInputField).on("change",ACC.wishList.handleChangeProductQuantity);

	},
   //Add to cart on wish List page 	
	addToCart: function () {
	   	ACC.wishList.addToCartProductPopup();
	   },
 //quantity change on wishlist page product   		        
   handleChangeProductQuantity: function (event) {
	   var quantity = $.trim(event.target.value);
	   var productCode=jQuery.trim($(this).attr("data-product-code"));
	   var wishListName=$(this).attr("data-wishlist-name").replace(/\s+/, "") ;
	   $("input:hidden#quantity"+productCode+wishListName).val(quantity);
   },   		        
   

addToCartProductPopup: function ()
	   {
	   		$.ajax({
	               url: ACC.config.encodedContextPath+"/cart/addWishListOrder",
	               type: 'POST',
	               dataType: 'json',
	               contentType: 'application/json',
	               data: ACC.wishList.getJSONDataForAddToCart(),
	               async: false,
	               success: function (response) {
	        
	            	   ACC.product.displayAddToCartPopup(response);
	               },
	               error: function (jqXHR, textStatus, errorThrown) {
	                   // log the error to the console
	                   console.log("The following error occurred: " + textStatus, errorThrown);
	               }
	           });
	   	
	   	
	   },
	   getJSONDataForAddToCart: function () {
           var wishProductAsJSON = [];
         
           $(ACC.wishList.$productCount).each(function () {
        	   //putting default quantity as 1
        	   var qty=1;
               var productCode=$(this).attr("data-product-code");
               	  var code = jQuery.trim(productCode);
               	wishProductAsJSON.push({"product": {"code": code}, "quantity": qty});
              
           });
           return JSON.stringify({"cartEntries": wishProductAsJSON});
       },
       //Remove functionality for favorite and favorites page
      bindRemove:function(){

				
		$(document).on("click", ".js-removeProductFromWishlist", function(e) {
			e.preventDefault();
			productCode = $(this).attr("productCode");
			wishListPk = $(this).attr("wishListId");

			ACC.wishList.removeProductAndRedirect(productCode, wishListPk);

		});
		$(document).on("click", ".js-removeWishlist", function(e) {
			e.preventDefault();
			wishListName = $(this).attr("wishListName");

			ACC.wishList.removeWishListAndRedirect(wishListName);

		});	
		
},
	

	// Remove product from wish list.
	removeProductAndRedirect : function(productCode, wishListId) {
	
	         $.post(ACC.config.encodedContextPath+"/wishlist/removeProduct",
	            {
	               productCode: productCode,
	               wishListId: wishListId
	            }).
	            done(function(data) {
	              document.location.reload(true);
	          }).
	          fail(function(xht, textStatus, ex) {
	        	  console.log("Failed. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
	          });
    	

	    },  
	// Remove wish list.
	removeWishListAndRedirect : function(wishListName) {
    	
        $.get(ACC.config.encodedContextPath+"/wishlist/removeWishList",
           {
              wishlistName: wishListName
           }).
           done(function(data) {
        	   document.location.reload(true);
             
         }).
         fail(function(xht, textStatus, ex) {
             console.log("Failed. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
         });
	

	},
	// Get Customer wishList and display on popup
	bindWishListPopup : function() {

		$("a#wishListPopup").on("click", function(e) {
	
			e.preventDefault();
			var productCode=[];
			productCode=$(this).attr("data-product-code");
			
			$.get({
    			url:  ACC.config.encodedContextPath +"/wishlist/getWishList?productCode="+productCode
    		 })
    		 .done(function(response) {
    			$("#modal-add-favorite .modal-content").replaceWith(response);
    		    $("#modal-add-favorite").modal();
    		  })
    		 .fail(function(jqXHR, textStatus) {
    		    console.log( "Request failed: " + textStatus );
    		   })
    		 .always(function() {
    		   console.log("finished");
    			});
    			
    		
		});
		
	 $(document).on("click",".js-Favorite-submit", function(e){
    		
			e.preventDefault();
			var wishlistCallbackForm = $('#quickorderwishList');
			var wishListName=wishlistCallbackForm[0]['newWishlistName'].value;
			
			//validation for special character 
			if(wishListName!='' &&/^[a-zA-Z0-9][a-zA-Z0-9_ -]*$/.test(wishListName.trim()) == false) {
			  $('.modal-body .js-special-char-notallowd').css({'display':'block','color':'#E50000'});	
			  return;
			}
			if(ACC.wishList.wishformValidation())
			{
	    	   $(wishlistCallbackForm).ajaxSubmit({
	    		success: function(result){
	    			 $("#modal-add-favorite").modal("hide");
	    			 $("#modal-save-favorite .modal-content").replaceWith(result);
	    			 $("#modal-save-favorite").modal();
	    			 window.setTimeout(function() {
	    				    $("#modal-save-favorite").modal("hide");
	    				  }, 3000);
	    			
	    	 }
	    	});
	     }
			
    	 }).ajaxError(function(event, xhr, settings, thrownError) {
   			console.log(thrownError);
   			});

	},
	//Validation if both newwishlst and existing wishlist  empty
	wishformValidation : function() {

		if (($('#wishlistName').val()== '' )&&($('#newWishlistName').val() == '')) {
			if($('#js-existing-wishList-checkbox').is(':checked'))
			{
			 $('.modal-body .js-empty-existing-WishList').css({'display':'block','color':'#E50000'});
			 return false
			}
			else
				{
			   $('.modal-body .js-empty-WishList').css({'display':'block','color':'#E50000'});
			  return false
				}
			
		} else {
			return true;
		}
	},
	
	//Add to favourite pop up validation
	  bindAddFavoritePopUpValidation : function() {

		$(document).on('change','input[id="js-New-wishList-checkbox"]', function() {
                    
			         ACC.wishList.hideErrorMsgOnPopup();
					$("#wishlistName option:first").attr('selected','selected');
					$('#js-existing-wishList-checkbox').prop("checked", false);
					$('#wishlistName').prop("disabled", true);
					$('#newWishlistName').prop("disabled", false);
					if ($("#newWishlistName").val() != '') {
						$('.js-Favorite-submit').removeAttr('disabled');
					 }
					/*else
					{
						$('.js-Favorite-submit').attr('disabled', true); ;	
					}*/

				});

		$(document).on('change', '#newWishlistName', function() {
			ACC.wishList.hideErrorMsgOnPopup();
			$("#wishlistName option:first").attr('selected','selected');
			if ($("#newWishlistName").val() != '') {
				
				$('#js-existing-wishList-checkbox').prop("checked", false);
				$('#js-New-wishList-checkbox').prop("checked", true);
				$('.js-Favorite-submit').removeAttr('disabled');
			} /*else {
              
				$('.js-Favorite-submit').attr('disabled', true);
			}*/
		});

		$(document).on('change', 'input[id="js-existing-wishList-checkbox"]',
				function() {
				   ACC.wishList.hideErrorMsgOnPopup();
			        $("#newWishlistName").val('');
					$('#js-New-wishList-checkbox').prop("checked", false);
					$('#newWishlistName').prop("disabled", true);
					$('#wishlistName').prop("disabled", false);
					
					if($("#wishlistName option:selected").val()!='')
						{
						$('.js-Favorite-submit').removeAttr('disabled');
						}
					/*else
						{
						$('.js-Favorite-submit').attr('disabled', true);
						}*/

				});

		$(document).on('change', 'select[id="wishlistName"]', function() {

			var wishListName = $("#wishlistName option:selected").val();
			if (wishListName != '') {
				$('#newWishlistName').prop("disabled", true);
				$('#js-existing-wishList-checkbox').prop('checked', true);
				$('.js-Favorite-submit').removeAttr('disabled');
				$('.wishListinvalid').css('display', 'none');
			} /*else {
				$('.js-Favorite-submit').attr('disabled', true);
			}*/

		});
	
	},
	hideErrorMsgOnPopup: function ()
    {
		 //hide error message 
		$(".modal-body .js-special-char-notallowd").hide();
		$(".modal-body .js-empty-WishList").hide();
		$(".modal-body .js-empty-existing-WishList").hide();
    },
    
    bindNewWishListPopUp:function() {
    	
    	$('#js-newwishList-btn').click(function(event) {
    		event.preventDefault();
    
    		var wishListName=$(".js-inputfield").val();
    		if(/^[a-zA-Z0-9][a-zA-Z0-9_ -]*$/.test(wishListName.trim()) == false) {
    			  $('.modal-body .js-special-char-notallowd').css({'display':'block','color':'#E50000'});	
    			  return;
    		 }
    		if(wishListName.trim()=='')
    		{
    			 $('.modal-body .js-empty-WishList').css({'display':'block','color':'#E50000'});
    			 return;
    		}
    	
    		else
    		{
    			$('#newwishistForm').submit();
    		}
    	});
    	$('.js-inputfield').on('mouseout', function() {
    		$('.modal-body .js-special-char-notallowd').hide();
    		$('.modal-body .js-empty-WishList').hide();
		});
    	
    		

	    },

	bindWishListDropDownOther : function() {

		$('#save-btn').click(function(event) {
			event.preventDefault();
			$('#editFrom').submit();
		});

		$('.topbar-login--favorites a').on('focus click', function() {
			$('.topbar-login--favorites').addClass('active');
		});
		$('.topbar-login--favorites').on('mouseover', function() {
			if (true) {
				$('.topbar-login--favorites').removeClass('always-active');
				$('.topbar-login--favorites').addClass('active');
			}
		});
		$('.topbar-login--favorites').on('mouseout', function() {
			$('.topbar-login--favorites').removeClass('active');
		});
		$('.topbar-login--account').on('mouseout', function() {
			$('.topbar-login--favorites').removeClass('active');
		});
		$('.topbar-login--form .icon-close').on('click', function() {
			$('.topbar-login--form').removeClass('active');
			window.setTimeout(function() {
			}, 300);
		});

		$('.topbar-login--favorites .icon-close').on('click', function() {
			$('.topbar-login--favorites').removeClass('active');
			window.setTimeout(function() {
			}, 300);
		});

		$("#edit-name").on("click", function() {
			if ($("#input").attr("disabled")) {
				$("#input").removeAttr("disabled");
				$("#input").focus();
				$(".actions").css("display", "block");
				$("#edit-name").addClass("hidden");
			} else {
				$("#input").attr("disabled", "disabled");
				$(".actions").css("display", "none");
				$("#edit-name").removeClass("hidden");
			}
		});

		$("#cancel-btn, #save-btn").on("click", function() {
			$("#input").attr("disabled", "disabled");
			$(".actions").hide();
			$("#edit-name").removeClass("hidden");
		});
		$("#modal-add-fav").on("click", function() {
			$("#modal-add-favorite").modal("hide");
			$("#modal-save-favorite").modal("show");
		});

		$("#modal-add-fav-cancel").on("click", function() {
			$("#modal-add-favorite").modal("hide");
		});

	},
    
};
