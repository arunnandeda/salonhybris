/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
var ACC = ACC || {};
ACC.Registration = {

	_autoload : ["hidePasswordError","formInputValidation", "prospectCustomerFormValidation",
			"newRegisterFormValidation" ],

			hidePasswordError : function(){
				$(document).ready(function(){
				$('#pwd_minchar').addClass('hidden');
				});
			},
			
	formInputValidation : function() {
		$(document).on("click", "#registerFormSubmit", function(e) {
			var name = $('#name').val();
			var pwd = $('#pwd').val();
			var checkPwd = $('#checkPwd').val();
			var hasError=false;
			if (name == '' || name == null) {
					$('#name').closest('.form-input-section').find('.validation').show();
					hasError= true;
				} else {
					$('#name').closest('.form-input-section').find('.validation').hide();
				}
			if (pwd.length < 6 ) {
					$('#pwd').closest('.form-input-section').find('.validation').show();
					hasError= true;
				} else {
					$('#pwd').closest('.form-input-section').find('.validation').hide();
				}
			if (pwd !== checkPwd) {
				$('#checkPwd').closest('.form-input-section').find('.validation').show();
				hasError= true;
			} else {
				$('#checkPwd').closest('.form-input-section').find('.validation').hide();
			}
			return !hasError;	
		});

	},

	prospectCustomerFormValidation : function() {
		$(document).on("click", "#submitInfo", function(e) {
			var name = $('#name').val();
			var email = $('#emailId').val();
			var salonName = $('#salonName').val();
			var phone = $('#phone').val();
			var noOfEmp = $('#noOfEmp').val();
			var salonSize = $('#salonSize').val();
			var position = $('#position').val();
			var vat = $('#vatNumber').val();
			var street = $('#street').val();
			var city = $('#city').val();
			var region = $('#region').val();
			var zip = $('#postalcode').val();
			var country = $('#country').val();
			var regex=new RegExp('^[0-9]+$');
			var hasErrors=false;
			//validate salon name
			if (salonName == '' || salonName == null) {
				$('#salonName').closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$('#salonName').closest('.form-input-section').find('.validation').hide();
			}
			//validate vat number
			if (vat == '' || vat == null) {
				$("#vatNumber").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#vatNumber").closest('.form-input-section').find('.validation').hide();
			}
			//validate address
			if (street == '' || street == null) {
				$("#street").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#street").closest('.form-input-section').find('.validation').hide();
			}
			//validate city
			if (city == '' || city == null) {
				$("#city").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#city").closest('.form-input-section').find('.validation').hide();
			}
			//validate region/district
			if (region == '' || region == null) {
				$("#region").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#region").closest('.form-input-section').find('.validation').hide();
			}
			//validate zip
			if (zip == '' || zip == null) {
				$("#postalcode").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#postalcode").closest('.form-input-section').find('.validation').hide();
			}
			//validate country
			if (country == 0)  {
				$("#country").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			}else {
				$("#country").closest('.form-input-section').find('.validation-regex').hide();
			}
			//validate vat number value via regular expression
			/*if (!$("#vatNumber").val().match(regex)) {
				$("#vatNumber").closest('.form-input-section').find('.validation-regex').show();
				return false;
			} else {
				$("#vatNumber").closest('.form-input-section').find('.validation-regex').hide();
			}*/
			// terms and condition must be checked
			if (!$("#checkterms").is(":checked")) {
				$("#checkterms").closest('.form-input-section').find('.validation').show();
				hasErrors = true;
			} else {
				$("#checkterms").closest('.form-input-section').find('.validation').hide();
			}
			return !hasErrors;
		});
	},
	newRegisterFormValidation : function() {
		$(document)
				.on(
						"click",
						"#submitNewInfo",
						function(e) {
							var form = $('#submitNewInfo').closest('form');
							form.find('.input_error').addClass('hidden');
							var name = $('#name').val();
							var email =$('#email').val();
							var emailpattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
							if (!name) {
								//,"position":"absolute", "left":"60%","top":"48%"
								$("#error_newregister").removeClass('hidden').css({"display":"block"});
								return false;	
							} else {
								$("#error_newregister").css("display", "none");
							}
							if (!email || !emailpattern.test(email)) {
								//,"position":"absolute", "left":"60%","top":"60%"
								$("#email_err").removeClass('hidden').css({"display":"block"});
								return false;
							} else {
								$("#email_err").css("display", "none");
							}
						});
	}

};
