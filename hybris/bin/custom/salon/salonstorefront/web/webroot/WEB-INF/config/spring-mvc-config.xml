<?xml version="1.0" encoding="UTF-8"?>
<!--
 [y] hybris Platform

 Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.

 This software is the confidential and proprietary information of SAP
 ("Confidential Information"). You shall not disclose such Confidential
 Information and shall use it only in accordance with the terms of the
 license agreement you entered into with SAP.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:mvc="http://www.springframework.org/schema/mvc"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:util="http://www.springframework.org/schema/util"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/context
		http://www.springframework.org/schema/context/spring-context.xsd
		http://www.springframework.org/schema/mvc
		http://www.springframework.org/schema/mvc/spring-mvc.xsd
		http://www.springframework.org/schema/util
		http://www.springframework.org/schema/util/spring-util.xsd
		http://www.springframework.org/schema/aop
		http://www.springframework.org/schema/aop/spring-aop.xsd">

	<!--
		Section for Traditional Spring MVC.
		Annotation driven controllers, and a list of packages to scan. Can be extended by adding (or removing) packages.
	-->
    <context:annotation-config/>
    <bean id="webLazyInitOverrideBeanFactoryPostProcessor" parent="lazyInitOverrideBeanFactoryPostProcessor" />

	<!-- activates annotation driven binding -->
	<mvc:annotation-driven ignore-default-model-on-redirect="true" validator="validator">
		<mvc:message-converters>
			<bean class="org.springframework.http.converter.ResourceHttpMessageConverter"/>
			<bean class="org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter"/>
			<bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter" />
		</mvc:message-converters>
	</mvc:annotation-driven>

	<!-- Scan for annotation configured controller -->
	<context:component-scan base-package="com.salon.storefront" scope-resolver="de.hybris.platform.spring.IgnoreTenantScopeMetadataResolver"  />

    

	<!-- Data Value Processor -->
	<bean name="defaultRequestDataValueProcessor" class="com.salon.storefront.util.DefaultRequestDataProcessor" />

	<!--This is a bean post-processor for RequestMappingHandlerAdapter-->
	<bean class="com.salon.storefront.web.mvc.RequestMappingHandlerAdapterConfigurer" init-method="init">
		<property name="urlPathHelper">
			<bean class="com.salon.storefront.web.mvc.AcceleratorUrlPathHelper"/>
		</property>
	</bean>

	<alias name="defaultRequireHardLoginEvaluator" alias="requireHardLoginEvaluator" />
	<bean id="defaultRequireHardLoginEvaluator" class="com.salon.storefront.security.evaluator.impl.RequireHardLoginEvaluator" >
		<property name="userService" ref="userService"/>
		<property name="cookieGenerator" ref="guidCookieGenerator"/>
		<property name="sessionService" ref="sessionService"/>
		<property name="cartService" ref="cartService"/>
	</bean>

	<!--  MVC Interceptors -->
	<alias name="defaultBeforeControllerHandlersList" alias="beforeControllerHandlersList" />
	<util:list id="defaultBeforeControllerHandlersList" >
		<!-- List of handlers to run -->
		<bean class="com.salon.storefront.interceptors.beforecontroller.SecurityUserCheckBeforeControllerHandler" />
		<bean class="com.salon.storefront.interceptors.beforecontroller.RequireHardLoginBeforeControllerHandler" >
				<property name="redirectStrategy" ref="redirectStrategy"/>
				<property name="loginUrl" value="/login"/>
				<property name="loginAndCheckoutUrl" value="/login/checkout"/>
				<property name="requireHardLoginEvaluator" ref="requireHardLoginEvaluator"/>
		</bean>
		<bean class="com.salon.storefront.interceptors.beforecontroller.DeviceDetectionBeforeControllerHandler" />
		<bean class="com.salon.storefront.interceptors.beforecontroller.SetLanguageBeforeControllerHandler" />
		<bean class="com.salon.storefront.interceptors.beforecontroller.SetUiExperienceBeforeControllerHandler" />
		<bean class="com.salon.storefront.interceptors.beforecontroller.ThemeBeforeControllerHandler" />
	</util:list>
	
	<alias name="defaultBeforeViewHandlersList" alias="beforeViewHandlersList" />
	<util:list id="defaultBeforeViewHandlersList"  >
	<!-- The CmsPageBeforeViewHandler could change the target view, so it should be run first. -->
		<bean class="com.salon.storefront.interceptors.beforeview.CmsPageBeforeViewHandler" />
		<bean class="com.salon.storefront.interceptors.beforeview.UiThemeResourceBeforeViewHandler" >
			<property name="defaultThemeName" value="blue"/>
		</bean>
		<bean class="com.salon.storefront.interceptors.beforeview.SeoRobotsFollowBeforeViewHandler" >
			<property name="robotIndexForJSONMapping">
				<map>
					<entry key="/search" value="noindex,follow" />
				</map>
			</property>
		</bean>
		<bean class="com.salon.storefront.interceptors.beforeview.UiExperienceMetadataViewHandler"  />
		<bean class="com.salon.storefront.interceptors.beforeview.AnalyticsPropertiesBeforeViewHandler" />
		<bean class="com.salon.storefront.interceptors.beforeview.ConfigWro4jBeforeViewHandler" >
			<property name="siteConfigService" ref="siteConfigService"/>
		</bean>
		<bean class="com.salon.storefront.interceptors.beforeview.DebugInfoBeforeViewHandler" />
		<bean class="com.salon.storefront.interceptors.beforeview.CartRestorationBeforeViewHandler">
			<property name="sessionService" ref="sessionService" />
			<property name="pagesToShowModifications">
				<list>
					<value>/cart</value>
				</list>
			</property>
		</bean>
		<bean class="com.salon.storefront.interceptors.beforeview.GoogleMapsBeforeViewHandler" />
	</util:list>
	
	<!-- Interceptor that runs once per request and before the controller handler method is called -->
	<alias name="defaultBeforeControllerHandlerInterceptor" alias="beforeControllerHandlerInterceptor" />
	<bean id="defaultBeforeControllerHandlerInterceptor" class="com.salon.storefront.interceptors.BeforeControllerHandlerInterceptor" >
		<property name="beforeControllerHandlers">
			<ref bean="beforeControllerHandlersList" />	
		</property>
	</bean>

	<!-- Interceptor that runs BeforeViewHandlers before the view is rendered -->
	<alias name="defaultBeforeViewHandlerInterceptor" alias="beforeViewHandlerInterceptor" />
	<bean id="defaultBeforeViewHandlerInterceptor" class="com.salon.storefront.interceptors.BeforeViewHandlerInterceptor" >
		<property name="beforeViewHandlers">
			<ref bean="beforeViewHandlersList" />	
		</property>
	</bean>

	<mvc:interceptors>
		<ref bean="beforeControllerHandlerInterceptor" />
		<ref bean="beforeViewHandlerInterceptor" />

		<!--
		<bean class="org.springframework.web.servlet.handler.ConversionServiceExposingInterceptor" >
			<constructor-arg ref="conversionService"/>
		</bean>
		-->
	</mvc:interceptors>

	<!-- Tell Spring MVC how to find its jsp files -->
	<bean id="viewResolver" class="com.salon.storefront.web.view.UiExperienceViewResolver" >
		<property name="viewClass" value="org.springframework.web.servlet.view.JstlView"/>
		<property name="prefix" value="/WEB-INF/views/"/>
		<property name="addOnPrefix" value="addons/"/>
		<property name="suffix" value=".jsp"/>
		<property name="redirectHttp10Compatible" value="true"/>
		<property name="cache" value="true"/>

		<property name="uiExperienceService" ref="uiExperienceService"/>
		<property name="uiExperienceViewPrefix">
			<map>
				<entry key="DESKTOP" value="${commerceservices.default.desktop.ui.experience:desktop}/"/>
				<entry key="MOBILE" value="mobile/"/>
			</map>
		</property>
		<property name="unknownUiExperiencePrefix" value="${commerceservices.default.desktop.ui.experience:desktop}/"/>
	</bean>

	<!-- Localization -->
	<bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean" >
		<property name="validationMessageSource" ref="storefrontMessageSource"/>
	</bean>

	<bean id="baseMessageSource" class="org.springframework.context.support.ReloadableResourceBundleMessageSource" >
		<property name="basenames">
			<list>
				<value>/WEB-INF/messages/base</value>
			</list>
		</property>
		<property name="defaultEncoding" value="UTF-8"/>
		<!-- The number of seconds to cache loaded properties files. Set to -1 (never) for production. -->
		<property name="cacheSeconds" value="#{configurationService.configuration.getProperty('storefront.resourceBundle.cacheSeconds')}"/>
		<property name="fallbackToSystemLocale" value="false"/>
	</bean>


	<!-- theme message source - falls back to the messageSource -->
	<alias name="storefrontMessageSource" alias="themeSource" />
	<alias name="storefrontMessageSource" alias="messageSource" />
	<bean id="storefrontMessageSource" class="com.salon.storefront.web.theme.StorefrontResourceBundleSource">
		<property name="basenamePrefix" value="/WEB-INF/messages/" />
		<property name="basePrefix" value="base" />
		<property name="sitePrefix" value="site" />
		<property name="themePrefix" value="theme" />
		<property name="parentMessageSource" ref="baseMessageSource" />
		<property name="requiredAddOnsNameProvider" ref="reqAddOnsNameProvider" />
		<!-- The number of seconds to cache loaded properties files. Set to -1 (never) for production. -->
		<property name="cacheSeconds" value="#{configurationService.configuration.getProperty('storefront.resourceBundle.cacheSeconds')}"/>
		<property name="defaultEncoding" value="UTF-8"/>
		<property name="fallbackToSystemLocale" value="false" />
		<property name="resourceLoader">
			<bean class="org.springframework.core.io.FileSystemResourceLoader"/>
		</property>
		<property name="siteThemeResolverUtils" ref="siteThemeResolverUtils"/>
	</bean>

	<!-- Theme Resolver Will resolve Theme from current page and then current cms site -->
	<alias name="uiExperienceSiteThemeResolver" alias="themeResolver"/>
	<bean id="uiExperienceSiteThemeResolver" class="com.salon.storefront.web.theme.UiExperienceSiteThemeResolver" >
		<property name="siteThemeResolverUtils" ref="siteThemeResolverUtils"/>
	</bean>
	
	<bean id="localeResolver" class="com.salon.storefront.web.i18n.StoreSessionLocaleResolver" >
		<property name="i18NService" ref="i18NService"/>
	</bean>

	<!-- CMS Content Element Controllers -->

	<bean id="browseHistory" class="de.hybris.platform.acceleratorstorefrontcommons.history.impl.DefaultBrowseHistory" >
		<property name="cmsSiteService" ref="cmsSiteService" />
		<property name="sessionService" ref="sessionService" />
		<property name="capacity" value="10" />
	</bean>

	<!-- Page Breadcrumb Builders -->

	<bean id="productBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder" >
		<property name="browseHistory" ref="browseHistory" />
		<property name="productModelUrlResolver" ref="productModelUrlResolver"/>
		<property name="categoryModelUrlResolver" ref="categoryModelUrlResolver"/>
		<property name="productService" ref="productService"/>
	</bean>

	<bean id="searchBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder" >
		<property name="commerceCategoryService" ref="commerceCategoryService"/>
		<property name="categoryModelUrlResolver" ref="categoryModelUrlResolver"/>
	</bean>

	<bean id="contentPageBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder" />

	<bean id="storefinderBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.StorefinderBreadcrumbBuilder" >
		<property name="messageSource" ref="storefrontMessageSource" />
		<property name="i18nService" ref="i18nService" />
	</bean>

	<bean id="simpleBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.DefaultResourceBreadcrumbBuilder" >
		<property name="i18nService" ref="i18nService" />
	</bean>
	
	<bean id="storeBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.StoreBreadcrumbBuilder" >
		<property name="messageSource" ref="storefrontMessageSource" />
		<property name="i18nService" ref="i18nService" />
	</bean>

	<bean id="accountBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.DefaultResourceBreadcrumbBuilder" >
		<property name="i18nService" ref="i18nService" />
		<property name="parentBreadcrumbLinkPath" value="/my-account"/>
		<property name="parentBreadcrumbResourceKey" value="#{'responsive' == '${commerceservices.default.desktop.ui.experience}' ? '' : 'header.link.account'}"/>
	</bean>

	<bean id="multiStepCheckoutBreadcrumbBuilder" class="de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.DefaultResourceBreadcrumbBuilder" >
		<property name="i18nService" ref="i18nService" />
		<property name="parentBreadcrumbResourceKey" value="checkout.multi.breadcrumb"/>
	</bean>

	<!-- -->
	<alias name="defaultPreviewUrlResolverPageMappings" alias="previewUrlResolverPageMappings" />
	<util:map id="defaultPreviewUrlResolverPageMappings">
		<entry key="homepage" value="/"/>
		<entry key="cartPage" value="/cart"/>
		<entry key="search" value="/search"/>
		<entry key="searchEmpty" value="/search"/>
		<entry key="account" value="/my-account"/>
		<entry key="profile" value="/my-account/profile"/>
		<entry key="address-book" value="/my-account/address-book"/>
		<entry key="add-edit-address" value="/my-account/add-edit-address"/>
		<entry key="payment-details" value="/my-account/payment-details"/>
		<entry key="order" value="/my-account/order"/>
		<entry key="orders" value="/my-account/orders"/>
		<entry key="multiStepCheckoutSummaryPage" value="/checkout/multi/add-delivery-address"/>
		<entry key="importCSVSavedCartPage" value="/import/csv/saved-cart"/>
		<entry key="saved-carts" value="/my-account/saved-carts"/>
		<entry key="savedCartDetailsPage" value="/my-account/saved-carts"/>
		<entry key="storefinderPage" value="/store-finder"/>
		<entry key="login" value="/login"/>
		<entry key="checkout-login" value="/login/checkout"/>
		<entry key="forgottenPassword" value="/login/pw/request"/>
		<entry key="updatePassword" value="/my-account/update-password"/>
		<entry key="notFound" value="/error"/>

		<!--Configuration WCMS Mobile Page to URL -->
		<entry key="mobile-homepage" value="/"/>
		<entry key="mobile-cartPage" value="/cart"/>
		<entry key="mobile-search" value="/search"/>
		<entry key="mobile-searchEmpty" value="/search"/>
		<entry key="mobile-account" value="/my-account"/>
		<entry key="mobile-profile" value="/my-account/profile"/>
		<entry key="mobile-address-book" value="/my-account/address-book"/>
		<entry key="mobile-add-edit-address" value="/my-account/add-edit-address"/>
		<entry key="mobile-payment-details" value="/my-account/payment-details"/>
		<entry key="mobile-order" value="/my-account/order"/>
		<entry key="mobile-orders" value="/my-account/orders"/>
		<entry key="mobile-multiStepCheckoutSummaryPage" value="/checkout/multi/add-delivery-address"/>
		<entry key="mobile-storefinderPage" value="/store-finder"/>
		<entry key="mobile-login" value="/login"/>
		<entry key="mobile-checkout-login" value="/login/checkout"/>
		<entry key="mobile-forgottenPassword" value="/login/pw/request"/>
		<entry key="mobile-updatePassword" value="/my-account/update-password"/>
		<entry key="mobile-register" value="/register"/>
		<entry key="mobile-orderConfirmationPage" value="/my-account/order"/>
		<entry key="mobile-notFound" value="/error"/>
	</util:map>

	<alias name="defaultPreviewDataModelUrlResolver" alias="previewDataModelUrlResolver" />
	<bean id="defaultPreviewDataModelUrlResolver" class="com.salon.storefront.url.DefaultPreviewDataModelUrlResolver" >
		<property name="productModelUrlResolver" ref="productModelUrlResolver"/>
		<property name="categoryModelUrlResolver" ref="categoryModelUrlResolver"/>
		<property name="pageMapping">
			<ref bean="previewUrlResolverPageMappings" />
		</property>
	</bean>

	<bean id="pageTitleResolver" class="de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver" >
		<property name="cmsSiteService" ref="cmsSiteService"/>
		<property name="commerceCategoryService" ref="commerceCategoryService"/>
		<property name="productService" ref="productService"/>
	</bean>

	<alias name="defaultVariantSortStrategy" alias="variantSortStrategy" />
	<bean id="defaultVariantSortStrategy" class="de.hybris.platform.acceleratorstorefrontcommons.variants.impl.DefaultVariantSortStrategy" >
		<property name="sortingFieldsOrder">
			<list>
				<value>size</value>
			</list>
		</property>
		<property name="comparators">
			<util:map>
				<entry key="size" value-ref="sizeAttributeComparator"/>
			</util:map>
		</property>
		<property name="defaultComparator" ref="variantsComparator"/>
	</bean>

	<alias name="defaultVariantsComparator" alias="variantsComparator"/>
	<bean id="defaultVariantsComparator"
		  class="de.hybris.platform.acceleratorstorefrontcommons.variants.impl.DefaultVariantComparator" />

	<bean id="fixation" class="de.hybris.platform.servicelayer.security.spring.HybrisSessionFixationProtectionStrategy"/>
	
	<alias name="defaultReCaptchaAspect" alias="reCaptchaAspect"/>
	<bean name="defaultReCaptchaAspect" class="com.salon.storefront.security.captcha.ReCaptchaAspect">
		<property name="siteConfigService" ref="siteConfigService"/>
		<property name="baseStoreService" ref="baseStoreService"/>
	</bean>

	<!-- redefined here to make sure the bean is processed after all the addon's beans are loaded -->
	<bean id="listMergeBeanPostProcessor"
	      class="de.hybris.platform.commerceservices.spring.config.ListMergeDirectiveBeanPostProcessor"/>
	<bean id="mapMergeBeanPostProcessor"
	      class="de.hybris.platform.commerceservices.spring.config.MapMergeDirectiveBeanPostProcessor"/>

	<bean id="siteThemeResolverUtils" class="com.salon.storefront.util.SiteThemeResolverUtils">
		<property name="uiExperienceService" ref="uiExperienceService"/>
		<property name="cmsSiteService" ref="cmsSiteService"/>
		<property name="defaultTheme" value="blue"/>
	</bean>
	
	
	<!-- secure portal start -->
	      <aop:config proxy-target-class="true" expose-proxy="true">
	<aop:pointcut expression="execution(* com.salon.storefront.secureportaladdon..controllers..*.showRegistrationPage(..))" id="showRegistrationPagePointcut"/>
	<aop:pointcut expression="execution(* com.salon.storefront.secureportaladdon..controllers..*.submitRegistration(..))" id="submitRegistrationPointcut"/>
	<aop:aspect id="prepB2bRegistrationReCaptchaAspect" ref="reCaptchaAspect">
		<aop:around method="prepare" pointcut-ref="showRegistrationPagePointcut"/>
	</aop:aspect>
	<aop:aspect id="b2bRegistrationReCaptchaAspect" ref="reCaptchaAspect">
		<aop:around method="advise" pointcut-ref="submitRegistrationPointcut"/>
	</aop:aspect>
</aop:config>
     <bean class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject" ref="loginSuccessForceDefaultTarget"/>
		<property name="targetMethod" value="addAll"/>
		<property name="arguments">
			<list>
    			<value>login/pw/change</value>
    		</list>
		</property>
	</bean>
  
    <alias alias="userService" name="defaultSecureUserServicce"/>
    <bean id="defaultSecureUserServicce" class="com.salon.storefront.secureportaladdon.services.impl.SecureUserService" parent="defaultUserService">
        <property name="cmsSiteService" ref="cmsSiteService" />
    </bean>
	<!-- Before controller responsible for intercepting request to the storefront and apply the new security rule when a site is secured -->
	<bean id="securePortalBeforeControllerHandler" class="com.salon.storefront.secureportaladdon.interceptors.SecurePortalBeforeControllerHandler">
		<property name="unsecuredUris" ref="unsecuredUris" />
		<property name="controlUris" ref="controlUris" />
		<property name="cmsSiteService" ref="cmsSiteService" />
		<property name="userService" ref="userService" />
		<property name="defaultLoginUri" value="${default.login.uri}" />
		<property name="checkoutLoginUri" value="${checkout.login.uri}" />
		<property name="defaultRegisterUri" value="/register" />
		<property name="siteBaseUrlResolutionService" ref="siteBaseUrlResolutionService" />
		<property name="redirectStrategy" ref="redirectStrategy" />
		<property name="requestProcessor" ref="asmRequestProcessor"/>
	</bean>
	
	<bean id="asmRequestProcessor" class="com.salon.storefront.secureportaladdon.interceptors.AsmRequestProcessor">
	    <property name="sessionService" ref="sessionService"/>
		<property name="addOnValueProviderRegistry" ref="addOnValueProviderRegistry"/>
		<property name="assistedServiceAddOnName" value="assistedservicestorefront"/>
		<property name="asmRequestParameter" value="asm"/>
		<property name="quitAsmRequestUri" value="/assisted-service/quit"/>
		<property name="agentLoggedInKey" value="agentLoggedIn"/>
    </bean>

	<!-- Add our new before controller handler to the existing list using a merge -->
	<bean depends-on="beforeControllerHandlersList" parent="listMergeDirective">
		<property name="add" ref="securePortalBeforeControllerHandler" />
	</bean>

	<!-- Before view responsible for intercepting request to the storefront and apply the new security rule when a site is secured -->
	<bean id="secureCustomerPortalBeforeViewHandler" class="com.salon.storefront.secureportaladdon.interceptors.SecurePortalBeforeViewHandler">
		<property name="cmsSiteService" ref="cmsSiteService" />
		<property name="cmsPageService" ref="cmsPageService" />
		<property name="pageTitleResolver" ref="pageTitleResolver" />
		<property name="spViewMap" ref="spViewMap" />
	</bean>

	<!-- Add our new before view handler to the existing list using a merge -->
	<bean depends-on="beforeViewHandlersList" parent="listMergeDirective">
		<property name="add" ref="secureCustomerPortalBeforeViewHandler" />
	</bean>

	<!--SP Mappings: Map that contains replacement info used in SecureCustomerPortalBeforeViewHandler.java to update the model with SCP 
		components as well replace the views -->
	<util:map id="spViewMap" key-type="java.lang.String" value-type="java.lang.Map">
		<entry key="pages/account/accountLoginPage" value-ref="loginPageMap" />
		<entry key="pages/password/passwordResetChangePage" value-ref="resetPasswordPageMap" />
		<entry key="addon:/secureportaladdon/pages/accountRegistration" value-ref="registerPageMap" />
	</util:map>


	<!-- Each page that needs replacement has its own map below -->
	<util:map id="loginPageMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="viewName" value="/pages/secureportal/login" />
		<entry key="cmsPageId" value="SecureCustomerPortalSecuredLoginPage" />
	</util:map>

	<util:map id="resetPasswordPageMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="viewName" value="/pages/resetPassword" />
		<entry key="cmsPageId" value="SecureCustomerPortalSecuredResetPasswordPage" />
	</util:map>
	
	<util:map id="registerPageMap" key-type="java.lang.String" value-type="java.lang.String">
		<entry key="viewName" value="/pages/registrationPage" />
		<entry key="cmsPageId" value="registration" />
	</util:map>

	<!-- List of unsecured mappings, i.e. those that the SecurePortalBeforeControllerHandler instance will let through regardless 
		of authentication -->
	<util:set id="unsecuredUris" value-type="java.lang.String">
		<value>/_s/**</value>
		<value>/punchout/**</value>
		<value>/assisted-service/emulate</value>
		<value>/cart/rollover/MiniCart</value>
	</util:set>

	<!-- List of control mappings, i.e. those that do not launch a new page but would only change a value on the server side such as language. -->
	<util:set id="controlUris" value-type="java.lang.String">
		<value>/_s/**</value>
		<value>/punchout/**</value>
		<value>/cart/rollover/MiniCart</value>
		<value>/assisted-service/emulate</value>
		<value>/register/captcha/widget/recaptcha</value>
	</util:set>

	<!-- Take the existing exclusions and add this addons' unsecured mappings to it -->
	<bean class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject" ref="excludeUrlSet" />
		<property name="targetMethod" value="addAll" />
		<property name="arguments" ref="unsecuredUris" />
	</bean>
	<!-- AOP for captcha  -->
	<aop:config proxy-target-class="true" expose-proxy="true">
		<aop:pointcut expression="execution(* com.salon.storefront.secureportaladdon..controllers..*.showRegistrationPage(..))" id="showRegistrationPagePointcut"/>
		<aop:pointcut expression="execution(* com.salon.storefront.secureportaladdon..controllers..*.submitRegistration(..))" id="submitRegistrationPointcut"/>		
	</aop:config>
	<!-- Hack the list of allowed workflow attachment data in the B2B Admin Cockpit -->
	<bean class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="targetObject" ref="WorkflowAttachmentData" />
		<property name="targetMethod" value="addAll" />
		<property name="arguments" ref="b2bRegistrationWorkflowAttachmentData" />
	</bean>		
		
	<bean id="b2bRegistrationWorkflowAttachmentData" class="java.util.LinkedList">
		<constructor-arg>
			<list value-type="java.lang.String">
				<value>com.salon.core.model.B2BRegistrationModel</value>
			</list>
		</constructor-arg>
	</bean>
	
	<bean id="WorkflowAttachmentData" class="java.util.LinkedList">
		<constructor-arg>
			<list value-type="java.lang.String">
				<value>de.hybris.platform.core.model.product.ProductModel</value>
				<value>de.hybris.platform.category.model.CategoryModel</value>
				<value>de.hybris.platform.core.model.order.OrderModel</value>
			</list>
		</constructor-arg>
	</bean>
	
	<!-- Controllers & Mappings (necessary to override controllers with the same mappings in the target storefront) -->
	<bean name="registerPageController" class="com.salon.storefront.secureportaladdon.controllers.B2BRegistrationController"/>
	<!-- secure portal end -->
	
	
</beans>
