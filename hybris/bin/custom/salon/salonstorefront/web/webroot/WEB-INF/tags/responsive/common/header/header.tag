<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ attribute name="checkoutPage" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ attribute name="actionNameKey" required="false" type="java.lang.String" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

 <c:url value="/j_spring_security_check" var="loginActionUrl"/>
<cms:pageSlot position="TopHeaderSlot" var="component" element="div"
	class="container">
	<cms:component component="${component}" />
</cms:pageSlot>
<!-- Desktop View -->
<div class="hidden-xs hidden-sm visible-md visible-lg">
	<header class="js-mainHeader">
		<div class="top-bar">
			<div class="container">
				<div class="content">
					<div class="search">
						<cms:pageSlot position="SearchBox" var="component">
							<cms:component component="${component}" element="div" />
						</cms:pageSlot>
					</div>
					<div class="logo">
						<cms:pageSlot position="SiteLogo" var="logo" limit="1">
							<cms:component component="${logo}" element="div"
								class="yComponentWrapper" />
						</cms:pageSlot>
					</div>
					<nav class="nav-bar">
						<ul class="topbar-elements">
							<li><select class="form-control" name="" id="">
									<option value="en">en</option>
									<option value="en">fr</option>
							</select></li>
							<li><a href="#"><span class="icon icon-phone"></span></a></li>
						</ul>
						<ul class="topbar-login">
							<c:if test="${empty hideHeaderLinks}">
								<c:if test="${uiExperienceOverride}">
									<li class="backToMobileLink"><c:url
											value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
										<a href="${backToMobileStoreUrl}"> <spring:theme
												code="text.backToMobileStore" />
									</a></li>
								</c:if>
								
							<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
									<li class="topbar-login--form"><a href="#"><spring:theme
												code="header.link.login" /></a>
										<div class="login-panel dark-bg">
											<a href="#" class="icon icon-close"></a>
											<h4>
												<spring:theme code="login.login" />
											</h4>
											<!-- Login form starts -->
											<form:form action="${loginActionUrl}" method="post" id="loginFormPopup" commandName="loginForm">
										    <c:if test="${not empty message}">
												<span class="has-error"> <spring:theme code="${message}"/>
												</span>
										    </c:if>
										    <spring:theme code="login.email" var="usernamePlaceholder"/>
										    <spring:theme code="login.password" var="passwordPlaceholder"/>
										    <form:input path="j_username" id ="j_username_popup" type="text" cssClass="form-control" placeholder="${usernamePlaceholder}"/>
										    <form:input path="j_password" id="j_password_popup" type="password" cssClass="form-control" placeholder="${passwordPlaceholder}"/>
											<ycommerce:testId code="login_forgotPassword_link">
												<a href="#" data-link="<c:url value='/login/pw/request'/>"
													class="js-password-forgotten"
													data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
													<spring:theme code="login.link.forgottenPwd" />
												</a>
											</ycommerce:testId>
											<div class="login-panel--remember dark-bg">
												<input type="checkbox" name="remember" value=""> <label
													for="remember">Remember me</label>
											</div>
											<ycommerce:testId code="login_Login_button">
							               <button class="btn btn-login button" type="submit">
							                    <spring:theme code="login.login"/>
							                </button>
							            </ycommerce:testId>
											<div class="login-panel__separator"></div>
											<h4>New to the portal ?</h4>
											<c:if test="${enableRegistration}">
												<a href="<c:url value='/register'/>">
													<button type="button" class="btn-register btn button">
														<spring:theme code="text.secureportal.link.createAccount" />
													</button>
												</a>
											</c:if>
											</form:form>
										</div></li>
								</sec:authorize>
								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
									<li class="liOffcanvas"><ycommerce:testId
											code="header_signOut">
											<a href="<c:url value='/logout'/>"> <span
												class="icon icon-turnoff"></span>
											</a>
										</ycommerce:testId></li>
								</sec:authorize>
								
								<!-- 	Desktop wishList heart Drop down start  -->
								<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
									<li class="topbar-login--favorites"><cms:pageSlot
											position="WishListDropDownContentSlot" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot></li>
								</sec:authorize>
								<%-- <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
									<li><a href="<c:url value='/login'/>"> <span
											class="icon icon-favourite"></span>
									</a></li>
								</sec:authorize> --%>

                             <!-- 	Desktop wishList heart Drop down  end-->
							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<li><cms:pageSlot position="MiniCart" var="cart"
										element="div" class="componentContainer">
										<cms:component component="${cart}" element="div" />
									</cms:pageSlot></li>
							</sec:authorize>
							</c:if>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<a id="skiptonavigation"></a>
		<c:set var="page" value="LoginRegister"></c:set>
		<c:if test="${checkoutPage eq false && pageType ne page}">
			<nav:topNavigation />
		</c:if>
	</header>
</div>
<!-- Mobile view -->
<div class="visible-xs visible-sm hidden-md hidden-lg">
	<header>
		<div class="top-bar">
			<div class="container">
				<div class="content">
					<div class="burger-button">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
							<cms:component component="${logo}" element="div"
								class="yComponentWrapper" />
						</cms:pageSlot>
					<div class="hamburger-icon">
					</div>
					</div>
					<div class="icons-list">
						<a href="#"><span class="icon icon-search"></span></a> <a href="#"><span
							class="icon icon-account"></span></a>
						<!-- Mobile view	wish list drop down -->
						<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <cms:pageSlot position="WishListDropDownContentSlot"
								var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</sec:authorize>
						<%-- <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">

							<a href="<c:url value='/login'/>"> <span
								class="icon icon-favourite"></span></a>
						</sec:authorize> --%>
						<!-- Mobile view wish list drop down end-->
						<a href="#"><span class="icon icon-shoppingbag"></span></a>
					</div>
				</div>
			</div>
		</div>
		<div class="menu-mobile closed">
			<ul class="main-menu">
				<nav:topNavigation />
			</ul>
			<ul class="secondary-menu">
				<li><a href="#support">Support</a></li>
				<li><a href="#term">Term &amp; Conditions</a></li>
				<li><a href="#signout">Sign out</a></li>
			</ul>
		</div>
	</header>
</div>
<!-- mobile view end -->
<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"
	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>