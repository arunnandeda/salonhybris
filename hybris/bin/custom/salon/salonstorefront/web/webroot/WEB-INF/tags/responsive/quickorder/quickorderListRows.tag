<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="quickorder"
	tagdir="/WEB-INF/tags/responsive/quickorder"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/cart/addQuickOrder" var="quickOrderAddToCartUrl"
	htmlEscape="false" />
<spring:theme code="text.quickOrder.product.quantity.max"
	var="maxProductQtyMsg" />
<spring:theme code="text.quickOrder.form.product.exists"
	var="productExistsInFormMsg" />
<spring:theme code="text.quickOrder.placeholder" var="searchPlaceholder" />
<spring:theme code="text.quickOrder.zero.quantity"
	var="zeroQtyMsg" />	
	<spring:theme code="text.quickOrder.only.numeric"
	var="onlyNumericMsg" />
<c:url value="/quickOrder/productInfo" var="searchUrl" />
<c:url value="/search/autocomplete/SearchBox" var="autocompleteUrl" />

<div class="add-items js-quick-order-container"
	data-quick-order-min-rows="${quickOrderMinRows}"
	data-quick-order-max-rows="${quickOrderMaxRows}"
	data-quick-order-add-to-cart-url="${quickOrderAddToCartUrl}"
	data-product-exists-in-form-msg="${productExistsInFormMsg}">
	<div class="add-search js-li-container">
		<h2 class="h3">
			<spring:theme code="text.quickOrder.additems" />
		</h2>
		<div class="form-wrap">
			<div class="dropdown">
				<!-- 	<div
				class="item__sku__input js-sku-container quickOrder_searchBox quickSearch"> -->
				<form name="search_form_SearchBox" method="get"
					class="search_form_searchBox"
					action="/salonstorefront/salon/en/EUR/search/">
					<input type="text" name="text" id="js-quick-search-input"
						class="js-sku-input-field ui-autocomplete-input js-quick-search-input"
						placeholder="${searchPlaceholder}"
						data-options='{"autocompleteUrl" : "${autocompleteUrl}","minCharactersBeforeRequest" : "3","waitTimeBeforeRequest" : "300","displayProductImages" : true}'>
					<i class="icon icon-help" data-toggle="modal"
						data-target="#modal-quick-order"></i> <input type="submit"
						value="search" class="false-hidden">
				</form>
				<div
					class="js-sku-validation-container help-block quick-order__help-block"></div>
			</div>
		</div>

		<!-- </div> -->
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-8">
		<div class="order-table order-list">
			<div class="table-wrap">
				<table class="js-product-container">
					<thead>
						<tr>
							<th></th>
							<th><spring:theme code="text.wishlist.Product" /></th>
							<th><spring:theme code="text.wishlist.Package" /></th>
							<th class="th-position-r"><spring:theme
									code="text.wishlist.unit" /></th>
							<th class="th-position-c"><spring:theme
									code="text.quickOrder.quantity" /></th>
							<th><spring:theme code="text.quickOrder.page.total" /></th>
							<th class="th-position-r"></th>
						</tr>
					</thead>
					<tbody class="item__productQty__input js-product-list-container">
					</tbody>
				</table>
			</div>
			<div class="table-bottom">
				<ul class="table-nav">
					<li ><a href="" id="js-reset-quick-order-form-btn-top"><spring:theme
								code="text.quickOrder.clearOrder"/></a></li>
					<li>
					
						<!-- <a class="add-favorite" href="#Add-to-favorites"> --> 
						<span class="text-left ui-state-disabled js-add-remove-disable">
						<cms:pageSlot
							position="AddToWishListContentSlot" var="feature">
							<cms:component component="${feature}"/>
		            </cms:pageSlot>
		            </span> <!-- <span
							class="icon icon-favorite_border"></span>+</a> -->
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="current-order">
			<h4>
				<spring:theme code="text.quickOrder.current.quickorder" />
			</h4>
			<div class="text-center ">
			<span class="item-number quickOrderAddCart-TotalItem">0</span>
			<span><spring:theme code="text.quickOrder.totalItem" /></span></div>
			<div class="cart-total quickOrderAddCart-TotalPrice"></div>
			<div class="button-wrapper">
				<button class="btn btn-primary add-item-to-cart addToCartButton"
					id="js-add-to-cart-quick-order-btn-top" disabled="disabled">
					<spring:theme code="text.addToCart" var="addToCartText" />
					<spring:theme code="basket.add.to.basket" />
				</button>
			</div>
		</div>
	</div>
</div>
 <div class="modal fade modal--quick-order" tabindex="-1" role="dialog" id="modal-quick-order">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><spring:theme code="text.quickOrder.welcome.quickorder"/></h3>
              </div>
              <div class="modal-body">
                <div class="number-list">
                  <ul>
                    <li>
                      <span class="big-number">1</span>
                      <div class="item-content">
                        <p><spring:theme code="text.quickOrder.build.quickorder"/></p>
                      </div>
                    </li><li>
                      <span class="big-number">2</span>
                      <div class="item-content">
                        <p><spring:theme code="text.quickOrder.many.quickorder"/></p>
                        <p><spring:theme code="text.quickOrder.edit.quickorder"/></p>
                      </div>
                    </li><li> 
                      <span class="big-number">3</span>
                      <div class="item-content">
                        <p><spring:theme code="text.quickOrder.when.quickorder"/></p>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="video-wrapper">
                  <iframe width="582" height="327" src="https://www.youtube.com/embed/t2BvDw2GkK8" frameborder="0" allowfullscreen=""></iframe>
                </div>
                <div class="form-group">
                  <div class="checkbox inverted">
                    <label>
                      <input type="checkbox" id="quick-order-modal-check" name="quick-order-modal-check">
                      <spring:theme code="text.quickOrder.dont.quickorder"/>
                    </label>
                  </div>
                </div>
                <div class="button-wrapper" data-dismiss="modal" >
                  <button class="btn btn-primary"> <spring:theme code="text.quickOrder.ok.quickorder"/></button>
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<script id="quickOrderRowTemplate" type="text/x-jquery-tmpl">
<tr>
		<td><a href="${request.contextPath}{{= url}}" tabindex="-1">
						{{if images != null && images.length > 0}} <img
						src="{{= images[0].url}}" alt=""/>
						{{else}} <img
						src="${themeResourcePath}/images/missing_product_EN_300x300.jpg" alt=""/>
						{{/if}}

	         </a>
        </td>

		<td><span class="product-name"> {{= name}}</span>
        <!--  {{if stock.stockLevelStatus.code && stock.stockLevelStatus.code !=
						'outOfStock'}} <span class="stock"> <spring:theme
								code="product.variants.in.stock" />
                          </span> 
					
           {{else}} <span class="out-of-stock"> <spring:theme
								code="product.variants.out.of.stock" />
					     </span>
             {{/if}} -->
          </span> 
       <span class="product-color">Light Blue Gold</span></td>
       <td>250 ml can</td>
           
		<td class="td-position-r">
					<div class="price">
                        <span class="js-product-price{{= code}} js-product-info" data-product-price="{{= price.value}}">
						<span class="currency"></span> <span id="value9">{{= price.formattedValue}}</span>
					</div></span>
		</td>
		<td class="td-position-c">
						{{if stock.stockLevelStatus.code=='outOfStock'}} 
                           <input type="text" class="js-quick-order-qty{{= code}}  js-quick-order-qty form-control"
							value="0" maxlength="3" size="1" data-product-code="{{= code}}"
							data-max-product-qty="{{= stock.stockLevel}}"
							data-stock-level-status="{{= stock.stockLevelStatus.code}}"
							data-price="{{= price.value}}" data-name="{{= name}}" readonly />
					 
                        <span class="js-product-info js-qty-validation-container_{{= code}} help-block quick-order__help-block"
							data-max-product-qty-msg="${maxProductQtyMsg}"></span>
						{{else}} 
                           <input type="text" class="js-quick-order-qty{{= code}}  js-quick-order-qty form-control"
							value="1" maxlength="3" size="1" data-product-code="{{= code}}"
							data-max-product-qty="{{= stock.stockLevel}}"
							data-stock-level-status="{{= stock.stockLevelStatus.code}}"
							data-price="{{= price.value}}" data-name="{{= name}}" />
						<span
							class="js-product-info js-qty-validation-container_{{= code}} help-block quick-order__help-block"
							data-max-product-qty-msg="${maxProductQtyMsg}" data-zero-qty-msg="${zeroQtyMsg}" 
                                data-only-numeric-msg="${onlyNumericMsg}" ></span>
                           
						{{/if}}
          </td>
		<td class="td-position-total">
					<div class="price js-product-info js-quick-order-item-total{{= code}}">
						<span class="currency"></span>
                          <span id="result9"> 
                          {{if stock.stockLevelStatus.code && stock.stockLevelStatus.code != 'outOfStock'}}										
	                            {{= price.formattedValue}}   
		                        {{else}}											
	                          {{= price.formattedValue}}
		                      {{/if}}	
                       </span>
					</div>
				</td>
				<td class="td-position-r">
                     <button class="js-remove-quick-order-row icon icon-delete has-border">
                       </button>
		</td>
</tr>
</script>