<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="path" required="true" rtexprvalue="true"%>
<%@ attribute name="errorPath" required="false" rtexprvalue="true"%>
<%@ attribute name="errorCtrElementType" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:bind path="${not empty errorPath ? errorPath : path}">
	<c:choose>
		<c:when test="${not empty status.errorMessages}">
			<div class="form-group" >
				<jsp:doBody />
				
				<c:if test="${ empty errorCtrElementType }">
					<c:set value="div" var="errorCtrElementType"></c:set>
				</c:if>
				<${errorCtrElementType} class="validation-submit validation error">
					<div class="validation-container">
						<h2 class="validation-title">
							<form:errors path="${not empty errorPath ? '' : path}" />
						</h2>
					</div>
				</div>
			</${errorCtrElementType}>
			
			
		</c:when>
		<c:otherwise>
			<div class="form-group">
				<jsp:doBody />
			</div>
		</c:otherwise>
	</c:choose>
</spring:bind>
