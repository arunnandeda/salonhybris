<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="false"
	type="java.lang.String"%>
<%@ attribute name="action" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />


<div class="container">
<div class="user-register__headline col-md-10">
	<spring:theme code="register.new.customer" />
</div>
<div class="col-md-12">
<p>
	<spring:theme code="register.description" />
</p>
</div>
<div id="b2bactivation">
<form:form method="post" commandName="b2BRegistrationForm"
	action="${action}">
	
	<div class = "column">
	<div class="col-xs-12 col-md-8">
	<div class="required_fieldstyle" style="position:relative">
	<formElement:formSelectBox idKey="register.title"
		labelKey="register.title" selectCSSClass="form-control required_field"
		path="titleCode" mandatory="true" skipBlank="false"
		skipBlankMessageKey="form.select.empty" items="${titles}" />
	</div>	
		<div class="required_fieldstyle form-input-section" style="position:relative">
	<formElement:formInputBox idKey="name" labelKey="Full Name" path="name"
		inputCSS="form-control required_field" mandatory="true" />
			<div id="customer_name_err"
				class="validation-submit validation validation-email" style="display:none">
				<div class="validation-container">
					<h2 class="validation-title">
						<spring:theme code="text.secureportal.register.name" />
					</h2>
				</div>
			</div>
			</div>
			<div class="required_fieldstyle form-input-section" style="position:relative">
			<formElement:formPasswordBox idKey="pwd" labelKey="Password"
				path="pwd" inputCSS="form-control required_field password-strength"
				mandatory="true" />
			<div id="pwd_err"
				class="validation-submit validation validation-email" style="display:none">
				<div class="validation-container">
					<h2 class="validation-title">
						<spring:theme code="text.b2bRegister.password.length" />
					</h2>
				</div>
			</div>
			</div>
					<div class="required_fieldstyle form-input-section" style="position: relative">
						<formElement:formPasswordBox idKey="checkPwd"
							labelKey="Confirm Password" path="checkPwd"
							inputCSS="form-control required_field" mandatory="true" />
						<div id="checkpwd_err"
							class="validation-submit validation validation-email" style="display:none">
							<div class="validation-container">
								<h2 class="validation-title">
									<spring:theme code="validation.checkPwd.equals" />
								</h2>
							</div>
						</div>
					</div>
				</div>
			<div class="form-actions clearfix">
		<form:hidden path="token" />
		<div class="col-xs-12 col-md-8">
		<ycommerce:testId code="register_Register_button">
		
			<button type="submit" id="registerFormSubmit"
				class="btn btn btn-primary btn-block">
				<spring:theme code='${actionNameKey}' />
			</button>
		</ycommerce:testId>
		</div>
	</div>
	</div>
	
</form:form>
</div>
</div>