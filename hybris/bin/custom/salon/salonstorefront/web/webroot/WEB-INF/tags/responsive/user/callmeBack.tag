<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="false"
	type="java.lang.String"%>
<%@ attribute name="action" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<!-- Modal -->
		<div id="callMeBackModal" class="modal fade" tabindex="-1" role="dialog" >
			<div class="modal-dialog" role="document">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4  class="modal-body">
							<label><spring:theme code="call.back.header.msg"/></label>
						</h4>
					</div>
					<div class="modal-body">
					<c:url value="register/callmeback" var="callMeBackAction"></c:url>
						<form method="POST" action="${callMeBackAction }">
							<div class="form-input-section" style="position: relative">
								<label><spring:theme
										code="text.secureportal.register.prospect.name.placeholder" /></label><input
									type="text" name="fullName" id="fullName"
									placeholder="<spring:theme code="call.back.user.name" />"
									class="form-control required_field" />
								<div id="name_err"
									class="validation-submit validation validation-email " style="display:none">
									<div class="validation-container">
										<h2 class="validation-title">
											<spring:theme code="text.secureportal.register.name" />
										</h2>
									</div>
								</div>
							</div>
							<br />
							<div class="form-input-section" style="position: relative">
								<label><spring:theme
										code="text.secureportal.register.prospect.contact" /></label><input
									type="number" name="phoneNo" id="phoneNo"
									placeholder="<spring:theme code="call.back.user.number" />"
									class="form-control required_field" />
								<div id="phone_err"
									class="validation-submit validation validation-email" style="display:none">
									<div class="validation-container">
										<h2 class="validation-title">
											<spring:theme code="text.secureportal.callback.phone" />
										</h2>
									</div>
								</div>
							</div>
							<br />
							<div class="form-input-section" style="position: relative">
								<label><spring:theme
										code="text.secureportal.register.prospect.preferredtime" /></label>
										<textarea rows="2" cols="6" name="time" id="time"
									placeholder="<spring:theme code="call.back.user.preferredtime" />"
									class="form-control required_field"></textarea>
								<div id="time_err"
									class="validation-submit validation validation-email " style="display:none">
									<div class="validation-container">
										<h2 class="validation-title">
											<spring:theme code="text.secureportal.register.time" />
										</h2>
									</div>
								</div>
							</div>
							<br />
							<div class="form-input-section" style="position: relative">
								<label><spring:theme
										code="text.secureportal.register.prospect.reason" /> </label>
								<textarea rows="2" cols="6" name="reason" id="reason"
									placeholder="<spring:theme code="call.back.user.reason" />" class="form-control required_field">
								</textarea>
								<div id="reason_err"
									class="validation-submit validation validation-email"
									style="display: none">
									<div class="validation-container">
										<h2 class="validation-title">
											<spring:theme code="text.secureportal.register.reason" />
										</h2>
									</div>
								</div>
							</div>
							<br /> <input type="hidden" name="CSRFToken"
								value="${CSRFToken}" />
							<p align="center">
								<button class="btn btn-primary" id="contactInfo">
									<spring:theme
										code="text.secureportal.register.customer.submit.button" />
								</button>
							</p>
						</form>
					</div>
					<div class="modal-footer"></div>
				</div>

			</div>
		</div>
		<!--  modal ends-->