<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<%-- <div class="login-page__headline">
	<spring:theme code="login.title" />
</div> --%>
<form:form action="${action}" method="post" commandName="loginForm">
	<div class="login-banner">
		<div class="row">
			<div class="col-xs-7 col-md-3">
                <div class="h3">login for <br />special offers</div>
              </div>
				<c:if test="${not empty message}">
					<span class="has-error"> <spring:theme code="${message}" />
					</span>
				</c:if>

				<spring:theme code="login.email" var="usernamePlaceholder" />
				<spring:theme code="login.password" var="passwordPlaceholder" />

				 <div class="hidden-xs hidden-sm col-md-2 col-lg-3 relative">
				<form:input id="j_username" placeholder="${usernamePlaceholder}"
					path="j_username" cssClass="form-control" />
				<a class="help-link" data-enable-not-logged href="<c:url value='/register'/>"><spring:theme code="text.secureportal.link.createAccount" /></a>
				<div id="valid-email" class="validation validation-email">
					<div class="validation-container">
						<h2 class="validation-title"><spring:theme code="account.error.credentials.wrong"/></h2>
						<p class="validation-text"><spring:theme code="text.secureportal.register.error"/></p>
					</div>
				</div>
			</div>
			<div class="hidden-xs hidden-sm col-md-2 col-lg-3">
				<form:input id="j_password" placeholder="${passwordPlaceholder}"
					path="j_password" type="password" cssClass="form-control" />
				<ycommerce:testId code="login_forgotPassword_link">
					<a class="help-link" href="#"
						data-link="<c:url value='/login/pw/request'/>"
						data-cbox-title="<spring:theme code="forgottenPwd.title"/>"> <spring:theme
							code="login.link.forgottenPwd" /></a>
				</ycommerce:testId>
			</div>
			<div class="col-xs-5 col-md-4 col-lg-3 cta">
				<div class="hidden-xs hidden-sm remember">
					<input type="checkbox" name="remember" value=""> <label
						for="remember"><spring:theme code="account.remember.login"/></label>
				</div>
				<div class="login-button">
					<ycommerce:testId code="login_Login_button">
						<button type="submit" id="link-login-mobile"
							data-enable-not-logged class="btn btn-inverted mobile-link">
							<spring:theme code="${actionNameKey}" />
						</button>
					</ycommerce:testId>
					<button id="login-btn1" data-enable-not-logged
						class="btn btn-inverted desktop-link">
						<spring:theme code="${actionNameKey}" />
					</button>
				</div>
			</div>
		</div>
	</div>

	<%-- <div class="forgotten-password">
        <ycommerce:testId code="login_forgotPassword_link">
            <a href="#" data-link="<c:url value='/login/pw/request'/>" class="js-password-forgotten"
               data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
                <spring:theme code="login.link.forgottenPwd"/>
            </a>
        </ycommerce:testId>
    </div> --%>

	<%-- <div class="row login-form-action">
		<c:set var="loginBtnClasses" value="col-sm-12 col-md-6 col-md-push-6" />

		<c:if test="${enableRegistration}">
			<c:set var="loginBtnClasses" value="col-sm-6 col-sm-push-6" />
		</c:if>

		<div class="${loginBtnClasses}">
			<ycommerce:testId code="login_Login_button">
				<button type="submit" class="btn btn-primary btn-block">
					<spring:theme code="${actionNameKey}" />
				</button>
			</ycommerce:testId>
		</div>
 --%>
		<%-- <div class="col-sm-6 col-sm-pull-6">
			<c:if test="${enableRegistration}">
				<a href="<c:url value='/register'/>">
					<button type="button" class="btn btn-default btn-block">
						<spring:theme code="text.secureportal.link.createAccount" />
					</button>
				</a>
			</c:if>
		</div>
	</div> --%>
</form:form>

