<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>

<c:if test="${navigationNode.visible}">
	<aside class="nav-left col-xs-12 col-md-3">
		<div class="side-menu-container">
			<div class="side-menu-switcher visible-xs-block visible-sm-block">
				<a class="side-menu-switcher__link" role="button"
					data-toggle="collapse" href="#side-menu" aria-expanded="false"
					aria-controls="side-menu">${navigationNode.title} <span
					class="icon icon-arrow"></span>
				</a>
			</div>
			<div id="side-menu" class="side-menu panel-group" role="tablist"
				aria-multiselectable="true">
				<c:forEach items="${navigationNode.links}" var="link">
					<div class="side-menu-item panel">
						<a id="icon-close" href="#" class="icon icon-close"></a>
						<div class="side-menu-item__header" role="tab"
							id="side-menu-item-heading-2">

							<a href="${request.contextPath}${link.url}"
								class="side-menu-item__link collapsed"
								aria-controls="side-menu-item-body-2">${link.linkName}</a>

						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</aside>
</c:if>

