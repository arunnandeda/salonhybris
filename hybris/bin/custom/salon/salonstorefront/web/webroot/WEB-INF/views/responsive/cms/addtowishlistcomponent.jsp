<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- First wish list pop up -->
<div class="modal fade modal--add-favorite"  tabindex="-1" role="dialog" id="modal-add-favorite">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
         </div>
     </div>
 </div>
 <!-- second sucess wish list pop up -->
 <div class="modal fade modal--save-favorite"  tabindex="-1" role="dialog" id="modal-save-favorite">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
         </div>
     </div>
 </div>
 <a href="#" id="wishListPopup"  data-product-code="${product.code}" >
  <span class="icon icon-favorite_border"></span>+
 </a>