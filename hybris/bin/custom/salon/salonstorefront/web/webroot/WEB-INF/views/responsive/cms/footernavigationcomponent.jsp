<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:if test="${component.visible}">
			<div class="col-xs-12 col-sm-3 col-lg-2 footer__followus--goldwell">
				<h3>
					<spring:theme code="footer.text.goldwell" />
				</h3>
				<ul>
					<c:forEach items="${component.navigationNode.children}"
						var="childLevel1" step="${component.wrapAfter}" varStatus="i">

						<c:forEach items="${childLevel1.children}" var="childLevel2"
							begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
							<c:forEach items="${childLevel2.entries}" var="childlink">

								<li><a href="${childlink.item.url}"> <c:if
											test="${fn:indexOf(childlink.item.uid, 'Facebook') > -1}">
											<c:set var="iconClass" value="icon-facebook"></c:set>

										</c:if> <c:if
											test="${fn:indexOf(childlink.item.uid, 'Twitter') > -1}">
											<c:set var="iconClass" value="icon-twitter"></c:set>

										</c:if> <c:if test="${fn:indexOf(childlink.item.uid, 'You') > -1}">
											<c:set var="iconClass" value="icon-youtube"></c:set>

										</c:if> <c:if test="${fn:indexOf(childlink.item.uid, 'Linked') > -1}">
											<c:set var="iconClass" value="icon-linkedin"></c:set>

										</c:if> <span class="icon ${iconClass}"></span>

								</a></li>
							</c:forEach>
							
						</c:forEach>
					</c:forEach>

				</ul>
				<div>
					<c:forEach items="${component.navigationNode.children}"
						var="childLevel1">

						<c:forEach items="${childLevel1.children}" var="childLevel2">
							<c:forEach items="${childLevel2.entries}" var="childlink">
								<c:if test="${fn:indexOf(childlink.item.uid, 'Terms') > -1}">
									<a href="${childlink.item.url}" class="footer__followus--terms">Terms
										of use</a>
								</c:if>
								<c:if test="${fn:indexOf(childlink.item.uid, 'Contact') > -1}">
									<a href="${childlink.item.url}"
										class="footer__followus--contact">Contact us</a>
								</c:if>
							</c:forEach>
						</c:forEach>

					</c:forEach>
				</div>
			</div>
			<div class="col-xs-12 col-sm-1 col-lg-1 footer__separator--followus">
                        <div class="footer__separatorbg"></div>
                      </div>
			<div class="col-xs-12 col-sm-3 col-lg-2 footer__followus--kms">
				<h3>
					<spring:theme code="footer.text.kms" />
				</h3>
				<ul>
					<c:forEach items="${component.navigationNode.children}"
						var="childLevel1" step="${component.wrapAfter}" varStatus="i">

						<c:forEach items="${childLevel1.children}" var="childLevel2"
							begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
							<c:forEach items="${childLevel2.entries}" var="childlink">

								<li><a href="${childlink.item.url}"> <c:if
											test="${fn:indexOf(childlink.item.uid, 'Facebook') > -1}">
											<c:set var="iconClass" value="icon-facebook"></c:set>

										</c:if> <c:if
											test="${fn:indexOf(childlink.item.uid, 'Twitter') > -1}">
											<c:set var="iconClass" value="icon-twitter"></c:set>

										</c:if> <c:if test="${fn:indexOf(childlink.item.uid, 'You') > -1}">
											<c:set var="iconClass" value="icon-youtube"></c:set>

										</c:if> <c:if test="${fn:indexOf(childlink.item.uid, 'Linked') > -1}">
											<c:set var="iconClass" value="icon-linkedin"></c:set>

										</c:if> <span class="icon ${iconClass}"></span>

								</a></li>
							</c:forEach>
						</c:forEach>

					</c:forEach>
				</ul>
			</div>
		
		<div class="col-xs-12 col-sm-5 col-lg-3">
			<div class="footer__gettheapp">
				<c:forEach items="${component.navigationNode.children}"
					var="childLevel1">
					<c:forEach items="${childLevel1.children}"
						step="${component.wrapAfter}" varStatus="i">
						<c:if
							test="${component.wrapAfter > i.index && fn:indexOf(childLevel1.title, 'App') > -1}">
							<h3>${childLevel1.title}</h3>
						</c:if>
					</c:forEach>
				</c:forEach>


				<div class="footer__gettheapp--ctas">
					<c:forEach items="${component.navigationNode.children}"
						var="childLevel1">
						<c:forEach items="${childLevel1.children}" var="childLevel2">
							<c:forEach items="${childLevel2.entries}" var="childlink">


								<c:if test="${fn:indexOf(childlink.item.uid, 'Android') > -1}">
									<a href="${childlink.item.url}"
										class="footer__gettheapp--android"></a>
								</c:if>
								<c:if test="${fn:indexOf(childlink.item.uid, 'AppStore') > -1}">
									<a href="${childlink.item.url}" class="footer__gettheapp--ios"></a>
								</c:if>
							</c:forEach>
						</c:forEach>
					</c:forEach>

				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-right">
					<a href="#" class="">${notice}</a>
				</div>
			</div>
		</div>
	<div class="footer__right col-xs-12 col-md-3 no-border">
		<c:if test="true">
			<div class="row">
				<div class="col-xs-6 col-md-6 footer__dropdown lang_selector">
					<footer:languageSelector languages="${languages}"
						currentLanguage="${currentLanguage}" />
				</div>
				<div class="col-xs-6 col-md-6 footer__dropdown currency selector">
					<footer:currencySelector currencies="${currencies}"
						currentCurrency="${currentCurrency}" />
				</div>
			</div>
		</c:if>
	</div>
</c:if>