<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<c:url value="/my-account/favourite" var="setActionUrl" />
<c:url value="/my-account/favourites" var="allWishListUrl" />

<!-- Desktop view -->
<div class="hidden-xs hidden-sm visible-md visible-lg">
	<a href="#"><span class="icon icon-favourite"></span></a>
	<div class="favorite-panel dark-bg">
		<a href="#" class="icon icon-close"></a>
		<h4>
			<spring:theme code="text.wishList.dropdown.myfavorite" />
		</h4>
		<div class="account-panel__separator"></div>
		<c:if test="${not empty allWishList}">
			<div>
				<h4>
					<spring:theme code="text.wishList.dropdown.lists" />
				</h4>
				<c:forEach items="${allWishList}" var="wishlistEntry">
					<a href="${setActionUrl}/${wishlistEntry.id}">${wishlistEntry.name}</a>
				</c:forEach>

			</div>
		</c:if>
		<c:if test="${not empty allWishList}">
			<div class="account-panel__separator"></div>
			<a href="${allWishListUrl}"><spring:theme
					code="text.wishList.dropdown.manage" /> </a>
		</c:if>
		<c:if test="${empty allWishList}">
			<spring:theme code="text.wishlist.allempty" />
		</c:if>

	</div>
</div>
<!-- mobile view -->
<div class="visible-xs visible-sm hidden-md hidden-lg">
	<a href="${allWishListUrl}" class="icon icon-favourite"></a>
</div>