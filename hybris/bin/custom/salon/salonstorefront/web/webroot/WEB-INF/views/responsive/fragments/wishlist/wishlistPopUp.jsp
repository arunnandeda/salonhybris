<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title">
			<spring:theme code="text.wishlist.addtofavoritepopup" />
		</h4>
	</div>
	<div class="modal-body">
		<c:choose>
			<c:when test="${invalidUser eq true}">
				<div class="wishlist-invalid-user">
					<spring:theme code="text.wishlist.invaliduser" />
				</div>
			</c:when>
			<c:otherwise>
				<div class="from-group">
					<form:form method="POST"
						action="${request.contextPath}/wishlist/add-to-wishlist"
						id="quickorderwishList" commandName="quickorderwishList">

						<input type="hidden" id="productCode" class="form-control"
							name="productCode" value="${productCode}">
						<span class="js-special-char-notallowd display-none"><spring:theme
								code="wishlist.special.char.notallowed" /></span>
						<span class="js-empty-WishList display-none"> <spring:theme
								code="text.wishlist.enter.wishlistname" />
						</span>

						<div class="checkbox">
							<label> <input type="radio" id="js-New-wishList-checkbox"
								name="favorite">
							<spring:theme code="text.wishlist.favoritesList" />
							</label> <input type="text" id="newWishlistName" name="newWishlistName"
								class="form-control" placeholder="list name"> <label>
								<input type="radio" id="js-existing-wishList-checkbox"
								name="favorite">
							<spring:theme code="text.wishlist.existingList" />
							</label> <select name="wishlistName" id="wishlistName" 
								class="form-control">
								<option value=""><spring:theme
										code="text.wishlist.select" /></option>
								<c:forEach var="wishList" items="${wishListData}">
									<option value="${wishList.name}"><c:out
											value="${wishList.name}" />
								</c:forEach>
							</select>
						</div>
						<span class="js-empty-existing-WishList display-none"> <spring:theme
								code="text.wishlist.existing.empty" />
						</span>
						<div class="buttons">
							<a href="#" class="btn button" id="modal-add-fav-cancel"
								data-dismiss="modal"> <spring:theme
									code="text.wishlist.cancel" /></a> <a href="#"
								class="btn btn-primary js-Favorite-submit" id="modal-add-fav"
								> <spring:theme code="text.wishlist.save" /></a>
						</div>
					</form:form>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>
