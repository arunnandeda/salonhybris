<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:url value="/my-account/favourite" var="setFavouriteUrl" />
<c:url value="/my-account/removeWishList" var="removeWishListUrl" />
<c:url value="/my-account/create-wishlist" var="newWishListUrl" />


<!-- this model popup for create new wish list -->
<div class="modal fade modal--new-list" tabindex="-1" role="dialog"
	id="modal-new-list">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">
					<spring:theme code="text.wishlist.newWishlist" />
				</h3>
			</div>
			<div class="modal-body">

				<form:form action="${newWishListUrl}" method="post"
					id="newwishistForm">
					<div class="input">
						<input type="text" class="form-control input-name js-inputfield"
							name="wishListName"
							placeholder="<spring:theme code="text.wishlist.newWishlist.placeholder"/>">
							
						<!-- Validation error message for empty and special character  -->
						<span class="js-special-char-notallowd display-none"
							><spring:theme
								code="wishlist.special.char.notallowed" /></span> 
						<span class="js-empty-WishList display-none">
							<spring:theme code="text.wishlist.enter.wishlistname" />
						</span>
					</div>
					<div class="buttons">
						<a href="" class="btn button" data-dismiss="modal"><spring:theme
								code="text.wishlist.cancel" /></a>
						<button id="js-newwishList-btn" class="btn btn-primary">
							<spring:theme code="text.wishlist.save" />
						</button>
					</div>
				</form:form>

			</div>
		</div>
	</div>
</div>
<!-- this model popup for create new wish list end-->

<div class="main">
	<div class="container">
		<div class="favorites">
			<div class="row">
				<cms:pageSlot position="SideContent" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<div class="col-xs-12 col-md-9">
					<c:choose>
						<c:when test="${not empty customerWishList}">
							<h1 class="page-title"><spring:theme code="text.wishlist.title.favorites"/></h1>
							<div class="favorites-table">
								<div class="table-wrap">
									<table>
										<thead>
											<tr>
												<th><spring:theme code="text.wishlist.name" /></th>
												<th><spring:theme code="text.wishlist.creationdate" /></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${customerWishList}" var="wishlistEntry">
												<tr>
													<td>${wishlistEntry.name}</td>
													<td><fmt:formatDate pattern="dd/MM/yyyy"
															value="${wishlistEntry.creationtime}" /></td>
													<td class="actions"><a
														href="${setFavouriteUrl}/${wishlistEntry.id}"><spring:theme
																code="text.wishlist.edit" />
													</a> <a href="${removeWishListUrl}/${wishlistEntry.name}">
													<spring:theme code="text.wishlist.delete" /></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<div class="btn-link">
										<a href="#" class="btn btn-primary" data-toggle="modal"
											data-target="#modal-new-list"><spring:theme
												code="text.wishlist.newWishlist" /></a>
									</div>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="alert alert-info">
								<strong><spring:theme code="text.wishlist.allempty" /></strong>
							</div>
						</c:otherwise>
					</c:choose>

				</div>
			</div>
		</div>
	</div>
</div>