<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:url value="/my-account/edit-wishlistname" var="editWishListUrl" />
<c:url value="/my-account/favourite" var="setActionUrl" />
<c:url value="/my-account/removeWishList" var="removeWishListUrl" />

<product:addToCartTitle />
<!-- <div class="hidden-xs hidden-sm visible-md visible-lg"> -->
<!--  <div class="hidden-xs hidden-sm visible-md visible-lg"> -->
<div class="main">
	<div class="container">
		<div class="favorite-details">
			<div class="row">
				<cms:pageSlot position="SideContent" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<div class="col-xs-12 col-md-9">
					<c:choose>
						<c:when test="${wishListData!=null}">
						<!-- add items search -->
							<%-- <div class="add-items">
								<div class="add-search">
									<h2 class="h3">
										<spring:theme code="text.wishlist.additem" />
									</h2>  
									<div class="form-wrap">

										<div class="dropdown">
											<div class="">
												<form action="#search" method="get">
													<input type="text" name="add-search" id="add-search"
														placeholder="<spring:theme code="text.wishList.addfavorite.placeholder"/>"> <i
														class="icon icon-help" data-toggle="modal"
														data-target="#modal-quick-order"></i> <input type="submit"
														value="search" class="false-hidden">
												</form>
											</div>
										</div>
									</div>
								</div>
							</div> --%>
							<div class="title">
								<form:form action="${editWishListUrl}" method="post"
									id="editFrom">
									<input id="input" name="wishListName" type="text"
										value="${wishListData.name}" disabled="disabled">
									<input type="hidden" name="wishListId"
										value="${wishListData.id}">
									<span id="edit-name" class="icon icon-pencil"></span>
									<div class="actions pull-right">
										<div id="cancel-btn" class="btn btn-primary"><spring:theme
												code="text.wishlist.cancel" /></div> <a id="save-btn"
											class="btn btn-primary" href=""><spring:theme
												code="text.wishlist.save" /></a>
									</div>
								</form:form>
							</div>
							<c:set value="${wishListData.entries}" var="wishListproduct"></c:set>
							<!--   If wish list  contain any product or not ... -->
							<c:choose>
								<c:when test="${fn:length(wishListData.entries) gt 0}">
									<div class="order-table order-list">
										<div class="table-wrap">
											<table>
												<thead>
													<tr>
														<th></th>
														<th><spring:theme code="text.wishlist.Product" /></th>
														<th><spring:theme code="text.wishlist.Package" /></th>
														<th><spring:theme code="text.wishlist.unit" /></th>
														<th></th>
													</tr>
												</thead>
												<tbody>

													<c:forEach items="${wishListData.entries}"
														var="wishListProductData">
														<tr id="productListcount"
															data-stock-level-status="${wishListProductData.product.stock.stockLevelStatus.code}"
															data-product-code="${wishListProductData.product.code}"
															data-name="${wishListProductData.product.name}">
															<c:url value="${wishListProductData.product.url}"
																var="productUrl" />
															<td><a href="${productUrl}"
																title="${wishListProductData.product.name}"><product:productPrimaryImage
																		product="${wishListProductData.product}"
																		format="thumbnail" /> </a></td>

															<td><span class="product-name">${wishListProductData.product.name}</span>
																<span class="product-color"></span> <c:if
																	test="${wishListProductData.product.stock.stockLevelStatus.code eq 'outOfStock'}">
																	<span class="product-color"><spring:theme
																			code="text.product.out.of.stock" /></span>
																</c:if></td>
															<td></td>
															<td>
																<div class="price-cart">${wishListProductData.product.price.formattedValue}</div>
															</td>
															<td>
																<button
																	class="icon icon-delete has-border js-removeProductFromWishlist"
																	productCode="${wishListProductData.product.code}"
																	wishListId="${wishListData.id}"></button>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>

										</div>
										<div class="table-bottom">
											<div class="table-pages">
												<!-- This button Remove all wishentry from wishList -->


												<a href="${removeWishListUrl}/${wishListData.name}">
													<button class="btn btn-primary">
														<spring:theme code="text.wishlist.deletelist" />
													</button>
												</a>

												<button
													class="btn btn-primary addToCartButton add-item-to-cart"
													id="js-add-to-cart-wish-order-btn">
													<spring:theme code="text.wishlist.addtocart" />
												</button>
											</div>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<div class="alert alert-info">
										<strong><spring:theme code="text.wishlist.empty" /></strong>
									</div>
								</c:otherwise>
							</c:choose>
						</c:when>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>
</div>