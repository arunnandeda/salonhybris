<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<c:url value="/register/newaccount/submit" var="submitActionUrl" />
<c:url value="/register" var="registerAction" />

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="main" style="margin-top: 20px;">
	<div class="container">
		<form:form method="post"
			commandName="prospectCustomerRegistrationForm"
			action="${submitActionUrl}">
			<div class="account-registation">
				<div class="head">
					<h1>
						<spring:theme
							code="text.secureportal.register.prospect.account.registration" />
					</h1>
					<span class="links"> <a href="#"><spring:theme
								code="text.secureportal.register.prospect.help" /></a> <a
						data-toggle="modal" data-target="#callMeBackModal" href="#"><spring:theme
								code="text.secureportal.register.prospect.callback" /></a>
					</span>
				</div>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-12 col-lg-8">
						<div class="col-xs-12 col-lg-6">
							<div class="column">
								<h4>
									<spring:theme
										code="text.secureportal.register.prospect.saloninfo" />
								</h4>
								<p>
									*
									<spring:theme
										code="text.secureportal.register.prospect.reqd.field" />
								</p>
								<form:hidden path="name" id="name" />
								<form:hidden path="email" id="emailId" />
								<input type="hidden" name="CSRFToken" value="${CSRFToken.token}" />

								<!-- Salon name -->

								<div class="form-input-section">
									<formElement:formInputBox idKey="salonName" path="salonName"
										inputCSS="form-control required_field" mandatory="true"
										labelKey="BUSINESS/SALON NAME" />
									<div id="salon_name"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.name.required" />
											</h2>
										</div>
									</div>
								</div>

								<!-- Phone number -->

								<div>
									<formElement:formInputBox inputCSS="form-control" idKey="phone"
										path="phone" labelKey="PHONE NUMBER" />
								</div>

								<!-- no of emp -->

								<div>
									<formElement:formInputBox inputCSS="form-control"
										idKey="noOfEmp" path="noOfEmp" labelKey="NUMBER OF EMPLOYEES" />
								</div>
								<!-- Salon size -->
								<div>
									<formElement:formInputBox inputCSS="form-control"
										idKey="salonSize" path="salonSize" labelKey="SALON SIZE" />
								</div>

								<!-- position -->

								<div>
									<form:select cssClass="form-control" path="position"
										idKey="position">
										<form:option value="0" label="YOUR POSITION" />
										<form:option value="1" label="Owner" />
										<form:option value="2" label="Manager" />
										<form:option value="3" label="Employee" />
									</form:select>
								</div>

								<!--  vat number -->

								<div class="form-input-section">
									<span> <formElement:formInputBox idKey="vatNumber"
											path="vatNo" inputCSS="form-control required_field"
											labelKey="VAT NUMBER *" errorCtrElementType="span" /> <i
										class="icon icon-help" data-toggle="modal"
										data-target="#modal-account-information"></i>
									</span>
									<div id="vat_number"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.vat.required" />
											</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-lg-6">
							<div class="column">
								<h4>
									<spring:theme
										code="text.secureportal.register.prospect.address.info" />
								</h4>
								<p>
									*
									<spring:theme
										code="text.secureportal.register.prospect.reqd.field" />
								</p>
								
								<!-- Street -->
								
								<div class="form-input-section">
									<formElement:formInputBox
										inputCSS="form-control required_field" idKey="street"
										path="address.line1" labelKey="NUMBER AND STREET ADDRESS *" />
									<div id="street_err"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.street.required" />
											</h2>
										</div>
									</div>
								</div>
								
								<!-- City -->
								
								<div class="form-input-section">
									<formElement:formInputBox
										inputCSS="form-control required_field" idKey="city"
										path="address.townCity" labelKey="CITY *" />
									<div id="city_err"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.city.required" />
											</h2>
										</div>
									</div>
								</div>
								
								<!-- State/Region -->
								
								<div class="form-input-section">
									<formElement:formInputBox
										inputCSS="form-control required_field" idKey="region"
										path="district" labelKey="STATE / PROVINCE / REGION *" />
									<div id="region_err"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.region.required" />
											</h2>
										</div>
									</div>
								</div>
								
								<!-- Postal Code -->
								
								<div class="form-input-section">
									<formElement:formInputBox
										inputCSS="form-control required_field" idKey="postalcode"
										path="address.postcode" labelKey="ZIP / POSTAL CODE *" />
									<div id="zipcode_err"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.zip.required" />
											</h2>
										</div>
									</div>
								</div>
								
								<!-- Countries -->
								
								<div class="form-input-section">
									<form:select cssClass="form-control required_field"
										id="country" skipBlank="false"
										skipBlankMessageKey="form.select.empty"
										path="address.countryIso">
										<form:option  value="" label="Country *" />
										<form:options items="${country}"
										itemLabel="name" itemValue="isocode"/>
									</form:select>
									<div id="country_err"
										class="validation-submit validation validation-email">
										<div class="validation-container">
											<h2 class="validation-title">
												<spring:theme code="salon.country.required" />
											</h2>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-4">
						<div class="info">
							<span class="title"><spring:theme
									code="text.secureportal.register.prospect.reg.info" /></span> <span>
								<spring:theme
									code="text.secureportal.register.prospect.reg.info" /> .
							</span>
						</div>
					</div>
				</div>
				<div class="check col-xs-12 col-md-12">
					<div class="col col-xs-12 col-lg-8">
						<div class="terms">
							<div class="checkbox terms-span form-input-section">
								<input type="checkbox" id="checkterms"
									class="form-control required_field" name="terms">
								<p>
									<spring:theme code="text.secureportal.terms.agree" />
									<a href="#"><spring:theme
											code="text.secureportal.register.conditions" />.</a>
								</p>
							
							<div id="check_err"
								class="validation-submit validation validation-email">
								<div class="validation-container">
									<h2 class="validation-title">
										<spring:theme code="salon.checkterms.required" />
									</h2>
								</div>
							</div>
							</div>
							<div>
								<button type="submit" id="submitInfo" class="btn btn-primary">
									<spring:theme
										code="text.secureportal.register.customer.submit.button" />
								</button>
							</div>

						</div>
					</div>

				</div>
				
				<div class="modal fade modal--account" tabindex="-1" role="dialog"
					id="modal-account-information">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">�</span>
								</button>
							</div>
							<div class="modal-body">
								<h4>
									<spring:theme code="text.secureportal.register.prospect.vatno" />
								</h4>
								<p>
									<spring:theme code="text.secureportal.register.text.content" />
									.
								</p>
								<h4>
									<spring:theme code="text.secureportal.register.text.content" />
								</h4>
								<p>
									<spring:theme code="text.secureportal.register.text.number" />
									.
								</p>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
			</div>
		</form:form>

		<user:callmeBack/>

	</div>
</div>


