<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:theme code="setupAccount.title" var="pageTitle"/>
	<c:url value="/register/account/activate" var="registerActionUrl" />
	<user:b2bAccountActivationForm actionNameKey="register.submit"
		action="${registerActionUrl}" />
		
