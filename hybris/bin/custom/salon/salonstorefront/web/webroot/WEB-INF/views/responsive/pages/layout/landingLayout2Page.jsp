<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
	<!-- <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>-->
	<div class="container">
		<div class="home">
			<div class="home-hero">
				<div class="row">
					<div class="col-md-12">
						<cms:pageSlot position="Section1" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
			<div class="home-card">
				<div class="row">
					<a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2A" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2B" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2C" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2D" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="promo-banner">
						<cms:pageSlot position="Section5" var="feature" element="div">
							<cms:component component="${feature}" element="div"
								class="yComponentWrapper" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</div>
	</div>
</template:page>