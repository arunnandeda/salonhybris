<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="quickorder"
	tagdir="/WEB-INF/tags/responsive/quickorder"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:theme code="product.grid.confirmQtys.message"
	var="gridConfirmMessage" />

<template:page pageTitle="${pageTitle}">
	<div id="quickOrder" data-grid-confirm-message="${gridConfirmMessage}">
		<product:addToCartTitle />
		<div class="main">
			<div class="container">
				<div class="quick-order">
					<h1>
						<spring:theme code="text.quickOrder.header" />
					</h1>
					<quickorder:quickorderListRows />
				</div>
			</div>
		</div>
	</div>
</template:page>