<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<template:page pageTitle="${pageTitle}">
	<jsp:body>
	<div class="container">
        <div class="home" data-not-logged-in="">
          <br/>
        <div class="home-hero">
				<div class="row">
					<div class="col-md-12">
						<cms:pageSlot position="Section1" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
			<div class="home-hero">
            <div class="row"><!-- 
            <div class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6"> -->
                <div class="login-section">
                <cms:pageSlot position="SideContent" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
                    <c:url value="/j_spring_security_check" var="loginActionUrl"/>
                    <user:securedlogin actionNameKey="login.login" action="${loginActionUrl}"/>
                </div>
            </div>
        </div>
			<div class="home-card">
				<div class="row">
					<a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2A" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2B" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2C" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a> <a href="#card01" class="col-xs-12 col-md-3">
						<div class="card-item">
							<div class="card-assets">
								<cms:pageSlot position="Section2D" var="feature" element="div">
									<cms:component component="${feature}" element="div" />
								</cms:pageSlot>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="promo-banner">
						<cms:pageSlot position="Section5" var="feature" element="div">
							<cms:component component="${feature}" element="div"
								class="yComponentWrapper" />
						</cms:pageSlot>
					</div>
				</div>
			</div>
      </div>
     </div>
	</jsp:body>
</template:page>
