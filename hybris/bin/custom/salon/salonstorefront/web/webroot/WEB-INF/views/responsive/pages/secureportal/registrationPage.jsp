<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>

<template:page pageTitle="${pageTitle}">

          
 <div class="main" style="margin-top: 20px;">
      <div class="container">
        <div class="register" id="register">
                  <div class="register-head">
            <div class="title"><h1><spring:theme code="text.secureportal.register.title" /></h1></div>
            <div class="cta">
              <a href="#"><spring:theme code="text.secureportal.register.account.faq" /></a>
               <a
						data-toggle="modal" data-target="#callMeBackModal"><spring:theme
								code="text.secureportal.register.prospect.callback" /> </a>
            </div>
          </div>
          <div class="row">
<cms:pageSlot position="LeftContentSlot" var="feature">
 <div class="col-xs-12 col-md-6">
              <div class="section-registration register-member">
                <div class="content" id="registered_user">
                  <div class="inner">
                    <h2><spring:theme code="text.secureportal.register.customer.number.question" /></h2>
                    <p><spring:theme code="text.secureportal.register.account.para" /></p>
                    <c:url value="/register/showEmails" var="showEmailsAction"></c:url>
                    <c:url value="/register/salonCustomerRegistration" var="submitEmailAction"></c:url>
                    <form action="#">
                      <div class="grouped">
                        <input id="customerNumberInput" class="form-control" type="text" 
                        placeholder="<spring:theme code="text.secureportal.register.customer.number.placeholder" />"/>
                        <a class="link" href="#"><spring:theme code="text.secureportal.register.customer.number.missing" /></a>
                        <div id="error_register" class="validation-submit validation validation-email hidden">
                      	<div class="validation-container">
                      	<h3 class="validation-title">
                      	<spring:theme code="text.secureportal.register.customer.number"/>
                      	</h3></div></div>
                      </div>
                       <span id="error_reg" class="input_error hidden">*<spring:theme code="text.secureportal.register.choose.email"/></span>
                      <span id="error_customerExists" class="input_error"></span>
                      <span id="error_b2bunit" class="input_error"></span>
                      <span id="register_message" class="display_msg hidden"><spring:theme code="text.secureportal.register.message"/></span>
                      <span id="no_email_message" class="input_error hidden">*<spring:theme code="text.secureportal.noemail.message"/></span>
                      
                      <div class="validation-member">
                        <h4><spring:theme code="text.secureportal.register.account.address.message" /></h4>
                        <div id="email_container"></div>
                        <p class="information"><spring:theme code="text.secureportal.register.customer.information" /></p>
                      </div>
                      <div><button id="validationMember" class="btn btn-primary getCustomerEmails"
                       data-entry-get-email-action="${showEmailsAction}"
                       data-entry-submit-email-action="${submitEmailAction}"><spring:theme code="text.secureportal.register.customer.submit.button" /></button></div>
                    
                    </form>
                  </div>
                </div>
                	
		<cms:component component="${feature}"  element="div"/>
	
              </div>
            </div>
            </cms:pageSlot>
            <cms:pageSlot position="RightContentSlot" var="feature">
                        <div class="col-xs-12 col-md-6">
              <div class="section-registration register-new">
                <cms:component component="${feature}" element="div"/>
        
                <div class="content">
                  <div class="inner">
                    <h2><spring:theme code="text.secureportal.register.prospect.new.question"/></h2>
                    <p><spring:theme code="text.secureportal.register.prospect.register.message"/></p>
                    <div class="muted"><spring:theme code="text.secureportal.register.prospect.fields.required"/></div>
                    <c:url value="/register/newaccount" var="actionUrl"></c:url>
                    <form action="${actionUrl}" method= "POST"  >
                      <div class="grouped">
                      <div class="form-input-section" style="position:relative">
                      	<div class = "form-group">
                      	<input class="form-control required_field" type="text" name="name" id="name" placeholder="<spring:theme code="text.secureportal.register.prospect.name.placeholder"/>"/></div>
                        <div id="error_newregister" class="validation-submit validation validation-email hidden">
                      	<div class="validation-container no-before">
                      	<h3 class="validation-title">
                      	<spring:theme code="text.secureportal.register.name"/>
                      	</h3></div></div>
                      </div>
                        
                      	<div class="form-input-section"  style="position:relative">
                      		<div class = "form-group">
                      		<input class="form-control required_field" type="text" name="email"  id="email" placeholder="<spring:theme code="text.secureportal.register.prospect.email.placeholder"/>"/>
                         <div id="email_err"
								class="validation-submit validation validation-email hidden">
								<div class="validation-container">
									<h3 class="validation-title">
										<spring:theme
									code="text.secureportal.register.prospect.email.error" />
									</h3>
								</div>
							</div>
                      	</div>
                        </div>
                        <%-- <span id="email_err" class="input_error hidden"><spring:theme code="text.secureportal.register.prospect.email.error"/></span> --%>
                        <input type="hidden" name="CSRFToken" value="${CSRFToken.token}" />
                        <input type="hidden" id="person" value="${response.emailIds}">
                      </div>
                      <div><button type="submit" class="btn btn-primary"  id="submitNewInfo" ><spring:theme code="text.secureportal.register.customer.submit.button"/></button></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
            <user:callmeBack/>
            
            </cms:pageSlot>
            </div>
            </div>
            </div>
            </div>
           </template:page> 
           
          