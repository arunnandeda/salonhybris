package com.hybris.ps.ant.taskdefs;

import de.hybris.ant.taskdefs.UpdatePlatformAntPerformableImpl;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.util.JspContext;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

/**
 * Custom update platform task to include/exclude extensions project data based on the
 * following settings :
 * <ul>
 * <li>includedExtensions: list of included extensions patterns separated by ','</li>
 * <li>excludedExtensions: list of excluded extensions patterns separated by ','</li>
 * </ul>
 *
 * @author guillaume.lasnier
 */
public class CustomUpdatePlatformAntPerformableImpl extends UpdatePlatformAntPerformableImpl {
    private static final Logger LOG = Logger.getLogger(CustomUpdatePlatformAntPerformableImpl.class);
    private final PathMatcher pathMatcher = new AntPathMatcher();
    private final String includedExtensions;
    private final String excludedExtensions;

    /**
     * Default constructor
     */
    public CustomUpdatePlatformAntPerformableImpl() {
        this(Registry.getMasterTenant().getTenantID(), null, null);
    }

    /**
     * @param tenantID
     * @param includedExtensions
     * @param excludedExtensions
     */
    public CustomUpdatePlatformAntPerformableImpl(final String tenantID, final String includedExtensions,
            final String excludedExtensions) {
        super(tenantID);
        this.includedExtensions = StringUtils.isNotBlank(includedExtensions) ? includedExtensions : "*";
        this.excludedExtensions = StringUtils.isNotBlank(excludedExtensions) ? excludedExtensions : "";
        if (LOG.isDebugEnabled()) {
            LOG.debug("includedExtensions = '" + includedExtensions + "'");
            LOG.debug("excludedExtensions = '" + excludedExtensions + "'");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JspContext createJspContext() {
        final JspContext jspContext = super.createJspContext();
        final Map<String, String[]> requestParams = new HashMap<>(jspContext.getServletRequest().getParameterMap());
        generateSampleParams(requestParams);
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameters(requestParams);
        final MockHttpServletResponse response = new MockHttpServletResponse();
        return new JspContext(null, request, response);
    }

    /**
     * Generate project data related parameters.
     *
     * @param requestParams the request parameters used for updating the system.
     */
    private void generateSampleParams(final Map<String, String[]> requestParams) {
        for (final Iterator<Extension> it = getExtensions(); it.hasNext(); ) {
            final Extension ext = it.next();
            final String extensionName = ext.getCreatorName();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Scanning extension '" + extensionName + "'");
            }
            if (isExtensionIncluded(extensionName)) {
                LOG.info("Enabling project data for extension '" + extensionName + "'");
                requestParams.put(extensionName + "_sample", new String[]{"true"});
                final Collection<String> creatorparams = ext.getCreatorParameterNames();
                if (CollectionUtils.isNotEmpty(creatorparams)) {
                    for (final String param : creatorparams) {
                        final String defaultValue = ext.getCreatorParameterDefault(param);
                        if (StringUtils.isNotBlank(defaultValue)) {
                            requestParams.put(extensionName + "_" + param, new String[]{defaultValue});
                        }
                    }
                }
            }
        }
    }

    /**
     * @param extensionName the extension's name/
     * @return <code>true</code> if the extension's project data should be created.
     */
    private boolean isExtensionIncluded(final String extensionName) {
        return pathMatcher.match(includedExtensions, extensionName) && !pathMatcher
                .match(excludedExtensions, extensionName);
    }
}
