#!/bin/bash 

OWN_NAME=setantenv.sh

if [ "$0" == "./$OWN_NAME" ]; then
	echo Please call as ". ./$OWN_NAME", not ./$OWN_NAME !
	exit
fi

CURRENT_PATH=`pwd`
PLATFORM_PATH=../bin/platform
cd "$PLATFORM_PATH"

. ./setantenv.sh

cd "$CURRENT_PATH"
